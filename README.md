# DPS8M-HW

This project is an implementation of a DPS8/M mainframe in an FPGA.

## Overview
This project is being built using the TerasIC DE10 series of FPGA development boards. The initial implementation is starting with a Front-End Processor (FNP) being built on a DE10-Lite board. 

A project announcement can be found at: https://dps8m.gitlab.io/blog/posts/20240622_FPGA/

Project updates are available at: https://dps8m.gitlab.io/blog/tags/FPGA/

## Support
The development of this system is being discussed on Slack in the "Multiplexed Information and Computing Service" workspace on the hardware-emulation channel.


