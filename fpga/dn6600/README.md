# DN355 DE-10 Lite

This is an attempt at creating a DN355/DN6600 using a DE-10 Lite FPGA development board.

## DE-10 Lite Switches, Keys and Indicators

The DE-10 Lite has two push button keys, ten two-position slide switches, ten red LEDs and six 7-segment displays.

The switches are mapped as follows (down is switch towards edge of board):

Switch 0     : Operation Select    : Run (up) / Halt (down)

Switch 1     : Program Load Source : PGM (up) / Preloaded RAM Image (down)

Switch 2     : Page Mode           : Memory Paging (up) / No Paging (down)

Switch 3     : Normal Mode         : Normal Mode (up) / Test Mode (down) 

switch 4     : unused

Switch 5     : Display CPU         : 6678 CPU (up) / IOM (down)

Switch 6 - 9 : Display Mode (octal value on the six 7-segment displays):


Display switch settings for 6678 CPU Mode (Switch 5 up):
+----------------------+---+---+---+---+--------+
| SW Number ->         | 9 | 8 | 7 | 6 | Binary |
|----------------------|---|---|---|---|--------|
| Instruction Counter  | D | D | D | D |  0000  |
| A Register           | D | D | D | U |  0001  |
| Q Register           | D | D | U | D |  0010  |
| X1 Register          | D | D | U | U |  0011  |
| X2 Register          | D | U | D | D |  0100  |
| X3 Register          | D | U | D | U |  0101  |
| Effective Address    | D | U | U | D |  0110  |
| Diagnostic Code      | D | U | U | U |  0111  |
| CPU State            | U | D | D | D |  1000  |
| Indicators & S Reg   | U | D | D | U |  1001  |
| Instruction          | U | D | U | D |  1010  |
| Upper alsMask        | U | D | U | U |  1011  |
| Lower alsMask        | U | U | D | D |  1100  |
| Mem Read Data        | U | U | D | U |  1101  |
| Mem Write Data       | U | U | U | D |  1110  |
| Temp Result Reg      | U | U | U | U |  1111  |
+----------------------+---+---+---+---+--------+

Display switch settings for IOM Mode (Switch 5 down):
+----------------------+---+---+---+---+--------+
| SW Number ->         | 9 | 8 | 7 | 6 | Binary |
|----------------------|---|---|---|---|--------|
| STEX Data            | D | D | D | D |  0000  |
| IOM PCW Low Word     | D | D | D | U |  0001  |
| IOM PCW High Word    | D | D | U | D |  0010  |
| Elapsed Timer        | D | D | U | U |  0011  |
| Int Vector to CPU    | D | U | D | D |  0100  |
| Elapsed Timer Load   | D | U | D | U |  0101  |
| Interval Timer       | D | U | U | D |  0110  |
| Interrupt Enables    | D | U | U | U |  0111  |
| Mailbox Write Data   | U | D | D | D |  1000  |
| Mailbox Enable Ctr   | U | D | D | U |  1001  |
|                      | U | D | U | D |  1010  |
|                      | U | D | U | U |  1011  |
|                      | U | U | D | D |  1100  |
|                      | U | U | D | U |  1101  |
|                      | U | U | U | D |  1110  |
|                      | U | U | U | U |  1111  |
+----------------------+---+---+---+---+--------+

CPU State Register Display (Octal Digits numbered 543210)
| Digit | Description   |
| ----- | -----------   |
|   0   | CPU State     |
              0 FETCH
              1 DECODE
              2 RESOLVE
              3 EXECUTE
              4 MEMORY
              5 FINISH
              6 PROG_LOAD
              7 HALT
|   1   | Fetch State   |
              0 SETUP
              1 DELAY
              2 READ
|   2   | Resolve State |
              0 SETUP
              1 READ
              2 CALC
|  4/3  | Memory State  |
             01 WRITE_DOUBLE_WORD
             02 FINISH_FIRST_WRITE
             03 START_SECOND_WRITE
             04 WRITE_WORD
             05 FINISH_WRITE
             10 READ_DOUBLE_WORD
             11 FINISH_FIRST_READ
             12 START_SECOND_READ
             13 READ_WORD
             14 FINISH_READ
             15 RMW_READ
             16 RMW_MODIFY
             17 RMW_WRITE
|   5   | [unused]      |


Pushbutton KEY0 : Reset
                   - when pressed, causes CPU reset and program reload from Program Load Source if PGM mode
                   - when in RAM Image mode, most likely the board will need to be reloaded for a good RAM image

Pushbutton KEY1 : Single Step
                   - In 1 Hz clock mode, you must hold it until LED 9 transitions from off to on for it to be detected
                   - Single Step stops AFTER the execution of the displayed instruction at the current IC

LED indicators:

LED [0]     : Run (on) / Halt (off)

LED [1]     : 1 KHz Clock

LED [2]     : System Reset

LED [3]     : 50 MHz Clock

LED [4]     : 4 Hz Clock

LED [7 - 5] : CPU Test Points

LED [8]     : Run Request (Switch 0 position)

LED [9]     : Reset Request (Key 0 position)


## Authors
Created by Dean S. Anderson with contributions by Charles Anthony.

## License
BSD 3-Clause License

## Project status
Currently in active development. A partial implementation of the DN355 CPU on a DE-10 Lite board is currently commited. It implements a large number of the machine instructions (though not all of them work correctly yet). Some T&D code has been written in the CpuProgrammer.v module.
Next steps:
- Complete coding of the DN355 CPU
- Finish a Test and Diagnostic program to verify correct operation of the CPU
- Use the J1 processor core to allow using FORTH for development of the DN355 IOM and external interfaces


[All CPU Bus line assignments are in the spreadsheet CPUBus0LineAssignments.ids in the doc directory]

cpuBus0
  200-186  romAddress
  178-171  indicators
  170-156  currentIC
  155-150  regS
  149-132  regX3
  131-114  regX2
  113-96   regX1
   77- 60  regA
   95- 78  regQ

min06bus
    17-0  effectiveAddress

stateBus
    2-0 cpuState
    4-3 fetchState
    7-6 resolveState
   12-9 memoryState 
   16-15 programState
