// Character Add Matrix - used to compute the next character address
module CharacterAddMatrix(
	input wire reset,
	input wire [0:2] row,
	input wire [0:2] col,
	output wire [0:3] result
);

	reg [0:3] matrix [0:7][0:7];
	
	assign result = matrix[row][col];

	always @ (posedge(reset))
	begin

		matrix[0][0] <= 4'o0;
		matrix[0][1] <= 4'o7;
		matrix[0][2] <= 4'o7;
		matrix[0][3] <= 4'o7;
		matrix[0][4] <= 4'o7;
		matrix[0][5] <= 4'o7;
		matrix[0][6] <= 4'o7;
		matrix[0][7] <= 4'o7;

		matrix[1][0] <= 4'o7;
		matrix[1][1] <= 4'o7;
		matrix[1][2] <= 4'o7;
		matrix[1][3] <= 4'o7;
		matrix[1][4] <= 4'o7;
		matrix[1][5] <= 4'o7;
		matrix[1][6] <= 4'o7;
		matrix[1][7] <= 4'o7;

		matrix[2][0] <= 4'o7;
		matrix[2][1] <= 4'o7;
		matrix[2][2] <= 4'o2;
		matrix[2][3] <= 4'o3;
		matrix[2][4] <= 4'o7;
		matrix[2][5] <= 4'o7;
		matrix[2][6] <= 4'o7;
		matrix[2][7] <= 4'o7;

		matrix[3][0] <= 4'o7;
		matrix[3][1] <= 4'o7;
		matrix[3][2] <= 4'o3;
		matrix[3][3] <= 4'o12;
		matrix[3][4] <= 4'o7;
		matrix[3][5] <= 4'o7;
		matrix[3][6] <= 4'o7;
		matrix[3][7] <= 4'o7;

		matrix[4][0] <= 4'o7;
		matrix[4][1] <= 4'o7;
		matrix[4][2] <= 4'o7;
		matrix[4][3] <= 4'o7;
		matrix[4][4] <= 4'o4;
		matrix[4][5] <= 4'o5;
		matrix[4][6] <= 4'o6;
		matrix[4][7] <= 4'o7;

		matrix[5][0] <= 4'o7;
		matrix[5][1] <= 4'o7;
		matrix[5][2] <= 4'o7;
		matrix[5][3] <= 4'o7;
		matrix[5][4] <= 4'o5;
		matrix[5][5] <= 4'o6;
		matrix[5][6] <= 4'o14;
		matrix[5][7] <= 4'o7;

		matrix[6][0] <= 4'o7;
		matrix[6][1] <= 4'o7;
		matrix[6][2] <= 4'o7;
		matrix[6][3] <= 4'o7;
		matrix[6][4] <= 4'o6;
		matrix[6][5] <= 4'o14;
		matrix[6][6] <= 4'o15;
		matrix[6][7] <= 4'o7;

		matrix[7][0] <= 4'o7;
		matrix[7][1] <= 4'o7;
		matrix[7][2] <= 4'o7;
		matrix[7][3] <= 4'o7;
		matrix[7][4] <= 4'o7;
		matrix[7][5] <= 4'o7;
		matrix[7][6] <= 4'o7;
		matrix[7][7] <= 4'o7;
		
	end
	
endmodule