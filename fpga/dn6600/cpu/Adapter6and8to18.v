// This module simply acts as a line adapter between a 6 and 8 bit value to 18 bit value.
// Used to display IR and S in DD01 format
module Adapter6and8to18(
	input 	wire [0:5] din6,
	input 	wire [0:7] din8,
	output 	wire [0:17] dout
);

assign dout[12:17] = din6;
assign dout[0:7] = din8;
assign dout[8:11] = 4'b0000;

endmodule
