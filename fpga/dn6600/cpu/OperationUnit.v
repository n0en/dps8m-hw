module OperationUnit (
	input wire 				clk,							// CPU clock
	input wire 				reset,						// System reset signal
	input wire 				run,							// Signal to CPU to run the instruction cycle
	input wire 				singleStepRequest,		// Signal to CPU to single step
	input wire 				programRequest,			// Signal to use PGM for initial load (when low use ROM)
	input wire 	[0:35] 	memoryReadData,			// Data read from memory on read cycle
	output reg 	[0:17] 	instruction,				// The current instruction register being decoded
	output reg 	[0:14] 	memoryAddress, 			// The memory address the CPU wants to read or write
	output reg 	[0:35] 	memoryWriteData, 			// Data to write to memory controller
	output reg 				memoryStart, 				// Start signal to memory controller to begin executing MOP
	output reg 	[0:2] 	memoryOp, 					// Memory operation requested for the memory cycle
	output reg 				runIndicator,				// Signal that shows CPU is currently running
	output reg 	[0:14] 	currentIC,					// The current value of the instruction counter
	output reg 	[0:3] 	cpuState,					// Current CPU state
	output reg 	[0:1] 	fetchState,					// Current FETCH cycle state
	output reg 	[0:1] 	resolveState,				// Current RESOLVE cycle state
	output reg 			 	unused,						// [not used]
	output reg 	[0:17] 	regA,							// The current value in the A register
	output reg 	[0:17] 	regQ,							// The current value in the Q register
	output reg 	[0:17] 	regX1,						// The current value in the X1 register
	output reg 	[0:17] 	regX2,						// The current value in the X2 register
	output reg 	[0:17] 	regX3,						// The current value in the X3 register
	output reg 	[0:5] 	regS,							// The current value in the S register
	output wire [0:7] 	indicators,					// The current value of the various indicator flags
	output reg 	[0:2] 	programState,
	output reg 	[0:14] 	effectiveAddress,			// The calculated effective address for a memory reference instruction
	output reg 	[0:17] 	diagnosticCode,			// A code indicating where a failure to CPU_DECODE an opcode happened
	input wire 	[0:1] 	opCodeGroup,				// Indicates the op code group number based on the primary op code (0, 1 or 2)
	input wire 	[0:5] 	opCode,						// Primary op code
	input wire 				indirectFlag,				// When true indicates that indirect addressing is being done
	input wire 	[0:1] 	tag,							// For group 0 op codes, contains the tag field
	input wire 	[0:8] 	displacement,				// For group 0 and 1 op codes, contains the displacement field
	input wire 	[0:2] 	s1,							// For group 1 and 2 op codes, contains the S1 field
	input wire 	[0:2] 	s2,							// For group 2 op codes, contains the S2 field
	input wire 	[0:5] 	k,								// For group 2 op codes, contains the K field
	input wire 	[0:35] 	alsMask,						// Special mask for ALS, QLS and LLS instructions based on the K field
	output reg 	[0:12] 	romAddress,					// Programmer address for initial program load
	input wire 	[0:17] 	romData,						// Data read from programmer at programAddress
	input wire 				iomFault,					// Fault indication from IOM
	output reg 	[0:17] 	tempResultReg,				// Temporary result register
	output reg 	[0:2] 	camRow,						// Row address for the character add matrix
	output reg 	[0:2] 	camCol,						// Column address for the character add matrix
	input wire 	[0:3] 	camResult,					// Character address based on camRow and camCol
	input wire 				memReady,					// Memory operation complete signal
	input wire 				parityA,						// Computed parity based on alpMask of the A register, 1 = odd, 0 = even
	input wire 				parityQ,						// Computed parity based on alpMask of the Q register, 1 = odd, 0 = even
	output reg 	[0:3] 	o_tp,							// Test point to be hooked up for debugging
	output reg 	[0:3] 	memoryRmwOp,				// For Memory RMW cycle, requested memory operation
	input wire 				mcCarry,						// Carry flag from memory controller for RMW operation
	input wire 				mcOverflow,					// Overflow flag from memory controller for RMW operation
	input wire 	[0:17] 	stex,							// STEX data from IOM
	input wire 				i_intReq,					// Interrupt Request from IOM
	input wire 	[0:7] 	intVector,					// Interrupt Vector from IOM
	output reg 				connect,						// Connect signal to IOM
	output reg 	[0:35] 	pcw,							// PCW to IOM on Connect
	output reg 				o_IntAck,					// Interrupt acknowledge to IOM
	output reg 				faultAck,					// IOM Fault acknowledge to IOM
	output reg 	[0:15] 	interruptLevelEnable,	// Interrupt level enables to IOM
	output wire	[0:2]		o_CharacterAddressMode,	// Character address mode for memory RMW cycle

	// Interrupt interface for SIC
	output reg				o_SIC,						// Signal indicating an SIC write cycle
	output reg	[0:15]	o_InterruptLevelData,	// Interrupt levels to set on SIC write cycle
	
	// NRM/NRML Shift Counter interface
	input wire	[0:5]		i_NrmShiftCount,			// Number of bits to shift for NRM/NRML
	output reg				unused2,						// [not used]
	
	input wire				i_NormalMode,				// Signal indicating Normal / not Test Mode
	
	// ALU Interface
	input wire				i_AluReady,					// Signal indicating ALU result is ready
	input wire	[0:17]	i_AluResultLow,			// Low order 18 bits of ALU result
	input wire	[0:17]	i_AluResultHigh,			// High order 18 bits of ALU result
	input wire				i_AluCarry,					// Carry flag from ALU result
	input wire				i_AluOverflow,				// Overflow flag from ALU result
	input wire				i_AluDivideCheck,			// Signal indicating ALU had a divide check fault
	
	output wire				o_AluStart,					// Signal to ALU to start requested operation
	output wire	[0:3]		o_AluOp,						// ALU Operation to perform on o_AluStart signal
	output wire [0:35]	o_AluOperand,				// 36 bit operand for ALU operation
	
	output reg 	[0:3]		o_SicLevel,					// Interrupt level for SIC instruction
	
	input  wire			 	i_SPI_RxValid,				// Signal that SPI RxData is now valid
	input  wire	[0:35]	i_SPI_RxData,				// Received data from SPI access
	output reg				o_SPI_Enable,				// Signal to trigger SPI operation
	
	output reg				o_IntLock					// Signal to indicate that we may be reading an interrupt vector
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;
	
	// Internal CPU State Machine storage
	localparam CPU_FETCH     	= 4'b0000;
	localparam CPU_DECODE    	= 4'b0001;
	localparam CPU_RESOLVE   	= 4'b0010;
	localparam CPU_EXECUTE   	= 4'b0011;
	localparam CPU_MEMORY	 	= 4'b0100;
	localparam CPU_FINISH    	= 4'b0101;
	localparam CPU_PROG_LOAD	= 4'b0110;
	localparam CPU_HALT      	= 4'b0111;
	localparam CPU_DIS			= 4'b1000;
	localparam CPU_CHAR_ADD		= 4'b1001;
	localparam CPU_ALU_START	= 4'b1010;
	localparam CPU_SPI			= 4'b1011;
	localparam CPU_RIA			= 4'b1100;
	
	reg [0:14] nextIC;	// Instruction Counter
	
	reg singleInterruptInhibitFlag;		// Flag used to skip any interrupts for one CPU cycle
	reg singleInterruptAllowFlag;			// Flag used to allow an interrupt on next FETCH even if ints are disabled
	reg interruptInhibitFlag;
	reg parityFaultInhibitFlag;
	reg overflowFaultInhibitFlag;
	reg parityErrorFlag;
	
	reg zeroFlag;
	reg negativeFlag;
	reg carryFlag;
	reg overflowFlag;
	
	reg updateZeroFlag;
	reg updateNegativeFlag;
	reg updateMcOverflow;
	reg updateMcCarry;
	reg [0:17] tempSecondResultReg;
	reg [0:4] resultCopyDest;
	reg [0:17] channelStatus;
	
	localparam resultNoCopy 			= 5'b00000;
	localparam resultToA					= 5'b00001;
	localparam resultToQ					= 5'b00010;
	localparam resultToX1				= 5'b00011;
	localparam resultToX2				= 5'b00100;
	localparam resultToX3				= 5'b00101;
	localparam resultToAQ				= 5'b00111;
	localparam resultAddToA				= 5'b01001;
	localparam resultToX1C				= 5'b01010;
	localparam resultToX2C				= 5'b01011;
	localparam resultToX3C				= 5'b01100;
	localparam resultToIndicators		= 5'b01101;
	localparam resultToPCW         	= 5'b01111;
	localparam resultNrmToA				= 5'b10011;
	localparam resultNrmToAQ			= 5'b10100;
	localparam resultAluToA				= 5'b10110;
	localparam resultAluToQ				= 5'b10111;
	localparam resultAluToAQ			= 5'b11000;
	
	
	reg [0:4] memoryReadPostOp;
	
	localparam MEMORY_LOAD			= 5'b00000;
	localparam MEMORY_AND_TO_A		= 5'b00100;
	localparam MEMORY_OR_TO_A		= 5'b00101;
	localparam MEMORY_EOR_TO_A		= 5'b00111;
	localparam MEMORY_CMP_TO_A		= 5'b01011;
	localparam MEMORY_CMP_TO_Q		= 5'b01100;
	localparam MEMORY_CMP_TO_X1	= 5'b01101;
	localparam MEMORY_CMP_TO_X2	= 5'b01110;
	localparam MEMORY_CMP_TO_X3	= 5'b01111;
	localparam MEMORY_CA_TO_X1		= 5'b10000;
	localparam MEMORY_CA_TO_X2		= 5'b10001;
	localparam MEMORY_CA_TO_X3		= 5'b10010;
	localparam MEMORY_MPF			= 5'b10011;
	localparam MEMORY_ALU			= 5'b10110;
	
	
	assign indicators[0] = zeroFlag;
	assign indicators[1] = negativeFlag;
	assign indicators[2] = carryFlag;
	assign indicators[3] = overflowFlag;
	assign indicators[4] = interruptInhibitFlag;
	assign indicators[5] = parityFaultInhibitFlag;
	assign indicators[6] = overflowFaultInhibitFlag;
	assign indicators[7] = parityErrorFlag;
	
	// Other signals
	reg singleStep;
	reg lastSingleStep;
	reg haltFlag;
	reg lastIntReq;				// State of i_intReq on previous instruction execution
	reg illegalOpCodeFault;		// A flag indicating that an attempt was made to execute an illegal operation code
	reg overflowFault;			// A flag indicating that an overflow fault is pending
	reg divideCheckFault;		// A flag indicating that a divide check has occurred
	reg skipDisplacement;		// A flag that causes the displacement calculation in CPU_DECODE to be skipped
	
	reg [0:2] r_CharAddressMode;		// Characater addressing mode when using an X register
	
	// Fetch cycle state
	localparam FETCH_SETUP	= 2'b00;
	localparam FETCH_DELAY	= 2'b01;
	localparam FETCH_READ	= 2'b10;
	
	// Address resolution states
	localparam RESOLVE_SETUP	= 2'b00;
	localparam RESOLVE_READ		= 2'b01;
	localparam RESOLVE_CALC		= 2'b10;
	localparam RESOLVE_CHAR		= 2'b11;
	
	// Memory cycle state
	localparam M_WRITE_DOUBLE_WORD	= 4'b0001;
	localparam M_FINISH_FIRST_WRITE	= 4'b0010;
	localparam M_START_SECOND_WRITE	= 4'b0011;
	localparam M_WRITE_WORD				= 4'b0100;
	localparam M_FINISH_WRITE			= 4'b0101;
	localparam M_READ_DOUBLE_WORD		= 4'b1000;
	localparam M_FINISH_FIRST_READ	= 4'b1001;
	localparam M_START_SECOND_READ	= 4'b1010;
	localparam M_READ_WORD				= 4'b1011;
	localparam M_FINISH_READ			= 4'b1100;
	localparam M_RMW_READ				= 4'b1101;
	localparam M_RMW_MODIFY				= 4'b1110;
	localparam M_RMW_WRITE				= 4'b1111;

	// Read/Modify/Write Operation Codes
	`define RMW_NOP				4'b0001
	`define RMW_ADD_A				4'b0001
	`define RMW_ADD_ONE			4'b0010
	`define RMW_AND_A				4'b0011
	`define RMW_OR_A				4'b0100
	`define RMW_EOR_A				4'b0101
	`define RMW_SUB_A				4'b0110
	`define RMW_WRITE_CHAR_A	4'b0111
	`define RMW_WRITE_CHAR_Q	4'b1000
	
	// Memory Operation Requested
	`define MOP_NOP					3'b000
	`define MOP_READ_WORD			3'b001
	`define MOP_READ_DOUBLE_WORD	3'b010
	`define MOP_WRITE_WORD			3'b011
	`define MOP_WRITE_DOUBLE_WORD	3'b100
	`define MOP_READ_MOD_WRITE		3'b101
	
	
	// Memory Macros
	`define MEMORY_READ(addr) \
		memoryAddress <= addr; \
		memoryWriteData <= 36'b0; \
		memoryOp <= `MOP_READ_WORD; \
		memoryRmwOp <= `RMW_NOP; \
		memoryStart <= HIGH;

	`define MEMORY_READ_DOUBLE(addr) \
		memoryAddress <= addr; \
		memoryWriteData <= 36'b0; \
		memoryOp <= `MOP_READ_DOUBLE_WORD; \
		memoryRmwOp <= `RMW_NOP; \
		memoryStart <= HIGH;

	`define MEMORY_WRITE(addr,data) \
		memoryAddress <= addr; \
		memoryWriteData[0:17] <= 18'b0; \
		memoryWriteData[18:35] <= data; \
		memoryOp <= `MOP_WRITE_WORD; \
		memoryRmwOp <= `RMW_NOP; \
		memoryStart <= HIGH;

	`define MEMORY_WRITE_DOUBLE(addr,dataHigh, dataLow) \
		memoryAddress <= addr; \
		memoryWriteData[0:17] <= dataHigh; \
		memoryWriteData[18:35] <= dataLow; \
		memoryOp <= `MOP_WRITE_DOUBLE_WORD; \
		memoryRmwOp <= `RMW_NOP; \
		memoryStart <= HIGH;
		
	`define MEMORY_RMW(addr,rmwOp) \
		memoryAddress <= addr; \
		memoryWriteData <= 36'b0; \
		memoryOp <= `MOP_READ_MOD_WRITE; \
		memoryRmwOp <= rmwOp; \
		memoryStart <= HIGH;
		
	`define MEMORY_DONE memoryStart <= LOW;
	
	`define MEMORY_WORD \
		memoryReadData[18:35]
		
	`define MEMORY_HIGH_WORD \
		memoryReadData[0:17]
		
	`define MEMORY_LOW_WORD \
		memoryReadData[18:35]
	
	`define MEMORY_GET_WORD(dest) \
		dest <= memoryReadData[18:35];
		
	`define MEMORY_GET_DOUBLE_WORD(destHigh,destLow) \
		destHigh <= memoryReadData[0:17]; \
		destLow <= memoryReadData[18:35];
	
	
	// Set up the program module for reset processing
	localparam PROG_SETUP  = 3'b000;
	localparam PROG_READ	  = 3'b001;
	localparam PROG_WRITE  = 3'b010;
	localparam PROG_FINISH = 3'b011;
	localparam PROG_NEXT   = 3'b100;
	
	// ALU Operations
	localparam ALU_OP_NOP				= 4'b0000;		// Do not perform any operation
	localparam ALU_OP_ADD_A_MEM		= 4'b0001;		// Add register A and memory
	localparam ALU_OP_SUB_A_MEM		= 4'b0010;		// Subtract memory from register A
	localparam ALU_OP_ADD_Q_MEM		= 4'b0011;		// Add register Q and memory
	localparam ALU_OP_SUB_Q_MEM		= 4'b0100;		// Subtract memory from register Q
	localparam ALU_OP_DVF				= 4'b0101;		// Divide AQ by memory
	localparam ALU_OP_NRM				= 4'b0110;		// Normalize
	localparam ALU_OP_NRML				= 4'b0111;		// Normalize Long
	localparam ALU_OP_ADD_AQ_MEM	 	= 4'b1000;		// Add register AQ and memory
	localparam ALU_OP_SUB_AQ_MEM	 	= 4'b1001;		// Subtract memory from register AQ
	
	// ALU Interface registers
	reg r_AluStart;
	reg [0:3] r_AluOp;
	reg [0:35] r_AluOperand;
	
	assign o_AluStart = r_AluStart;
	assign o_AluOp = r_AluOp;
	assign o_AluOperand = r_AluOperand;
	
	
	// Hardware break point register for debugging
	// When the instruction counter hits this address, the CPU will halt
	reg [0:14] breakpoint;
	
	// Output assignments
	assign o_CharacterAddressMode = r_CharAddressMode;
	
	
	// CPU Clock Divider
	reg [0:2] r_CpuClockGateCounter;
	
	always @ (posedge(clk), posedge(reset))
	begin
		if (reset)
			r_CpuClockGateCounter <= 3'b001;
		else if (r_CpuClockGateCounter == 3'b101)
			r_CpuClockGateCounter <= 3'b000;
		else
			r_CpuClockGateCounter <= r_CpuClockGateCounter + 3'b001;
	end

	// Instruction cycle and reset processing
	always @ (posedge(clk), posedge(reset))
	begin
		if (reset)
			begin
				o_tp <= 4'b0;
				nextIC <= 15'b0;
				regA <= 18'b0;
				regQ <= 18'b0;
				regX1 <= 18'b0;
				regX2 <= 18'b0;
				regX3 <= 18'b0;
				zeroFlag <= HIGH;
				negativeFlag <= LOW;
				carryFlag <= LOW;
				overflowFlag <= LOW;
				overflowFault <= LOW;
				divideCheckFault <= LOW;
				illegalOpCodeFault <= LOW;
				singleInterruptInhibitFlag <= LOW;
				singleInterruptAllowFlag <= LOW;
				interruptInhibitFlag <= LOW;
				parityFaultInhibitFlag <= LOW;
				overflowFaultInhibitFlag <= LOW;
				parityErrorFlag <= LOW;
				regS <= 6'b0;
				interruptLevelEnable <= 16'b0;
				instruction <= 18'o777773;
				romAddress <= 0;
				effectiveAddress <= 15'b0;
				runIndicator <= LOW;
				haltFlag <= LOW;
				lastSingleStep <= LOW;
				singleStep <= LOW;
				currentIC <= 15'b0;
				skipDisplacement <= LOW;
				r_CharAddressMode <= 0;
				camRow <= 0;
				camCol <= 0;
				memoryAddress <= 15'b0;
				memoryOp <= 0;
				memoryStart <= 0;
				memoryWriteData <= 36'b0;
				cpuState <= CPU_PROG_LOAD;
				programState <= PROG_SETUP;
				fetchState <= FETCH_SETUP;
				memoryReadPostOp <= MEMORY_LOAD;
				memoryRmwOp <= `RMW_NOP;
				lastIntReq <= LOW;
				connect <= LOW;
				pcw <= 0;
				o_IntAck <= LOW;
				o_SIC <= LOW;
				o_InterruptLevelData <= 16'b0;
				r_AluOp <= ALU_OP_NOP;
				r_AluStart <= LOW;
				o_SicLevel <= 4'b0;
				o_SPI_Enable <= LOW;
				o_IntLock <= HIGH;
				diagnosticCode <= 0;
//				breakpoint <= 15'o00506;  // end of mcs checksum
//				breakpoint <= 15'o00523;  // end of mcs move
//				breakpoint <= 15'o10135;
				breakpoint <= 15'o77777;
//				breakpoint <= 15'o07735;
//				breakpoint <= 15'o50102; //  pager test lda
			end
		else if (r_CpuClockGateCounter == 3'b000)
			case (cpuState)
				CPU_FETCH:
				begin
//					o_tp[0] <= lastIntReq;
//					o_tp[1] <= i_intReq;
				
					// Reset the character addressing to word addressing for now
					r_CharAddressMode <= 0;
					
					// Stop the ALU in case last operation used it
					r_AluStart <= LOW;

					case (fetchState)
						FETCH_SETUP:		// Set up memory acces to get instruction in program
						begin
							runIndicator <= HIGH;
							
							// Check for various fault conditions
							if (illegalOpCodeFault)
							begin
								illegalOpCodeFault <= LOW;
								instruction <= 18'o410000;			// TSY to handler pointed to by location 443 (octal)
								effectiveAddress <= 15'o00443;
								skipDisplacement <= HIGH;
								cpuState <= CPU_DECODE;
								fetchState <= FETCH_SETUP;
								resolveState <= RESOLVE_SETUP;
							end
							else if (overflowFault)
							begin
								overflowFault <= LOW;
								instruction <= 18'o410000;			// TSY to handler pointed to by location 444 (octal)
								effectiveAddress <= 15'o00444;
								skipDisplacement <= HIGH;
								cpuState <= CPU_DECODE;
								fetchState <= FETCH_SETUP;
								resolveState <= RESOLVE_SETUP;
							end
							else if (divideCheckFault)
							begin
								divideCheckFault <= LOW;
								instruction <= 18'o410000;			// TSY to handler pointed to by location 446 (octal)
								effectiveAddress <= 15'o00446;
								skipDisplacement <= HIGH;
								cpuState <= CPU_DECODE;
								fetchState <= FETCH_SETUP;
								resolveState <= RESOLVE_SETUP;
							end
							// If we have a pending interrupt that's not masked, force a TSY to the interrupt vector
							else if ((singleInterruptAllowFlag || !(singleInterruptInhibitFlag || interruptInhibitFlag)) && (!lastIntReq && i_intReq))
							begin
								lastIntReq <= HIGH;
								instruction <= 18'o410000;
								effectiveAddress <= intVector;
								skipDisplacement <= HIGH;
								tempResultReg <= intVector;
								cpuState <= CPU_DECODE;
								fetchState <= FETCH_SETUP;
								resolveState <= RESOLVE_SETUP;
								o_IntAck <= HIGH;
								o_tp <= o_tp + 3'b001;
							end
							else
							begin
								`MEMORY_READ(nextIC)
								currentIC <= nextIC;  // Save address of PC for breakpoint
								fetchState <= FETCH_DELAY;
							end
						end
						
						FETCH_DELAY:		// Wait for memory read to complete
							fetchState <= (memReady) ? FETCH_READ : FETCH_DELAY;
							
						FETCH_READ:		// Get get instruction
						begin
							instruction <= `MEMORY_WORD;
							`MEMORY_DONE
							nextIC <= nextIC + 15'b1;
							skipDisplacement <= LOW;
							cpuState <= CPU_DECODE;
							fetchState <= FETCH_SETUP;
						end
						
						default:	diagnosticCode <= 18'o777001;
					endcase
				end
				
				CPU_DECODE:  	// Disassemble the instruction 
				begin
					// These flags get automatically reset as they only apply for one instruction or clock cycle
					singleInterruptInhibitFlag <= LOW;
					singleInterruptAllowFlag <= LOW;
					o_IntAck <= LOW;
					o_IntLock <= LOW;
					
					// Reset in case the prior instruction was a CIOC
					connect <= LOW;

					// Establish default state for various control flags and state machines
					updateZeroFlag <= 0;
					updateNegativeFlag <= 0;
					updateMcOverflow <= 0;
					updateMcCarry <= 0;
					resultCopyDest <= resultNoCopy;
					memoryReadPostOp <= MEMORY_LOAD;

					if (skipDisplacement)
					begin
						cpuState <= CPU_RESOLVE;
					end
					else if (opCodeGroup == 0)
					begin
						case (tag)
							2'b00: 
							begin
								effectiveAddress <= $signed(displacement) + currentIC;
								resolveState <= RESOLVE_SETUP;
								cpuState <= (indirectFlag) ? CPU_RESOLVE : CPU_EXECUTE;
							end
							2'b01: 
							begin
								effectiveAddress <= $signed(displacement[3:8]) + regX1[3:17];
								r_CharAddressMode <= regX1[0:2];
								camRow <= regX1[0:2];
								camCol <= (displacement[0:2] == 3'o7) ? 3'o0 : displacement[0:2];	// C7 Fix
								resolveState <= RESOLVE_CHAR;
								cpuState <= CPU_RESOLVE;
							end
							2'b10: 
							begin
								effectiveAddress <= $signed(displacement[3:8]) + regX2[3:17];
								r_CharAddressMode <= regX2[0:2];
								camRow <= regX2[0:2];
								camCol <= (displacement[0:2] == 3'o7) ? 3'o0 : displacement[0:2];	// C7 Fix
								resolveState <= RESOLVE_CHAR;
								cpuState <= CPU_RESOLVE;
							end
							2'b11: 
							begin
								effectiveAddress <= $signed(displacement[3:8]) + regX3[3:17];
								r_CharAddressMode <= regX3[0:2];
								camRow <= regX3[0:2];
								camCol <= (displacement[0:2] == 3'o7) ? 3'o0 : displacement[0:2];	// C7 Fix
								resolveState <= RESOLVE_CHAR;
								cpuState <= CPU_RESOLVE;
							end
							default: 
							begin
								resolveState <= RESOLVE_SETUP;
								cpuState <= (indirectFlag) ? CPU_RESOLVE : CPU_EXECUTE;
							end
						endcase
					end
					else
						cpuState <= CPU_EXECUTE;
				end
				
				CPU_RESOLVE:	// Resolve the effective address for a memory access instruction
				begin
					case (resolveState)
						RESOLVE_SETUP:		// Set up to read the indirect word from memory
						begin
							`MEMORY_READ(effectiveAddress)
							resolveState <= RESOLVE_READ;
						end
						
						RESOLVE_READ:		// Wait for memory read to complete
							resolveState <= (memReady) ? RESOLVE_CALC : RESOLVE_READ;
							
						RESOLVE_CALC:		// Calculate next memory read or bail out
						begin
							`MEMORY_DONE

							// Update the effective address based on the tag in the memory word
							case (memoryReadData[19:20])
								2'b00: effectiveAddress <= memoryReadData[21:35];
								2'b01: effectiveAddress <= memoryReadData[21:35] + regX1[3:17];
								2'b10: effectiveAddress <= memoryReadData[21:35] + regX2[3:17];
								2'b11: effectiveAddress <= memoryReadData[21:35] + regX3[3:17];
							endcase
							
							resolveState <= RESOLVE_SETUP;
							
							// If the indirect flag in the memory word is set we need another CPU_RESOLVE cycle
							cpuState <= (memoryReadData[18]) ? CPU_RESOLVE : CPU_EXECUTE;
						end
						
						RESOLVE_CHAR:		// Perform character addition to determine effective address
						begin
							effectiveAddress <= camResult[0] ? (effectiveAddress + 15'b1) : effectiveAddress;
							r_CharAddressMode <= camResult[1:3];
							resolveState <= RESOLVE_SETUP;
							cpuState <= (indirectFlag) ? CPU_RESOLVE : CPU_EXECUTE;
						end

						default:	diagnosticCode <= 18'o777002;
					
					endcase
				end
					
				CPU_EXECUTE: 	// Perform desired operation
				begin
					case (opCodeGroup)
						2'b00:		// Group 0 Op Codes: Memory Reference
						begin
							case (opCode)
								6'o00: 			// DIAG - Test Mode diagnostic instruction (not a DN355 op code) - known as DIAG instruction
								begin
									if (i_NormalMode)
									begin
										illegalOpCodeFault <= HIGH;
									end
									else
									begin
										diagnosticCode <= diagnosticCode | displacement;
										haltFlag <= 1;												
									end
									cpuState <= CPU_FINISH;
								end
								6'o01: 			// MPF
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_MPF;
									resultCopyDest <= resultToAQ;
									cpuState <= CPU_MEMORY;
								end
								6'o02:			// ADCX2
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									memoryReadPostOp <= MEMORY_CA_TO_X2;
									resultCopyDest <= resultToX2C;
									cpuState <= CPU_MEMORY;
								end
								6'o03:			// LDX2
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= LOW;
									resultCopyDest <= resultToX2;
									cpuState <= CPU_MEMORY;
								end
								6'o04:			// LDAQ
								begin
									`MEMORY_READ_DOUBLE(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									resultCopyDest <= resultToAQ;
									cpuState <= CPU_MEMORY;
								end
								6'o05: 			// DIAGH - Test Mode diagnostic instruction (not a DN355 op code) - known as DIAGH instruction
								begin
									if (i_NormalMode)
										illegalOpCodeFault <= HIGH;
									else
										diagnosticCode <= (displacement << 9);
									cpuState <= CPU_FINISH;
								end
								6'o06: 			// ADA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_ALU;
									r_AluOp <= ALU_OP_ADD_A_MEM;
									resultCopyDest <= resultAluToA;
									cpuState <= CPU_MEMORY;
								end
								6'o07:			// LDA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o10:			// TSY
								begin
									`MEMORY_WRITE(effectiveAddress,currentIC + 18'b1)
									nextIC <= effectiveAddress + 15'b1;
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_MEMORY;
								end
								// Note that 6'o11 is used by MCS to trigger an illegal instruction fault so don't define it!
								6'o13:			// STX2
								begin
									`MEMORY_WRITE(effectiveAddress,regX2)
									cpuState <= CPU_MEMORY;
								end
								6'o14:			// STAQ
								begin
									`MEMORY_WRITE_DOUBLE(effectiveAddress,regA,regQ)
									cpuState <= CPU_MEMORY;
								end
								6'o15: 			// ADAQ
								begin
									`MEMORY_READ_DOUBLE(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_ALU;
									r_AluOp <= ALU_OP_ADD_AQ_MEM;
									resultCopyDest <= resultAluToAQ;
									cpuState <= CPU_MEMORY;
								end
								6'o16:			// ASA
								begin
									`MEMORY_RMW(effectiveAddress,`RMW_ADD_A)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									updateMcOverflow <= HIGH;
									updateMcCarry <= HIGH;
									cpuState <= CPU_MEMORY;
								end
								6'o17:			// STA
								begin
									if (r_CharAddressMode == 0)
									begin
										`MEMORY_WRITE(effectiveAddress,regA)
									end
									else
									begin
										`MEMORY_RMW(effectiveAddress,`RMW_WRITE_CHAR_A)
									end
									cpuState <= CPU_MEMORY;
								end
								6'o20:			// SZN
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									cpuState <= CPU_MEMORY;
								end
								6'o21:			// DVF
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_ALU;
									r_AluOp <= ALU_OP_DVF;
									resultCopyDest <= resultAluToAQ;
									cpuState <= CPU_MEMORY;
								end
								6'o23: 			// CMPX2
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= LOW;
									updateNegativeFlag <= LOW;
									memoryReadPostOp <= MEMORY_CMP_TO_X2;
									cpuState <= CPU_MEMORY;
								end								
								6'o24: 			// SBAQ
								begin
									`MEMORY_READ_DOUBLE(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_ALU;
									r_AluOp <= ALU_OP_SUB_AQ_MEM;
									resultCopyDest <= resultAluToAQ;
									cpuState <= CPU_MEMORY;
								end
								6'o26: 			// SBA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_ALU;
									r_AluOp <= ALU_OP_SUB_A_MEM;
									resultCopyDest <= resultAluToA;
									cpuState <= CPU_MEMORY;
								end
								6'o27: 			// CMPA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= LOW;
									updateNegativeFlag <= LOW;
									memoryReadPostOp <= MEMORY_CMP_TO_A;
									cpuState <= CPU_MEMORY;
								end
								6'o30:			//TODO: Implement LDEX
								begin
									cpuState <= CPU_FINISH;
								end
								6'o31: 			// CANA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_AND_TO_A;
									cpuState <= CPU_MEMORY;
								end
								6'o32:			// ANSA
								begin
									`MEMORY_RMW(effectiveAddress,`RMW_AND_A)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									cpuState <= CPU_MEMORY;
								end
								6'o34: 			// ANA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_AND_TO_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o35: 			// ERA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_EOR_TO_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o36:			// SSA
								begin
									`MEMORY_RMW(effectiveAddress,`RMW_SUB_A)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									updateMcOverflow <= HIGH;
									updateMcCarry <= HIGH;
									cpuState <= CPU_MEMORY;
								end
								6'o37: 			// ORA
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_OR_TO_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o40:			// ADCX3
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									memoryReadPostOp <= MEMORY_CA_TO_X3;
									resultCopyDest <= resultToX3C;
									cpuState <= CPU_MEMORY;
								end
								6'o41:			// LDX3
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= LOW;
									resultCopyDest <= resultToX3;
									cpuState <= CPU_MEMORY;
								end
								6'o42:			// ADCX1
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									memoryReadPostOp <= MEMORY_CA_TO_X1;
									resultCopyDest <= resultToX1C;
									cpuState <= CPU_MEMORY;
								end
								6'o43:			// LDX1
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= LOW;
									resultCopyDest <= resultToX1;
									cpuState <= CPU_MEMORY;
								end
								6'o44:			// LDI
								begin
									`MEMORY_READ(effectiveAddress)
									resultCopyDest <= resultToIndicators;
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_MEMORY;
								end
								6'o45:			// TNC
								begin
									if (~carryFlag) 
										nextIC <= effectiveAddress;		
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_FINISH;
								end
								6'o46: 			// ADQ
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_ALU;
									r_AluOp <= ALU_OP_ADD_Q_MEM;
									resultCopyDest <= resultAluToQ;
									cpuState <= CPU_MEMORY;
								end
								6'o47:			// LDQ
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									resultCopyDest <= resultToQ;
									cpuState <= CPU_MEMORY;
								end
								6'o50:			// STX3
								begin
									`MEMORY_WRITE(effectiveAddress,regX3)
									cpuState <= CPU_MEMORY;
								end
								// Note that 6'o51 is used by MCS to trigger an illegal instruction fault so don't define it!
								6'o53:			// STX1
								begin
									`MEMORY_WRITE(effectiveAddress,regX1)
									cpuState <= CPU_MEMORY;
								end
								6'o54:			// STI
								begin
									`MEMORY_WRITE(effectiveAddress,{indicators, 4'b0, regS})
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_MEMORY;
								end
								6'o55: 			// TOV
								begin
									if (overflowFlag) 
										nextIC <= effectiveAddress;
									overflowFlag <= LOW;							// We reset the overflow flag after testing it	
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_FINISH;
								end
								6'o56:			// STZ
								begin
									`MEMORY_WRITE(effectiveAddress,18'o0)
									cpuState <= CPU_MEMORY;
								end
								6'o57:			// STQ
								begin
									if (r_CharAddressMode == 0)
									begin
										`MEMORY_WRITE(effectiveAddress,regQ)
									end
									else
									begin
										`MEMORY_RMW(effectiveAddress,`RMW_WRITE_CHAR_Q)
									end
									cpuState <= CPU_MEMORY;
								end
								6'o60:			// CIOC
								begin
									`MEMORY_READ_DOUBLE(effectiveAddress)
									resultCopyDest <= resultToPCW;
									cpuState <= CPU_MEMORY;
								end
								6'o61: 			// CMPX3
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= LOW;
									updateNegativeFlag <= LOW;
									memoryReadPostOp <= MEMORY_CMP_TO_X3;
									cpuState <= CPU_MEMORY;
								end																
								6'o62:			// ERSA
								begin
									`MEMORY_RMW(effectiveAddress,`RMW_EOR_A)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									cpuState <= CPU_MEMORY;
								end
								6'o63: 			// CMPX1
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= LOW;
									updateNegativeFlag <= LOW;
									memoryReadPostOp <= MEMORY_CMP_TO_X1;
									cpuState <= CPU_MEMORY;
								end								
								6'o64: 			// TNZ
								begin
									if (~zeroFlag) 
										nextIC <= effectiveAddress;		
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_FINISH;
								end
								6'o65: 			// TPL
								begin
									if (~negativeFlag) 
										nextIC <= effectiveAddress;	
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_FINISH;
								end
								6'o66: 			// SBQ
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									memoryReadPostOp <= MEMORY_ALU;
									r_AluOp <= ALU_OP_SUB_Q_MEM;
									resultCopyDest <= resultAluToQ;
									cpuState <= CPU_MEMORY;
								end
								6'o67: 			// CMPQ
								begin
									`MEMORY_READ(effectiveAddress)
									updateZeroFlag <= LOW;
									updateNegativeFlag <= LOW;
									memoryReadPostOp <= MEMORY_CMP_TO_Q;
									cpuState <= CPU_MEMORY;
								end
								6'o70:			// STEX
								begin
									if (regS == 6'o06) // HSLA #1
										channelStatus = 18'o777777;
									else if (regS == 6'o77) // Timer; return panel switches
										channelStatus = 18'o123456; // TODO implement panel switches
									else
										channelStatus = 18'o000000;
									`MEMORY_WRITE(effectiveAddress,channelStatus)
									cpuState <= CPU_MEMORY;
								end
								6'o71: 			// TRA
								begin
									nextIC <= effectiveAddress;							
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_FINISH;
								end
								6'o72:			// ORSA
								begin
									`MEMORY_RMW(effectiveAddress,`RMW_OR_A)
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									cpuState <= CPU_MEMORY;
								end
								6'o74: 			// TZE
								begin
									if (zeroFlag) 
										nextIC <= effectiveAddress; 		
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_FINISH;
								end
								6'o75: 			// TMI
								begin
									if (negativeFlag) 
										nextIC <= effectiveAddress;	
									singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
									cpuState <= CPU_FINISH;
								end
								6'o76:			// AOS
								begin
									`MEMORY_RMW(effectiveAddress,`RMW_ADD_ONE)
									updateMcOverflow <= HIGH;
									updateMcCarry <= HIGH;
									updateZeroFlag <= HIGH;
									updateNegativeFlag <= HIGH;
									cpuState <= CPU_MEMORY;
								end
								default:	
								begin
									illegalOpCodeFault <= HIGH;
									cpuState <= CPU_FINISH;
								end
							endcase
						end
						2'b01:		// Group 1 Op Codes: Non-Memory Reference
						begin
							case (opCode)
								6'o12:
								begin
									case (s1)
										3'o0:		// RIER
										begin
											tempResultReg[16:17] <= 2'b00;
											tempResultReg[0:15] <= interruptLevelEnable;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o4:		// RIA
										begin
											if (i_intReq)
											begin
												o_IntLock <= HIGH;
												cpuState <= CPU_RIA;
											end
											else
											begin
												regA <= 18'o400447;
												cpuState <= CPU_FINISH;
											end
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
										end
										default: diagnosticCode <= 18'o777004;
									endcase
								end
								6'o22:
								begin
									case (s1)
										3'o0: 	// IANA
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											tempResultReg <= regA & $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o1: 	// IORA
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											tempResultReg <= regA | $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o2: 	// ICANA
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											tempResultReg <= regA & $signed(displacement);
											cpuState <= CPU_FINISH;
										end
										3'o3: 	// IERA
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											tempResultReg <= regA ^ $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o4:		// ICMPA
										begin
											updateZeroFlag <= LOW;
											updateNegativeFlag <= LOW;
											if (!regA[0] && displacement[0])
											begin
												carryFlag <= LOW;
												zeroFlag <= LOW;
												negativeFlag <= LOW;
											end
											else if (regA[0] == displacement[0])
											begin
												if (regA > $signed(displacement))
												begin
													carryFlag <= HIGH;
													zeroFlag <= LOW;
													negativeFlag <= LOW;
												end
												else if (regA == $signed(displacement))
												begin
													carryFlag <= HIGH;
													zeroFlag <= HIGH;
													negativeFlag <= LOW;
												end
												else
												begin
													carryFlag <= LOW;
													zeroFlag <= LOW;
													negativeFlag <= HIGH;
												end
											end
											else
											begin
												carryFlag <= HIGH;
												zeroFlag <= LOW;
												negativeFlag <= HIGH;
											end
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								6'o52:
								begin
									case (s1)
										3'o0:		// SIER
										begin
											interruptLevelEnable <= regA[0:15];
											cpuState <= CPU_FINISH;
										end
										3'o1:		// SPI - Not a DN355 op code but defined here to access SPI port
										begin
											// Contents of AQ will be read by the Ethernet Shield Module
											o_SPI_Enable <= HIGH;
											cpuState <= CPU_SPI;
										end
										3'o4:		// SIC
										begin
											o_InterruptLevelData <= regA[0:15];
											o_SicLevel <= displacement[5:8];
											o_SIC <= HIGH;
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								6'o73:
								begin
									case (s1)
										3'o0: 	// SEL
										begin
											regS <= displacement[3:8];	
											cpuState <= CPU_FINISH;
										end
										3'o1:		// IACX1
										begin
											updateZeroFlag <= HIGH;
											tempResultReg <= regX1[3:17] + $signed(displacement[3:8]);
											camRow <= regX1[0:2];
											camCol <= (displacement[0:2] == 3'o7) ? 3'o0 : displacement[0:2];	// C7 Fix
											resultCopyDest <= resultToX1C;
											cpuState <= CPU_CHAR_ADD;
										end
										3'o2:		// IACX2
										begin
											updateZeroFlag <= HIGH;
											tempResultReg <= regX2[3:17] + $signed(displacement[3:8]);
											camRow <= regX2[0:2];
											camCol <= (displacement[0:2] == 3'o7) ? 3'o0 : displacement[0:2];	// C7 Fix
											resultCopyDest <= resultToX2C;
											cpuState <= CPU_CHAR_ADD;
										end
										3'o3:		// IACX3
										begin
											updateZeroFlag <= HIGH;
											tempResultReg <= regX3[3:17] + $signed(displacement[3:8]);
											camRow <= regX3[0:2];
											camCol <= (displacement[0:2] == 3'o7) ? 3'o0 : displacement[0:2];	// C7 Fix
											resultCopyDest <= resultToX3C;
											cpuState <= CPU_CHAR_ADD;
										end
										3'o4:		// ILQ 
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											tempResultReg <= $signed(displacement);
											resultCopyDest <= resultToQ;
											cpuState <= CPU_FINISH;
										end
										3'o5:		// IAQ 
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											r_AluOp <= ALU_OP_ADD_Q_MEM;
											r_AluOperand <= $signed(displacement);
											resultCopyDest <= resultAluToQ;
											cpuState <= CPU_ALU_START;
										end
										3'o6: 	// ILA
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											tempResultReg <= $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o7: 	// IAA
										begin
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											r_AluOp <= ALU_OP_ADD_A_MEM;
											r_AluOperand <= $signed(displacement);
											resultCopyDest <= resultAluToA;
											cpuState <= CPU_ALU_START;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								default:	
								begin
									illegalOpCodeFault <= HIGH;
									cpuState <= CPU_FINISH;
								end
							endcase
						end
						2'b10:		// Group 2 Op Codes: Register Manipulation
						begin
							case (s1)
								3'o0:
								begin
									case (s2)
										3'o2:		// CAX2 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToX2;
											updateZeroFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o4:		// LLS
										begin
											carryFlag <= ((k > 35) && ({regA,regQ} != 0)) ||
																!((({regA,regQ} & alsMask) == 0) || (({regA,regQ} & alsMask) == alsMask));
											{tempSecondResultReg, tempResultReg} <= {regA,regQ} << k;
											resultCopyDest <= resultToAQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o5:		// LRS
										begin
											{tempSecondResultReg,tempResultReg} <= $signed({regA,regQ}) >>> k;
											resultCopyDest <= resultToAQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o6:		// ALS
										begin
											carryFlag <= ((k > 17) && (regA != 0)) ||
																!(((regA & alsMask[0:17]) == 0) || ((regA & alsMask[0:17]) == alsMask[0:17]));
											tempResultReg <= regA << k;
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o7:		// ARS
										begin
											tempResultReg <= $signed(regA) >>> k;
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								3'o1:
								begin
									case (s2)
										3'o4:		// NRML
										begin
											r_AluOp <= ALU_OP_NRML;
											resultCopyDest <= resultNrmToAQ;
											cpuState <= CPU_ALU_START;
										end
										3'o6:		// NRM
										begin
											r_AluOp <= ALU_OP_NRM;
											resultCopyDest <= resultNrmToA;
											cpuState <= CPU_ALU_START;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								3'o2:
								begin
									case (s2)
										3'o1:		// NOP
										begin
											cpuState <= CPU_FINISH;
										end
										3'o2:		// CX1A 
										begin
											tempResultReg <= regX1;
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o4:		// LLR
										begin
											{tempSecondResultReg,tempResultReg} <= ({regA,regQ} << k) | ({regA,regQ} >> (36 - k));
											resultCopyDest <= resultToAQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o5:		// LRL
										begin
											{tempSecondResultReg,tempResultReg} <= {regA,regQ} >> k;
											resultCopyDest <= resultToAQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o6:		// ALR
										begin
											tempResultReg <= (regA << k) | (regA >> (18 - k));
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o7:		// ARL
										begin
											tempResultReg <= regA >> k;
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								3'o3:
								begin
									case (s2)
										3'o1:		// INH
										begin
											interruptInhibitFlag <= HIGH;
											singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
											cpuState <= CPU_FINISH;
										end
										3'o2:		// CX2A 
										begin
											tempResultReg <= regX2;
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o3:		// CX3A 
										begin
											tempResultReg <= regX3;
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o6:		// ALP
										begin
											zeroFlag <= !parityA;
											tempResultReg <= (regA << k) | (regA >> (18 - k));
											resultCopyDest <= resultToA;
											updateNegativeFlag <= HIGH;											
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								3'o4:
								begin
									case (s2)
										3'o1:		// DIS 
										begin
											cpuState <= CPU_DIS;
										end
										3'o2:		// CAX1 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToX1;
											updateZeroFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o3:		// CAX3 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToX3;
											updateZeroFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o6:		// QLS
										begin
											carryFlag <= ((k > 17) && (regQ != 0)) ||
																!(((regQ & alsMask[0:17]) == 0) || ((regQ & alsMask[0:17]) == alsMask[0:17]));
											tempResultReg <= regQ << k;
											resultCopyDest <= resultToQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o7:		// QRS
										begin
											tempResultReg <= $signed(regQ) >>> k;
											resultCopyDest <= resultToQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								3'o6:
								begin
									case (s2)
										3'o3:		// CAQ 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o6:		// QLR
										begin
											tempResultReg <= (regQ << k) | (regQ >> (18 - k));
											resultCopyDest <= resultToQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o7:		// QRL
										begin
											tempResultReg <= regQ >> k;
											resultCopyDest <= resultToQ;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								3'o7:
								begin
									case (s2)
										3'o1:		// ENI
										begin
											interruptInhibitFlag <= LOW;
											singleInterruptInhibitFlag <= HIGH;			// Don't allow interrupts for the following instruction
											cpuState <= CPU_FINISH;
										end
										3'o3:		// CQA 
										begin
											tempResultReg <= regQ;
											resultCopyDest <= resultToA;
											updateZeroFlag <= HIGH;
											updateNegativeFlag <= HIGH;
											cpuState <= CPU_FINISH;
										end
										3'o6:		// QLP
										begin
											zeroFlag <= !parityQ;
											tempResultReg <= (regQ << k) | (regQ >> (18 - k));
											resultCopyDest <= resultToQ;
											updateNegativeFlag <= HIGH;											
											cpuState <= CPU_FINISH;
										end
										default:	
										begin
											illegalOpCodeFault <= HIGH;
											cpuState <= CPU_FINISH;
										end
									endcase
								end
								default:	
								begin
									illegalOpCodeFault <= HIGH;
									cpuState <= CPU_FINISH;
								end
							endcase
						end
						default:	
						begin
							illegalOpCodeFault <= HIGH;
							cpuState <= CPU_FINISH;
						end
					endcase
				end
				
				CPU_MEMORY:		// Perform a memory cycle
				begin
					if (memReady)
					begin
						case (memoryReadPostOp)
							MEMORY_LOAD:			
							begin
								`MEMORY_GET_DOUBLE_WORD(tempSecondResultReg,tempResultReg)
								cpuState <= CPU_FINISH;
							end
							MEMORY_AND_TO_A:
							begin
								tempResultReg <= regA & `MEMORY_WORD;
								cpuState <= CPU_FINISH;
							end
							MEMORY_OR_TO_A:
							begin
								tempResultReg <= regA | `MEMORY_WORD;
								cpuState <= CPU_FINISH;
							end
							MEMORY_EOR_TO_A:
							begin
								tempResultReg <= regA ^ `MEMORY_WORD;
								cpuState <= CPU_FINISH;
							end
							MEMORY_CMP_TO_A:		
							begin
								if (!regA[0] && memoryReadData[18])
								begin
									carryFlag <= LOW;
									zeroFlag <= LOW;
									negativeFlag <= LOW;
								end
								else if (regA[0] == memoryReadData[18])
								begin
									if (regA > $signed(`MEMORY_WORD))
									begin
										carryFlag <= HIGH;
										zeroFlag <= LOW;
										negativeFlag <= LOW;
									end
									else if (regA == $signed(`MEMORY_WORD))
									begin
										carryFlag <= HIGH;
										zeroFlag <= HIGH;
										negativeFlag <= LOW;
									end
									else
									begin
										carryFlag <= LOW;
										zeroFlag <= LOW;
										negativeFlag <= HIGH;
									end
								end
								else
								begin
									carryFlag <= HIGH;
									zeroFlag <= LOW;
									negativeFlag <= HIGH;
								end
								cpuState <= CPU_FINISH;
							end
							MEMORY_CMP_TO_Q:
							begin
								if (!regQ[0] && memoryReadData[18])
								begin
									carryFlag <= LOW;
									zeroFlag <= LOW;
									negativeFlag <= LOW;
								end
								else if (regQ[0] == memoryReadData[18])
								begin
									if (regQ > $signed(`MEMORY_WORD))
									begin
										carryFlag <= HIGH;
										zeroFlag <= LOW;
										negativeFlag <= LOW;
									end
									else if (regQ == $signed(`MEMORY_WORD))
									begin
										carryFlag <= HIGH;
										zeroFlag <= HIGH;
										negativeFlag <= LOW;
									end
									else
									begin
										carryFlag <= LOW;
										zeroFlag <= LOW;
										negativeFlag <= HIGH;
									end
								end
								else
								begin
									carryFlag <= HIGH;
									zeroFlag <= LOW;
									negativeFlag <= HIGH;
								end
								cpuState <= CPU_FINISH;
							end
							MEMORY_CMP_TO_X1:
							begin
								zeroFlag <= (regX1 == `MEMORY_WORD) ? HIGH : LOW;
								cpuState <= CPU_FINISH;
							end
							MEMORY_CMP_TO_X2:
							begin
								zeroFlag <= (regX2 == `MEMORY_WORD) ? HIGH : LOW;
								cpuState <= CPU_FINISH;
							end
							MEMORY_CMP_TO_X3:
							begin
								zeroFlag <= (regX3 == `MEMORY_WORD) ? HIGH : LOW;
								cpuState <= CPU_FINISH;
							end
							MEMORY_CA_TO_X1:
							begin
								tempResultReg <= $signed(regX1[3:17]) + $signed(memoryReadData[21:35]);
								camRow <= regX1[0:2];
								camCol <= memoryReadData[18:20];
								cpuState <= CPU_CHAR_ADD;
							end
							MEMORY_CA_TO_X2:
							begin
								tempResultReg <= $signed(regX2[3:17]) + $signed(memoryReadData[21:35]);
								camRow <= regX2[0:2];
								camCol <= memoryReadData[18:20];
								cpuState <= CPU_CHAR_ADD;
							end
							MEMORY_CA_TO_X3:
							begin
								tempResultReg <= $signed(regX3[3:17]) + $signed(memoryReadData[21:35]);
								camRow <= regX3[0:2];
								camCol <= memoryReadData[18:20];
								cpuState <= CPU_CHAR_ADD;
							end
							MEMORY_MPF:
							begin
								{tempSecondResultReg,tempResultReg} <= ($signed(regA) * $signed(`MEMORY_LOW_WORD)) << 1;
								if ((regA == 0400000) && (`MEMORY_HIGH_WORD == 0400000))
								begin
									overflowFlag <= HIGH;
									overflowFault <= !overflowFaultInhibitFlag;
								end
								cpuState <= CPU_FINISH;
							end
							MEMORY_ALU:
							begin
								r_AluOperand <= memoryReadData;
								cpuState <= CPU_ALU_START;
							end

							default: 				
							begin
								diagnosticCode <= 18'o777022;
								cpuState <= CPU_HALT;
							end
						endcase
						`MEMORY_DONE
					end

				end
				
				CPU_FINISH:		// Finish processing current instruction
				begin
					// We need to drop intAck for the RIA instruction
					o_IntAck <= LOW;
					
					// We need to drop SIC for the SIC instruction
					o_SIC <= LOW;

					// Perform the requested copy operation
					case (resultCopyDest)
						resultToA:
						begin
							case (r_CharAddressMode)
								3'b000: regA <= tempResultReg;
								3'b001: regA <= regA;
								3'b010: regA <= {9'b0,tempResultReg[0:8]};
								3'b011: regA <= {9'b0,tempResultReg[9:17]};
								3'b100: regA <= {12'b0,tempResultReg[0:5]};
								3'b101: regA <= {12'b0,tempResultReg[6:11]};
								3'b110: regA <= {12'b0,tempResultReg[12:17]};
								3'b111: regA <= regA;
							endcase
						end
						resultToQ:
						begin
							case (r_CharAddressMode)
								3'b000: regQ <= tempResultReg;
								3'b001: regQ <= regQ;
								3'b010: regQ <= {9'b0,tempResultReg[0:8]};
								3'b011: regQ <= {9'b0,tempResultReg[9:17]};
								3'b100: regQ <= {12'b0,tempResultReg[0:5]};
								3'b101: regQ <= {12'b0,tempResultReg[6:11]};
								3'b110: regQ <= {12'b0,tempResultReg[12:17]};
								3'b111: regQ <= regQ;
							endcase
						end
						resultToX1:	regX1 <= tempResultReg;
						resultToX2:	regX2 <= tempResultReg;
						resultToX3:	regX3 <= tempResultReg;
						resultToAQ:
						begin
							regA <= tempSecondResultReg;
							regQ <= tempResultReg;
						end
						resultToX1C: regX1 <= tempSecondResultReg;
						resultToX2C: regX2 <= tempSecondResultReg;
						resultToX3C: regX3 <= tempSecondResultReg;
						resultNoCopy:
						begin
							// Do nothing here
						end
						resultToIndicators:
						begin
							zeroFlag <= tempResultReg[0];
							negativeFlag <= tempResultReg[1];
							carryFlag <= tempResultReg[2];
							overflowFlag <= tempResultReg[3];
							interruptInhibitFlag <= tempResultReg[4];
							parityFaultInhibitFlag <= tempResultReg[5];
							overflowFaultInhibitFlag <= tempResultReg[6];
							parityErrorFlag <= tempResultReg[7];
							regS <= tempResultReg[12:17];
						end
						resultToPCW:
						begin
							pcw[0:17] <= tempSecondResultReg;
							pcw[18:35] <= tempResultReg;
							connect <= HIGH;
						end
						resultNrmToA:
						begin
							tempResultReg <= i_NrmShiftCount;
							if (i_AluResultLow != 18'b0)
							begin
								regA <= i_AluResultLow;
								if (overflowFlag)
									regX1 <= regX1 + 18'b1;
								else
									regX1 <= regX1 - i_NrmShiftCount;
							end
							overflowFlag <= LOW;
						end
						resultNrmToAQ:
						begin
							if ((regA != 18'b0) || (regQ != 18'b0))
							begin
								{regA,regQ} <= {i_AluResultHigh,i_AluResultLow};
								if (overflowFlag)
									regX1 <= regX1 + 18'b1;
								else
									regX1 <= regX1 - i_AluResultLow[12:17];
							end
							overflowFlag <= LOW;
						end
						resultAluToA:
						begin
							regA <= i_AluResultLow;
							carryFlag <= i_AluCarry;
							overflowFlag <= i_AluOverflow;
							overflowFault <= i_AluOverflow && !overflowFaultInhibitFlag;
						end
						resultAluToQ:
						begin
							regQ <= i_AluResultLow;
							carryFlag <= i_AluCarry;
							overflowFlag <= i_AluOverflow;
							overflowFault <= i_AluOverflow && !overflowFaultInhibitFlag;
						end
						resultAluToAQ:
						begin
							regA <= i_AluResultHigh;
							regQ <= i_AluResultLow;
							carryFlag <= i_AluCarry;
							overflowFlag <= i_AluOverflow;
							overflowFault <= i_AluOverflow && !overflowFaultInhibitFlag;
							divideCheckFault <= i_AluDivideCheck;
						end
					endcase
					
					// Update zero and negative flags appropriately
					case (resultCopyDest)
						resultToAQ:
						begin
							zeroFlag <= (updateZeroFlag) ? ((tempResultReg | tempSecondResultReg) ? LOW : HIGH) : zeroFlag;
							negativeFlag <= (updateNegativeFlag) ? (tempSecondResultReg[0] ? HIGH : LOW) : negativeFlag;
						end
						resultToX1C: zeroFlag <= (updateZeroFlag) ? ((tempSecondResultReg) ? LOW : HIGH) : zeroFlag;
						resultToX2C: zeroFlag <= (updateZeroFlag) ? ((tempSecondResultReg) ? LOW : HIGH) : zeroFlag;
						resultToX3C: zeroFlag <= (updateZeroFlag) ? ((tempSecondResultReg) ? LOW : HIGH) : zeroFlag;
						resultToIndicators:
						begin
							// Do nothing here on purpose! Indicators are set above
						end
						resultNrmToA:
						begin
							zeroFlag <= i_AluResultLow == 18'b0;
							negativeFlag <= i_AluResultLow[0];
						end
						resultNrmToAQ:
						begin
							zeroFlag <= (i_AluResultLow | i_AluResultHigh) == 18'b0;
							negativeFlag <= i_AluResultHigh[0];
						end
						resultAluToA:
						begin
							zeroFlag <= (updateZeroFlag) ? ((i_AluResultLow) ? LOW : HIGH) : zeroFlag;
							negativeFlag <= (updateNegativeFlag) ? (i_AluResultLow[0] ? HIGH : LOW) : negativeFlag;
						end
						resultAluToQ:
						begin
							zeroFlag <= (updateZeroFlag) ? ((i_AluResultLow) ? LOW : HIGH) : zeroFlag;
							negativeFlag <= (updateNegativeFlag) ? (i_AluResultLow[0] ? HIGH : LOW) : negativeFlag;
						end
						resultAluToAQ:
						begin
							zeroFlag <= (updateZeroFlag) ? ((i_AluResultLow | i_AluResultHigh) ? LOW : HIGH) : zeroFlag;
							negativeFlag <= (updateNegativeFlag) ? (i_AluResultHigh[0] ? HIGH : LOW) : negativeFlag;
						end
						default: 
						begin
							zeroFlag <= (updateZeroFlag) ? ((tempResultReg) ? LOW : HIGH) : zeroFlag;
							negativeFlag <= (updateNegativeFlag) ? (tempResultReg[0] ? HIGH : LOW) : negativeFlag;
						end
					endcase
					
					if (updateMcOverflow)
					begin
						overflowFlag <= mcOverflow;
						overflowFault <= mcOverflow && !overflowFaultInhibitFlag;
					end
					if (updateMcCarry)
						carryFlag <= mcCarry;
					if (!i_intReq)
						lastIntReq <= LOW;
					
					singleStep <= LOW;
					
					// Perform hardware breakpoint if needed
					if (breakpoint == currentIC)
					begin
						haltFlag <= HIGH;
						cpuState <= CPU_HALT;
					end
					else
					begin
						o_IntLock <= HIGH;
						cpuState <= (haltFlag) ? CPU_HALT : (run) ? CPU_FETCH : CPU_HALT;
					end
				end
				
				CPU_PROG_LOAD:		// Load initial program into RAM from either PGM or ROM
				begin
					if (programRequest)
					begin
					
						if (singleStepRequest && !lastSingleStep)
						begin
							singleStep <= HIGH;
						end
						else 
						begin
							singleStep <= LOW;
						end
						if (run || (!run && singleStep) || (!run && effectiveAddress < 13'o2360) )
						begin
							case (programState)
								PROG_SETUP:	// Set up for write cycle
								begin
									romAddress <= effectiveAddress[2:14];
									programState <= PROG_READ;
								end
								PROG_READ:	// Read ROM data
								begin
									tempResultReg <= romData;
									programState <= PROG_WRITE;
								end
								PROG_WRITE:	// Perform write cycle for program data
								begin
									`MEMORY_WRITE(effectiveAddress,tempResultReg)
									programState <= PROG_FINISH;
								end
								PROG_FINISH:	// Complete the write cycle
								begin
									if (memReady)
									begin
										`MEMORY_DONE
										programState <= PROG_NEXT;
									end
								end
								PROG_NEXT:		// Set up for next write cycle
								begin
									if (effectiveAddress == 13'o17777)
									begin
										nextIC <= 15'o02000;		// Starting IC for the tand.mif file
										cpuState <= (run) ? CPU_FETCH : CPU_HALT;
									end
									else
									begin
										singleStep <= LOW;
										effectiveAddress <= effectiveAddress + 15'b1;
										programState <= PROG_SETUP;
									end
								end
								default: programState <= PROG_SETUP;
							endcase
						end
						lastSingleStep <= singleStepRequest;
					end
					else
					begin
						// Just going to run image in RAM
						nextIC <= 15'o00464; 	// Starting IC for the dn355_1.mif file
						cpuState <= (run) ? CPU_FETCH : CPU_HALT;
					end
				end
				
				
				CPU_HALT:			// Processor is halted
				begin
					runIndicator <= 0;
					if (singleStepRequest && !lastSingleStep)
					begin
						singleStep <= HIGH;
						lastSingleStep <= HIGH;
					end
					else if (!singleStepRequest && lastSingleStep)
					begin
						singleStep <= LOW;
						lastSingleStep <= LOW;
					end
					haltFlag <= haltFlag && run;
					if (!haltFlag && (run | singleStep))
					begin
						o_IntLock <= HIGH;
						cpuState <= CPU_FETCH;
					end
					else
					begin
						o_IntLock <= LOW;
						cpuState <= CPU_HALT;
					end
				end
				
				
				CPU_DIS:				// Disabled waiting interrupt
				begin
					singleInterruptAllowFlag <= HIGH;
					if (i_intReq)
					begin
						o_IntLock <= HIGH;
						cpuState <= CPU_FETCH;
					end
					else
					begin
						o_IntLock <= LOW;
						cpuState <= CPU_DIS;
					end
				end
				
				
				CPU_CHAR_ADD:		// Perform indexed character add
				begin
					tempSecondResultReg <= (camResult[0]) 
																	? ({camResult[1:3],($signed(tempResultReg[3:17])+15'b1)}) 
																	: ({camResult[1:3],tempResultReg[3:17]}
															);
																				
					cpuState <= CPU_FINISH;
				end
				
				CPU_ALU_START:		// Start the ALU processing
				begin
					r_AluStart <= HIGH;
					cpuState <= (i_AluReady) ? CPU_FINISH : CPU_ALU_START;
				end
				
				CPU_SPI:				// SPI Operation in progress
				begin
					if (i_SPI_RxValid)
					begin
						resultCopyDest <= resultToAQ;
						tempSecondResultReg <= i_SPI_RxData[0:17];
						tempResultReg <= i_SPI_RxData[18:35];
						o_SPI_Enable <= LOW;
						cpuState <= CPU_FINISH;
					end
					else
					begin
						cpuState <= CPU_SPI;
					end
				end
				
				CPU_RIA:
				begin
					o_IntAck <= HIGH;
					regA <= intVector | 18'o400000;
					cpuState <= CPU_FINISH;
				end
				
				default:
				begin
					diagnosticCode <= 18'o777777;
				end
			endcase
	end
endmodule
