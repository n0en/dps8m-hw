// This module simply acts as a line adapter between a 15 bit value to 18 bit value.
// The high order bits are forced to zero.
module Adapter15to18(
	input 	wire [0:14] din,
	output 	wire [0:17] dout
);

assign dout[3:17] = din;
assign dout[0:2] = 3'b000;

endmodule
