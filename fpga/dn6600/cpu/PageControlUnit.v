// This module is responsible for converting a potentially paged address into an absolute address
module PageControlUnit(
	input wire 				i_Clk,
	input wire 				i_WriteIn,
	input wire 	[14:0] 	i_AddressIn,
	input wire 	[17:0] 	i_DataIn,
	input wire 				i_Enable,
	input wire 	[15:0] 	i_PatReg,
	input wire 				i_PageMode,
	input wire 	[17:0] 	i_MemoryIn,
	input wire 				i_Reset,
	output reg 				o_WriteCycle,
	output reg 	[15:0] 	o_AddressOut,
	output reg 	[17:0] 	o_DataOut,
	output reg 				o_Ready,
	output reg 				o_Fault,
	output reg 				o_MailBoxAccess,		// Signal indicating this is a mailbox access rather than memory
	output reg 				o_TP
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// FSM States
	localparam PCU_IDLE						= 3'b000;
	localparam PCU_CHECKING_PATREG		= 3'b001;
	localparam PCU_COMPLETING_CYCLE		= 3'b010;
	localparam PCU_WAITING_ENABLE_LOW	= 3'b011;
	localparam PCU_READING_PTE				= 3'b100;
	localparam PCU_PTE_DELAY_CYCLE		= 3'b101;
	localparam PCU_RESOLVING_PTE			= 3'b110;
	localparam PCU_WAITING_FAULT_CLEAR	= 3'b111;

	reg [2:0] currentState = PCU_IDLE;
	reg [17:0] ptereg = 0;
	reg last_i_Enable = LOW;
	
	wire pte_active;
	wire pte_security;
	wire pte_read_only;
	wire [1:0] pte_bad_address;
	
	assign pte_active = ptereg[5];
	assign pte_security = ptereg[6];
	assign pte_read_only = ptereg[7];
	assign pte_bad_address = ptereg[17:16];
	
	always @(posedge i_Clk, posedge i_Reset)
	begin
		
		if (i_Reset)
		begin
			currentState <= PCU_IDLE;
			o_TP <= LOW;
		end
		else
		begin
			case (currentState)
				PCU_IDLE:
				begin
					o_Ready <= LOW;
					o_WriteCycle <= LOW;
					o_Fault <= LOW;
					if (!last_i_Enable && i_Enable) currentState <= PCU_CHECKING_PATREG;
				end
				
				PCU_CHECKING_PATREG:
				begin
					if (!i_PageMode || (i_PatReg == 0))
					begin
						o_AddressOut[14:0] <= i_AddressIn;
						o_AddressOut[15] <= LOW;
						o_DataOut <= i_DataIn;
						o_WriteCycle <= i_WriteIn;
						o_MailBoxAccess = ((i_AddressIn == 15'o00450) || (i_AddressIn == 15'o00451)) ? HIGH : LOW;
						if (i_AddressIn == 15'o00451) o_TP <= HIGH;
						currentState <= PCU_COMPLETING_CYCLE;
					end
					else
					begin
						o_AddressOut <= i_PatReg[15:0] | (i_AddressIn >> 8);
						o_WriteCycle <= LOW;
						currentState <= PCU_PTE_DELAY_CYCLE;
					end
				end
				
				PCU_PTE_DELAY_CYCLE:
				begin
					currentState <= PCU_READING_PTE;
				end
				
				PCU_COMPLETING_CYCLE:
				begin
					o_WriteCycle <= LOW;
					currentState <= PCU_WAITING_ENABLE_LOW;
				end
				
				PCU_WAITING_ENABLE_LOW:
				begin
					o_Ready <= HIGH;
					currentState <= (i_Enable) ? PCU_WAITING_ENABLE_LOW : PCU_IDLE;
				end
				
				PCU_READING_PTE:
				begin
					ptereg <= i_MemoryIn;
					currentState <= PCU_RESOLVING_PTE;
				end
				
				PCU_RESOLVING_PTE:
				begin
					if (!pte_active)
					begin
						o_AddressOut[14:0] <= i_AddressIn;
						o_AddressOut[15] <= LOW;
						o_DataOut <= i_DataIn;
						o_WriteCycle <= i_WriteIn;
						o_MailBoxAccess = ((i_AddressIn == 15'o00450) || (i_AddressIn == 15'o00451)) ? HIGH : LOW;
						currentState <= PCU_COMPLETING_CYCLE;
					end
					else if (pte_security)
					begin
						o_Fault <= HIGH;
						currentState <= PCU_WAITING_FAULT_CLEAR;
					end
					else if (pte_read_only && i_WriteIn)
					begin
						o_Fault <= HIGH;
						currentState <= PCU_WAITING_FAULT_CLEAR;
					end
					else if (pte_bad_address)
					begin
						o_Fault <= HIGH;
						currentState <= PCU_WAITING_FAULT_CLEAR;
					end
					else
					begin
						o_AddressOut <= (ptereg & 18'o777400) | (i_AddressIn & 15'o00377);
						o_DataOut <= i_DataIn;
						o_WriteCycle <= i_WriteIn;
						o_MailBoxAccess = ((((ptereg & 18'o777400) | (i_AddressIn & 15'o00377)) == 15'o00450) 
									|| (((ptereg & 18'o777400) | (i_AddressIn & 15'o00377)) == 15'o00451)) ? HIGH : LOW;
						currentState <= PCU_COMPLETING_CYCLE;
					end
				end
				
				PCU_WAITING_FAULT_CLEAR:
				begin
					o_Fault <= HIGH;
					currentState <= (i_Enable) ? PCU_WAITING_FAULT_CLEAR : PCU_IDLE;
				end
				
				default: currentState <= PCU_IDLE;
			endcase
			
			last_i_Enable <= i_Enable;
		end
	end


endmodule
