module Addder18 (
	input wire 	[0:17]	i_Addend1,
	input wire 	[0:17]	i_Addend2,
	input wire        	i_Carry,
	input wire				i_Add,
	output wire	[0:17] 	o_Sum,
	output wire       	o_Carry,
	output wire       	o_Overflow
);

	wire [0:18] w_Operand1;
	wire [0:18] w_Operand2;
	wire [0:19] w_Result;

   assign w_Operand1 = { i_Addend1[0], i_Addend1 };
	assign w_Operand2 = { i_Addend2[0], i_Addend2 };
	assign w_Result = (i_Add) ? (w_Operand1 + w_Operand2 + i_Carry)
									  : (w_Operand1 - w_Operand2 - i_Carry);
	assign o_Sum = w_Result[2:19];
	assign o_Carry = (i_Add) ? w_Result[0] : !w_Result[0];
	assign o_Overflow = w_Result[1] ^ w_Result[2];
	
endmodule