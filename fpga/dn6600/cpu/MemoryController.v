// This module is the memory controller used to support the CPU in memory operations
module MemoryController (
	// General I/O
	input wire 				clk,						// System clock
	input wire 				reset,					// System reset signal
	output reg 	[0:3] 	controllerState,		// Current MEMORY cycle state
	
	// I/O between Controller and CPU
	input wire 				start,					// Initiate current mop on rising edge
	input wire 	[0:2] 	mop,						// Memory Operation
	input wire 	[0:3] 	rmwOp,					// Specific operation to carry out when doing read/modify/write cycle
	input wire 	[0:14] 	ain,						// Address input
	input wire 	[0:35] 	din,						// Double word input for writing to memory
	input wire 	[0:17] 	regA,						// The current value in the A register (used in read/modify/write operations)
	input wire 	[0:17] 	regQ,						// The current value in the Q register (used in read/modify/write operations)
	input wire 	[0:2] 	i_CharAddressMode,	// Mode select for writing character positions in memory word
	output reg 	[0:35] 	dout,						// Double word memory read results
	output reg 				fault,					// Flag indicating memory fault has occurred
	output reg 				ready,					// Flag indicating operation is complete
	
	// I/O between Controller and Memory Module
	input wire 				memReady,				// Memory operation complete signal
	input wire 	[0:17] 	memReadData,			// Data read from memory on read cycle
	output reg 				memChipSelect, 		// Chip select signal for selecting the RAM module
	output reg 				memWriteEnable,		// Write / not Read signal for the memory cycle
	output reg 	[0:14]	memAddress, 			// The memory address the controller wants to read or write
	output reg 	[0:17] 	memWriteData, 			// Data to write on memory write cycle
	output reg 				carryFlag,				// Carry flag from read/modify/write cycle
	output reg 				overflowFlag,			// Overflow flag from read/modify/write cycle
	input wire 				memFault,				// Memory module indicated fault
	
	// 18 bit Adder interface
	input wire	[0:17] 	i_Sum18,					// Sum from the 18 bit adder
	input wire       		i_Carry18,				// Carry from the 18 bit adder
	input wire       		i_Overflow18,			// Overflow indicator from the 18 bit adder
	output reg 	[0:17]	o_Addend1,				// First number for adding to 18 bit adder
	output reg	[0:17]	o_Addend2,				// Second number for adding to 18 bit adder
	output reg	       	o_Carry18,				// Carry for adding to 18 bit adder
	output reg				o_Add18					// Add / Not subtract for 18 bit adder

);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// Memory Operation Requested
	`define MOP_NOP					3'b000
	`define MOP_READ_WORD			3'b001
	`define MOP_READ_DOUBLE_WORD	3'b010
	`define MOP_WRITE_WORD			3'b011
	`define MOP_WRITE_DOUBLE_WORD	3'b100
	`define MOP_READ_MOD_WRITE		3'b101

	// Controller cycle state
	`define MC_IDLE						4'b0000
	`define MC_WRITING_DOUBLE_WORD	4'b0001
	`define MC_FINISH_FIRST_WRITE		4'b0010
	`define MC_START_SECOND_WRITE		4'b0011
	`define MC_WRITING_WORD				4'b0100
	`define MC_FINISH_WRITE				4'b0101
	`define MC_READING_DOUBLE_WORD	4'b0110
	`define MC_FINISH_FIRST_READ		4'b0111
	`define MC_STARTING_SECOND_READ	4'b1000
	`define MC_READING_WORD				4'b1001
	`define MC_FINISHING_READ			4'b1010
	`define MC_RMW_READING				4'b1011
	`define MC_RMW_MODIFYING			4'b1100
	`define MC_RMW_MOD2					4'b1101
	`define MC_RMW_WAITING				4'b1110
	`define MC_RMW_WRITING				4'b1111
		
	// Read/Modify/Write Operation Codes
	`define RMW_NOP				4'b0001
	`define RMW_ADD_A				4'b0001
	`define RMW_ADD_ONE			4'b0010
	`define RMW_AND_A				4'b0011
	`define RMW_OR_A				4'b0100
	`define RMW_EOR_A				4'b0101
	`define RMW_SUB_A				4'b0110
	`define RMW_WRITE_CHAR_A	4'b0111
	`define RMW_WRITE_CHAR_Q	4'b1000

	// Macros to simplify high/low word addressing
	`define DIN_LOW 	din[18:35]
	`define DIN_HIGH	din[0:17]
	`define DOUT_LOW	dout[18:35]
	`define DOUT_HIGH	dout[0:17]
	
	// Internal storage
	reg lastStart;

	always @ (posedge(clk), posedge(reset))
	begin
		if (reset)
		begin
			controllerState <= `MC_IDLE;
			lastStart <= LOW;
			ready <= LOW;
			memAddress <= 15'b0;
			memWriteData <= 18'b0;
			memWriteEnable <= LOW;
			memChipSelect <= LOW;
			carryFlag <= LOW;
			overflowFlag <= LOW;
			o_Carry18 <= LOW;
			o_Add18 <= HIGH;
		end
		else
		begin
			case (controllerState)
				`MC_IDLE:
				begin
					// Drop our ready line when start goes away
					if (!start)
						ready <= LOW;
						
					// Check if we have a start signal rising edge
					if (!lastStart & start)
					begin
						case (mop)
							`MOP_NOP:
							begin
								memAddress <= ain;
								memWriteData <= 18'b0;
								memWriteEnable <= LOW;
								memChipSelect <= LOW;
								controllerState <= `MC_IDLE;
								fault <= LOW;
							end
							`MOP_READ_WORD:
							begin
								memAddress <= ain;
								memWriteData <= 15'b0;
								memWriteEnable <= LOW;
								memChipSelect <= HIGH;
								controllerState <= `MC_READING_WORD;
								fault <= LOW;
							end
							`MOP_READ_DOUBLE_WORD:
							begin
								memAddress <= ain & 15'o77776;
								memWriteData <= 15'b0;
								memWriteEnable <= LOW;
								memChipSelect <= HIGH;
								controllerState <= `MC_READING_DOUBLE_WORD;
								fault <= LOW;
							end
							`MOP_WRITE_WORD:
							begin
								memAddress <= ain;
								memWriteData <= `DIN_LOW;
								memWriteEnable <= HIGH;
								memChipSelect <= HIGH;
								controllerState <= `MC_WRITING_WORD;
								fault <= LOW;
							end
							`MOP_WRITE_DOUBLE_WORD:
							begin
								memAddress <= ain & 15'o77776;
								memWriteData <= `DIN_HIGH;
								memWriteEnable <= HIGH;
								memChipSelect <= HIGH;
								controllerState <= `MC_WRITING_DOUBLE_WORD;
								fault <= LOW;
							end
							`MOP_READ_MOD_WRITE:
							begin
								memAddress <= ain;
								memWriteData <= 15'b0;
								memWriteEnable <= LOW;
								memChipSelect <= HIGH;
								controllerState <= `MC_RMW_READING;
								fault <= LOW;
							end
							default:
							begin
								memAddress <= 15'b0;
								memWriteData <= 15'b0;
								memWriteEnable <= LOW;
								memChipSelect <= HIGH;
								controllerState <= `MC_IDLE;
								fault <= HIGH;
							end
						endcase
					end
					lastStart <= start;
				end
				
				`MC_WRITING_DOUBLE_WORD:
				begin
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_FINISH_FIRST_WRITE : `MC_WRITING_DOUBLE_WORD;
				end
				
				`MC_FINISH_FIRST_WRITE:
				begin
					memWriteEnable <= LOW;
					memChipSelect <= LOW;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_FINISH_FIRST_WRITE : `MC_START_SECOND_WRITE;
				end
				
				`MC_START_SECOND_WRITE:
				begin
					memAddress <= ain | 15'b1;
					memWriteData <= `DIN_LOW;
					memWriteEnable <= HIGH;
					memChipSelect <= HIGH;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= `MC_WRITING_WORD;
				end
				
				`MC_WRITING_WORD:
				begin
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_FINISH_WRITE : `MC_WRITING_WORD;
				end
				
				`MC_FINISH_WRITE:
				begin
					memWriteEnable <= LOW;
					memChipSelect <= LOW;
					ready <= HIGH;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= `MC_IDLE;
//					controllerState <= (memReady) ? `MC_FINISH_WRITE : `MC_IDLE;
				end
				
				`MC_READING_DOUBLE_WORD:
				begin
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_FINISH_FIRST_READ : `MC_READING_DOUBLE_WORD;
				end
				
				`MC_FINISH_FIRST_READ:
				begin
					`DOUT_HIGH <= memReadData;
					memChipSelect <= LOW;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_FINISH_FIRST_READ : `MC_STARTING_SECOND_READ;
				end
				
				`MC_STARTING_SECOND_READ:
				begin
					memAddress <= ain | 15'b1;
					memChipSelect <= HIGH;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= `MC_READING_WORD;
				end
				
				`MC_READING_WORD:
				begin
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_FINISHING_READ : `MC_READING_WORD;
				end
				
				`MC_FINISHING_READ:
				begin
					`DOUT_LOW <= memReadData;
					memChipSelect <= LOW;
					ready <= HIGH;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= `MC_IDLE;
//					controllerState <= (memReady) ? `MC_FINISHING_READ : `MC_IDLE;
				end
				
				`MC_RMW_READING:
				begin
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_RMW_MODIFYING : `MC_RMW_READING;
				end
				
				`MC_RMW_MODIFYING:
				begin
					memChipSelect <= LOW;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else
					begin
						case (rmwOp)
							`RMW_ADD_A:
							begin
								o_Addend1 <= regA;
								o_Addend2 <= memReadData;
								o_Add18 <= HIGH;
								controllerState <= `MC_RMW_MOD2;
							end
							`RMW_ADD_ONE:
							begin
								o_Addend1 <= 18'b1;
								o_Addend2 <= memReadData;
								o_Add18 <= HIGH;
								controllerState <= `MC_RMW_MOD2;
							end
							`RMW_AND_A:
							begin
								memWriteData <= memReadData & regA;
								`DOUT_LOW <= memReadData & regA;
								controllerState <= (memReady) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_OR_A:
							begin
								memWriteData <= memReadData | regA;
								`DOUT_LOW <= memReadData | regA;
								controllerState <= (memReady) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_EOR_A:
							begin
								memWriteData <= memReadData ^ regA;
								`DOUT_LOW <= memReadData ^ regA;
								controllerState <= (memReady) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_SUB_A:
							begin
								o_Addend1 <= regA;
								o_Addend2 <= memReadData;
								o_Add18 <= LOW;
								controllerState <= `MC_RMW_MOD2;
							end
							`RMW_WRITE_CHAR_A:
							begin
								case (i_CharAddressMode)
									3'b000: `DOUT_LOW <= memReadData;
									3'b001: `DOUT_LOW <= `DOUT_LOW;
									3'b010: `DOUT_LOW <= {regA[9:17],memReadData[9:17]};
									3'b011: `DOUT_LOW <= {memReadData[0:8],regA[9:17]};
									3'b100: `DOUT_LOW <= {regA[12:17],memReadData[6:17]};
									3'b101: `DOUT_LOW <= {memReadData[0:5],regA[12:17],memReadData[12:17]};
									3'b110: `DOUT_LOW <= {memReadData[0:11],regA[12:17]};
									3'b111: `DOUT_LOW <= `DOUT_LOW;
								endcase
								controllerState <= (memReady) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_WRITE_CHAR_Q:
							begin
								case (i_CharAddressMode)
									3'b000: `DOUT_LOW <= memReadData;
									3'b001: `DOUT_LOW <= `DOUT_LOW;
									3'b010: `DOUT_LOW <= {regQ[9:17],memReadData[9:17]};
									3'b011: `DOUT_LOW <= {memReadData[0:8],regQ[9:17]};
									3'b100: `DOUT_LOW <= {regQ[12:17],memReadData[6:17]};
									3'b101: `DOUT_LOW <= {memReadData[0:5],regQ[12:17],memReadData[12:17]};
									3'b110: `DOUT_LOW <= {memReadData[0:11],regQ[12:17]};
									3'b111: `DOUT_LOW <= `DOUT_LOW;
								endcase
								controllerState <= (memReady) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							default: 
							begin
								fault <= HIGH;
								controllerState <= `MC_IDLE;
							end
						endcase
					end
				end
				
				`MC_RMW_MOD2:
				begin
					memWriteData <= i_Sum18;
					`DOUT_LOW <= i_Sum18;
					carryFlag <= i_Carry18;
					overflowFlag <= i_Overflow18;
					controllerState <= (memReady) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
				end
				
				`MC_RMW_WAITING:
				begin
					controllerState <= (memReady) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
				end
				
				`MC_RMW_WRITING:
				begin
					memWriteEnable <= HIGH;
					memChipSelect <= HIGH;
					if (memFault) 
					begin
						fault <= HIGH;
						controllerState <= `MC_IDLE;
					end
					else controllerState <= (memReady) ? `MC_FINISH_WRITE : `MC_RMW_WRITING;
				end
				
				default: 
				begin
					fault <= HIGH;
					controllerState <= `MC_IDLE;
				end
				
			endcase
		end
	end
endmodule
