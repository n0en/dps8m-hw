// This module simply acts as a line adapter between the various CPU states to 18 bit value.
module CpuStateAdapter(
	input wire [0:3] cpuState,
	input wire [0:1] fetchState,
	input wire [0:1] resolveState,
	input wire [0:3] memoryState,
	input wire [0:2] programState,
	output wire [0:17] dout
);

assign dout[0:1] = 2'b0;
assign dout[2:5] = cpuState;
assign dout[6] = 1'b0;
assign dout[7:8] = resolveState;
assign dout[9:10] = 2'b0;
assign dout[11:14] = memoryState;
assign dout[15:17] = programState;

endmodule
