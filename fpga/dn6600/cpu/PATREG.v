module PATREG(
	input wire [15:0] ain,
	input wire [17:0] din,
	input wire write,
	input wire reset,
	output reg [15:0] dout
);

	initial
	begin
		dout = 16'b0;
	end

	always @ (posedge write, posedge reset)
	begin
		if (reset)
			dout <= 16'o000000;
		else if (ain == 16'o000475)
			dout <= din[15:0];
		else
			dout <= dout;
	end

endmodule