// This module simply acts as a line adapter between a 36 bit value to two 18 bit values.
module Adapter36to2x18(
	input wire [0:35] din,
	output wire [0:17] dout0,
	output wire [0:17] dout1
);

assign dout0 = din[0:17];
assign dout1 = din[18:35];

endmodule
