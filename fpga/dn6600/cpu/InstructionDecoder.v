// InstructionDecoder: Break up the current instruction based on the op code
`define GROUP_0 2'b00
`define  GROUP_1 2'b01
`define  GROUP_2 2'b10

module InstructionDecoder(
	input wire [0:17] instruction,
	output wire [0:1] opCodeGroup,	// Indicates the op code group number based on the primary op code (0, 1 or 2)
	output wire [0:5] opCode,
	output wire indirectFlag,			// When true indicates that indirect addressing is being done
	output wire [0:1] tag,				// For group 0 op codes, contains the tag field
	output wire [0:8] displacement,	// For group 0 and 1 op codes, contains the displacement field
	output wire [0:2] s1,				// For group 1 and 2 op codes, contains the S1 field
	output wire [0:2] s2,				// For group 2 op codes, contains the S2 field
	output wire [0:5] k,					// For group 2 op codes, contains the K field
	output wire [0:35] alsMask,		// Special mask for ALS, QLS and LLS instructions based on the K field
	output wire [0:17] alpMask			// Special mask for ALP and QLP instructions based on the K field
);

	// Function to generate mask for ALS instruction based on shift count.
	// This mask is used to determine if the carry flag should be set or not.
   function automatic [0:35] getALSmask;
      input [0:5] count; 
      case (count)
			0: getALSmask = 36'b000000000000000000000000000000000000;
			1: getALSmask = 36'b110000000000000000000000000000000000;
			2: getALSmask = 36'b111000000000000000000000000000000000;
			3: getALSmask = 36'b111100000000000000000000000000000000;
			4: getALSmask = 36'b111110000000000000000000000000000000;
			5: getALSmask = 36'b111111000000000000000000000000000000;
			6: getALSmask = 36'b111111100000000000000000000000000000;
			7: getALSmask = 36'b111111110000000000000000000000000000;
			8: getALSmask = 36'b111111111000000000000000000000000000;
			9: getALSmask = 36'b111111111100000000000000000000000000;
			10: getALSmask = 36'b111111111110000000000000000000000000;
			11: getALSmask = 36'b111111111111000000000000000000000000;
			12: getALSmask = 36'b111111111111100000000000000000000000;
			13: getALSmask = 36'b111111111111110000000000000000000000;
			14: getALSmask = 36'b111111111111111000000000000000000000;
			15: getALSmask = 36'b111111111111111100000000000000000000;
			16: getALSmask = 36'b111111111111111110000000000000000000;
			17: getALSmask = 36'b111111111111111111000000000000000000;
			18: getALSmask = 36'b111111111111111111100000000000000000;
			19: getALSmask = 36'b111111111111111111110000000000000000;
			20: getALSmask = 36'b111111111111111111111000000000000000;
			21: getALSmask = 36'b111111111111111111111100000000000000;
			22: getALSmask = 36'b111111111111111111111110000000000000;
			23: getALSmask = 36'b111111111111111111111111000000000000;
			24: getALSmask = 36'b111111111111111111111111100000000000;
			25: getALSmask = 36'b111111111111111111111111110000000000;
			26: getALSmask = 36'b111111111111111111111111111000000000;
			27: getALSmask = 36'b111111111111111111111111111100000000;
			28: getALSmask = 36'b111111111111111111111111111110000000;
			29: getALSmask = 36'b111111111111111111111111111111000000;
			30: getALSmask = 36'b111111111111111111111111111111100000;
			31: getALSmask = 36'b111111111111111111111111111111110000;
			32: getALSmask = 36'b111111111111111111111111111111111000;
			33: getALSmask = 36'b111111111111111111111111111111111100;
			34: getALSmask = 36'b111111111111111111111111111111111110;
			default: getALSmask = 36'b111111111111111111111111111111111111;
		endcase
	endfunction
	
	// Function to generate mask for ALP and QLP instruction based on shift count.
	// This mask is used to determine if the carry flag should be set or not.
   function automatic [0:17] getALPmask;
      input [0:5] count; 
      case (count)
			0: getALPmask = 18'b000000000000000000;
			1: getALPmask = 18'b100000000000000000;
			2: getALPmask = 18'b110000000000000000;
			3: getALPmask = 18'b111000000000000000;
			4: getALPmask = 18'b111100000000000000;
			5: getALPmask = 18'b111110000000000000;
			6: getALPmask = 18'b111111000000000000;
			7: getALPmask = 18'b111111100000000000;
			8: getALPmask = 18'b111111110000000000;
			9: getALPmask = 18'b111111111000000000;
			10: getALPmask = 18'b111111111100000000;
			11: getALPmask = 18'b111111111110000000;
			12: getALPmask = 18'b111111111111000000;
			13: getALPmask = 18'b111111111111100000;
			14: getALPmask = 18'b111111111111110000;
			15: getALPmask = 18'b111111111111111000;
			16: getALPmask = 18'b111111111111111100;
			17: getALPmask = 18'b111111111111111110;
			default: getALPmask = 18'b111111111111111111;
		endcase
	endfunction



	assign opCode = instruction[3:8];
	
	GroupDecoder groupDecoder1(opCode, opCodeGroup);

	assign indirectFlag 		= instruction[0] && (opCodeGroup == `GROUP_0);
	assign tag 					= (opCodeGroup == `GROUP_0) ? instruction[1:2] : 2'b00;
	assign displacement 		= (opCodeGroup != `GROUP_2) ? instruction[9:17] : 9'b0;
	assign s1 					= ((opCodeGroup == `GROUP_1) || (opCodeGroup == `GROUP_2)) ? instruction[0:2] : 3'b0;
	assign s2 					= (opCodeGroup == `GROUP_2) ? instruction[9:11] : 3'b0;
	assign k 					= (opCodeGroup == `GROUP_2) ? instruction[12:17] : 3'b0;
	assign alsMask 			= getALSmask((opCodeGroup == `GROUP_2) ? instruction[12:17] : 3'b0);
	assign alpMask 			= getALPmask((opCodeGroup == `GROUP_2) ? instruction[12:17] : 3'b0);

endmodule

// GroupDecoder: Determine the operation code group based on the primary op code.
module GroupDecoder(
	input [0:5] primaryOpCode,
	output reg [0:1] groupNumber
);

always @ (primaryOpCode)
	case (primaryOpCode)
		6'o33: groupNumber <= 2'b10;
		6'o12: groupNumber <= 2'b01;
		6'o22: groupNumber <= 2'b01;
		6'o52: groupNumber <= 2'b01;
		6'o73: groupNumber <= 2'b01;
		default: groupNumber <= 2'b00;
	endcase

endmodule
