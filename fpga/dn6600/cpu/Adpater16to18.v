// This module simply acts as a line adapter between a 16 bit value to 18 bit value.
// The high order bits are forced to zero.
module Adapter16to18(
	input wire [15:0] din,
	output wire [17:0] dout
);

assign dout[15:0] = din;
assign dout[17:16] = 3'b00;

endmodule
