// PGM: This module is used to define a program for the DN355 that is loaded on reset

// Using the simple assembler:
//  The simple assembler is really just a bunch of macros used to "assemble" instructions
//  into their in-memory format. There is a significant example of this later in the code
//  that is used to provide some simple T&D confidence tests in the CPU.
//
// The CPU instructions are broken down into three different groups (as can be seen in
// the DD01 manual). 

// Group 0 instructions are memory reference instructions and need to
// have several additional parameters specified. For example, a transfer instruction:
//      `TRA	`DIR	`tIC	2	`_	
// In the above instruction you first see the opcode "TRA". This is preceeded by a back-tick (`)
// which is used in Verilog to signify the invocation of a macro. All assembler statements must
// start with an opcode. Also note that every line must end with a `_ to properly generate code.

// In the case of the group 0 memory reference there are two additional fields required: 
// a direct/indirect indicator of "DIR" or "IND" and a tag of "tIC", "tX1", "tX2", or "tX3".
// The direct/indirect indicator determines if direct or indirect addressing will be used.
// The tag determines what base for the relative displacement will be. Please see DD01 for more
// details on these fields. The final field is the "displacement" field. Again see DD01.

// For group 1 and 2 instructions only the opcode and a parameter (either Y or K depending on the
// instruction group) will be present.

// There is also a "DATA" opcode that simply places the 18 bit parameter in the current location.

// This CPU implementation also has a special Group 0 opcode "DIAG". This opcode should always
// specify `DIR and `tIC. When executed, the displacement field will be loaded into the DIAG
// register and the CPU will be halted. This is used extensively for T&Ds to identify when a
// test failure has occurred.

`define DATA asmData(

`define DIR 1'o0,
`define IND 1'o1,

`define tIC 2'o0,
`define tX1 2'o1,
`define tX2 2'o2,
`define tX3 2'o3,

`define DIAG asmG0(6'o00,
`define MPF asmG0(6'o01,
`define ADCX2 asmG0(6'o02,
`define LDX2 asmG0(6'o03,
`define LDAQ asmG0(6'o04,
`define ADA asmG0(6'o06,
`define LDA asmG0(6'o07,
`define TSY asmG0(6'o10,
`define STX2 asmG0(6'o13,
`define STAQ asmG0(6'o14,
`define ADAQ asmG0(6'o15,
`define ASA asmG0(6'o16,
`define STA asmG0(6'o17,
`define SZN asmG0(6'o20,
`define DVF asmG0(6'o21,
`define CMPX2 asmG0(6'o23,
`define SBAQ asmG0(6'o24,
`define SBA asmG0(6'o26,
`define CMPA asmG0(6'o27,
`define LDEX asmG0(6'o30,
`define CANA asmG0(6'o31,
`define ANSA asmG0(6'o32,
`define ANA asmG0(6'o34,
`define ERA asmG0(6'o35,
`define SSA asmG0(6'o36,
`define ORA asmG0(6'o37,
`define ADCX3 asmG0(6'o40,
`define LDX3 asmG0(6'o41,
`define ADCX1 asmG0(6'o42,
`define LDX1 asmG0(6'o43,
`define LDI asmG0(6'o44,
`define TNC asmG0(6'o45,
`define ADQ asmG0(6'o46,
`define LDQ asmG0(6'o47,
`define STX3 asmG0(6'o50,
`define STX1 asmG0(6'o53,
`define STI asmG0(6'o54,
`define TOV asmG0(6'o55,
`define STZ asmG0(6'o56,
`define STQ asmG0(6'o57,
`define CIOC asmG0(6'o60,
`define CMPX3 asmG0(6'o61,
`define ERSA asmG0(6'o62,
`define CMPX1 asmG0(6'o63,
`define TNZ asmG0(6'o64,
`define TPL asmG0(6'o65,
`define SBQ asmG0(6'o66,
`define CMPQ asmG0(6'o67,
`define STEX asmG0(6'o70,
`define TRA asmG0(6'o71,
`define ORSA asmG0(6'o72,
`define TZE asmG0(6'o74,
`define TMI asmG0(6'o75,
`define AOS asmG0(6'o76,

`define RIER asmG1(3'o0,6'o12,9'o0
`define RIA asmG1(3'o4,6'o12,9'o0
`define IANA asmG1(3'o0,6'o22,
`define IORA asmG1(3'o1,6'o22,
`define ICANA asmG1(3'o2,6'o22,
`define IERA asmG1(3'o3,6'o22,
`define ICMPA asmG1(3'o4,6'o22,
`define SIER asmG1(3'o0,6'o52,9'o0
`define SIC asmG1(3'o4,6'o52,
`define SEL asmG1(3'o0,6'o73,
`define IACX1 asmG1(3'o1,6'o73,
`define IACX2 asmG1(3'o2,6'o73,
`define IACX3 asmG1(3'o3,6'o73,
`define ILQ asmG1(3'o4,6'o73,
`define IAQ asmG1(3'o5,6'o73,
`define ILA asmG1(3'o6,6'o73,
`define IAA asmG1(3'o7,6'o73,

`define CAX2 asmG2(3'o0,6'o33,3'o2,0
`define LLS  asmG2(3'o0,6'o33,3'o4,
`define LRS  asmG2(3'o0,6'o33,3'o5,
`define ALS  asmG2(3'o0,6'o33,3'o6,
`define ARS  asmG2(3'o0,6'o33,3'o7,
`define NRML asmG2(3'o1,6'o33,3'o4,
`define NRM  asmG2(3'o1,6'o33,3'o6,
`define NOP  asmG2(3'o2,6'o33,3'o1,
`define CX1A asmG2(3'o2,6'o33,3'o2,0
`define LLR  asmG2(3'o2,6'o33,3'o4,
`define LRL  asmG2(3'o2,6'o33,3'o5,
`define ALR  asmG2(3'o2,6'o33,3'o6,
`define ARL  asmG2(3'o2,6'o33,3'o7,
`define INH  asmG2(3'o3,6'o33,3'o1,0
`define CX2A asmG2(3'o3,6'o33,3'o2,0
`define CX3A asmG2(3'o3,6'o33,3'o3,0
`define ALP  asmG2(3'o3,6'o33,3'o6,
`define DIS  asmG2(3'o4,6'o33,3'o1,0
`define CAX1 asmG2(3'o4,6'o33,3'o2,0
`define CAX3 asmG2(3'o4,6'o33,3'o3,0
`define QLS  asmG2(3'o4,6'o33,3'o6,
`define QRS  asmG2(3'o4,6'o33,3'o7,
`define CAQ  asmG2(3'o6,6'o33,3'o3,0
`define QLR  asmG2(3'o6,6'o33,3'o6,
`define QRL  asmG2(3'o6,6'o33,3'o7,
`define ENI  asmG2(3'o7,6'o33,3'o1,0
`define CQA  asmG2(3'o7,6'o33,3'o3,0
`define QLP  asmG2(3'o7,6'o33,3'o6,


`define _ );

module CpuProgrammer2 (
	input wire clk, 
	input wire reset,
	input wire [9:0] address,
	output reg [17:0] data,
	output reg loadComplete
);

	integer IP;
	reg [17:0] progMem [0:1023];
	
	task asmData ();
		input [17:0] DATA;
		progMem[IP] <= DATA;
		IP = IP + 1;
	endtask
	
	task asmG0 ();
		input [5:0] OP;
		input I;
		input [1:0] T;
		input [8:0] D;
		progMem[IP]	<= (I << 17) | (T << 15) | (OP << 9) | D;
		IP = IP + 1;
	endtask
	
	task asmG1 ();
		input [2:0] S1;
		input [5:0] OP;
		input [8:0] Y;
		progMem[IP]	<= (S1 << 15) | (OP << 9) | Y;
		IP = IP + 1;
	endtask

	task asmG2 ();
		input [2:0] S1;
		input [5:0] OP;
		input [2:0] S2;
		input [5:0] K;
		progMem[IP]	<= (S1 << 15) | (OP << 9) | (S2 << 6) | K;
		IP = IP + 1;
	endtask

	always @ (address, reset)
	begin
		if (reset)
		begin
			loadComplete <= 1'b0;
			IP = 0;
			// ********************************
			// *** Page Control Unit Tester ***
			// ********************************
			// This program is designed to do some basic testing of the Page Control Unit
			// that is responsible for providing address translation for access to more than
			// 32 Kw of memory.
			//
			// The program will run until it encounters a problem or finishes.
			// If a problem is encountered, the "special" diagnostic halt is executed
			// with a failure code indicating the point in the test the failure occurred.
			//
			// The DIAG op code is really just special handling of the illegal op code 0.
			// When executed, the displacement field is loaded into the DIAG register
			// and the CPU is placed in a halted state.
			//
			// The diagnostic code 0 is not used and octal 777 indicates test completed successfully.
			// ***************************************************
			
			// Set up the page table entries (page table at 000500
			`TRA	`DIR	`tIC	9'o004		`_			// 0000 Skip over data area
			`DATA					18'o001000	`_			// 0001 Address where we will store our PAT
			`DATA					18'o004440	`_			// 0002 PTE entry for page 3
			`DATA					18'o000475	`_			// 0003 Address of the actual PAT pointer register
			
			`LDX1	`DIR	`tIC	9'o775		`_			// 0004 Get address of page table into X1
			`LDX2	`DIR	`tIC	9'o776		`_			// 0005 Get address of page table pointer into X2
			`ILA					0				`_			// 0006 Initialize page zero entry to zero so it's inactive
			`STA	`DIR	`tX1	0				`_			// 0007 (to 1000)
			`IACX1				1				`_			// 0010 Point to 1001
			`STA	`DIR	`tX1	0				`_			// 0011 (to 1001)
			`IACX1				1				`_			// 0012 Point to 1002
			`STA	`DIR	`tX1	0				`_			// 0013 (to 1002)
			`IACX1				1				`_			// 0014 Point to 1003

			`LDA	`DIR	`tIC	9'o765		`_			// 0015 Get PTE for 1003 (from 0002)
			`STA	`DIR	`tX1	0				`_			// 0016 (to 1003)
			
			`LDA	`DIR	`tIC	9'o762		`_			// 0017 Set PAT
			`STA	`DIR	`tX2	0				`_			// 0020 (to 475)
			
			`ILA					9'o432		`_			// 0021 Flag value to write to start of page window
			`LDX1	`DIR	`tIC	9'o002		`_			// 0022 X1 points to start of page window
			`TRA	`DIR	`tIC	9'o002		`_			// 0023 Skip data word
			`DATA					18'o001400	`_			// 0024 Pointer to start of page window
			`STA	`DIR	`tX1	0				`_			// 0025 Write to first word of page window
			`LDX2	`DIR	`tIC	9'o002		`_			// 0026 X2 points to start of real memory
			`TRA	`DIR	`tIC	9'o002		`_			// 0027 Skip data word
			`DATA					18'o004400	`_			// 0030 Pointer to start of real memory
			`CMPA	`DIR	`tX2	0				`_			// 0031 Check the value
			`TZE	`DIR	`tIC	2				`_			// 0032 Skip diag on values equal (Failure = 001)
			`DIAG	`DIR	`tIC	9'o001		`_			// 0033 Values did not match
			
			// ** Halt with a success code of 777 ***
			`DIAG	`DIR	`tIC	9'o777		`_
		end
		else
		begin
			data <= progMem[address];
			loadComplete <= ((address + 1) >= IP) ? 1'b1 : 1'b0;
		end
	end

endmodule
