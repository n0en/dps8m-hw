// PGM: This module is used to define a program for the DN355 that is loaded on reset

// Using the simple assembler:
//  The simple assembler is really just a bunch of macros used to "assemble" instructions
//  into their in-memory format. There is a significant example of this later in the code
//  that is used to provide some simple T&D confidence tests in the CPU.
//
// The CPU instructions are broken down into three different groups (as can be seen in
// the DD01 manual). 

// Group 0 instructions are memory reference instructions and need to
// have several additional parameters specified. For example, a transfer instruction:
//      `TRA	`DIR	`tIC	2	`_	
// In the above instruction you first see the opcode "TRA". This is preceeded by a back-tick (`)
// which is used in Verilog to signify the invocation of a macro. All assembler statements must
// start with an opcode. Also note that every line must end with a `_ to properly generate code.

// In the case of the group 0 memory reference there are two additional fields required: 
// a direct/indirect indicator of "DIR" or "IND" and a tag of "tIC", "tX1", "tX2", or "tX3".
// The direct/indirect indicator determines if direct or indirect addressing will be used.
// The tag determines what base for the relative displacement will be. Please see DD01 for more
// details on these fields. The final field is the "displacement" field. Again see DD01.

// For group 1 and 2 instructions only the opcode and a parameter (either Y or K depending on the
// instruction group) will be present.

// There is also a "DATA" opcode that simply places the 18 bit parameter in the current location.

// This CPU implementation also has a special Group 0 opcode "DIAG". This opcode should always
// specify `DIR and `tIC. When executed, the displacement field will be loaded into the DIAG
// register and the CPU will be halted. This is used extensively for T&Ds to identify when a
// test failure has occurred.

`define DATA asmData(

`define DIR 1'o0,
`define IND 1'o1,

`define tIC 2'o0,
`define tX1 2'o1,
`define tX2 2'o2,
`define tX3 2'o3,

`define DIAG asmG0(6'o00,
`define MPF asmG0(6'o01,
`define ADCX2 asmG0(6'o02,
`define LDX2 asmG0(6'o03,
`define LDAQ asmG0(6'o04,
`define ADA asmG0(6'o06,
`define LDA asmG0(6'o07,
`define TSY asmG0(6'o10,
`define STX2 asmG0(6'o13,
`define STAQ asmG0(6'o14,
`define ADAQ asmG0(6'o15,
`define ASA asmG0(6'o16,
`define STA asmG0(6'o17,
`define SZN asmG0(6'o20,
`define DVF asmG0(6'o21,
`define CMPX2 asmG0(6'o23,
`define SBAQ asmG0(6'o24,
`define SBA asmG0(6'o26,
`define CMPA asmG0(6'o27,
`define LDEX asmG0(6'o30,
`define CANA asmG0(6'o31,
`define ANSA asmG0(6'o32,
`define ANA asmG0(6'o34,
`define ERA asmG0(6'o35,
`define SSA asmG0(6'o36,
`define ORA asmG0(6'o37,
`define ADCX3 asmG0(6'o40,
`define LDX3 asmG0(6'o41,
`define ADCX1 asmG0(6'o42,
`define LDX1 asmG0(6'o43,
`define LDI asmG0(6'o44,
`define TNC asmG0(6'o45,
`define ADQ asmG0(6'o46,
`define LDQ asmG0(6'o47,
`define STX3 asmG0(6'o50,
`define STX1 asmG0(6'o53,
`define STI asmG0(6'o54,
`define TOV asmG0(6'o55,
`define STZ asmG0(6'o56,
`define STQ asmG0(6'o57,
`define CIOC asmG0(6'o60,
`define CMPX3 asmG0(6'o61,
`define ERSA asmG0(6'o62,
`define CMPX1 asmG0(6'o63,
`define TNZ asmG0(6'o64,
`define TPL asmG0(6'o65,
`define SBQ asmG0(6'o66,
`define CMPQ asmG0(6'o67,
`define STEX asmG0(6'o70,
`define TRA asmG0(6'o71,
`define ORSA asmG0(6'o72,
`define TZE asmG0(6'o74,
`define TMI asmG0(6'o75,
`define AOS asmG0(6'o76,

`define RIER asmG1(3'o0,6'o12,9'o0
`define RIA asmG1(3'o4,6'o12,9'o0
`define IANA asmG1(3'o0,6'o22,
`define IORA asmG1(3'o1,6'o22,
`define ICANA asmG1(3'o2,6'o22,
`define IERA asmG1(3'o3,6'o22,
`define ICMPA asmG1(3'o4,6'o22,
`define SIER asmG1(3'o0,6'o52,9'o0
`define SIC asmG1(3'o4,6'o52,
`define SEL asmG1(3'o0,6'o73,
`define IACX1 asmG1(3'o1,6'o73,
`define IACX2 asmG1(3'o2,6'o73,
`define IACX3 asmG1(3'o3,6'o73,
`define ILQ asmG1(3'o4,6'o73,
`define IAQ asmG1(3'o5,6'o73,
`define ILA asmG1(3'o6,6'o73,
`define IAA asmG1(3'o7,6'o73,

`define CAX2 asmG2(3'o0,6'o33,3'o2,0
`define LLS  asmG2(3'o0,6'o33,3'o4,
`define LRS  asmG2(3'o0,6'o33,3'o5,
`define ALS  asmG2(3'o0,6'o33,3'o6,
`define ARS  asmG2(3'o0,6'o33,3'o7,
`define NRML asmG2(3'o1,6'o33,3'o4,
`define NRM  asmG2(3'o1,6'o33,3'o6,
`define NOP  asmG2(3'o2,6'o33,3'o1,
`define CX1A asmG2(3'o2,6'o33,3'o2,0
`define LLR  asmG2(3'o2,6'o33,3'o4,
`define LRL  asmG2(3'o2,6'o33,3'o5,
`define ALR  asmG2(3'o2,6'o33,3'o6,
`define ARL  asmG2(3'o2,6'o33,3'o7,
`define INH  asmG2(3'o3,6'o33,3'o1,0
`define CX2A asmG2(3'o3,6'o33,3'o2,0
`define CX3A asmG2(3'o3,6'o33,3'o3,0
`define ALP  asmG2(3'o3,6'o33,3'o6,
`define DIS  asmG2(3'o4,6'o33,3'o1,0
`define CAX1 asmG2(3'o4,6'o33,3'o2,0
`define CAX3 asmG2(3'o4,6'o33,3'o3,0
`define QLS  asmG2(3'o4,6'o33,3'o6,
`define QRS  asmG2(3'o4,6'o33,3'o7,
`define CAQ  asmG2(3'o6,6'o33,3'o3,0
`define QLR  asmG2(3'o6,6'o33,3'o6,
`define QRL  asmG2(3'o6,6'o33,3'o7,
`define ENI  asmG2(3'o7,6'o33,3'o1,0
`define CQA  asmG2(3'o7,6'o33,3'o3,0
`define QLP  asmG2(3'o7,6'o33,3'o6,


`define _ );

module CpuProgrammer (
	input wire clk, 
	input wire reset,
	input wire [9:0] address,
	output reg [17:0] data,
	output reg loadComplete
);

	integer IP;
	reg [17:0] progMem [0:1023];
	
	task asmData ();
		input [17:0] DATA;
		progMem[IP] <= DATA;
		IP = IP + 1;
	endtask
	
	task asmG0 ();
		input [5:0] OP;
		input I;
		input [1:0] T;
		input [8:0] D;
		progMem[IP]	<= (I << 17) | (T << 15) | (OP << 9) | D;
		IP = IP + 1;
	endtask
	
	task asmG1 ();
		input [2:0] S1;
		input [5:0] OP;
		input [8:0] Y;
		progMem[IP]	<= (S1 << 15) | (OP << 9) | Y;
		IP = IP + 1;
	endtask

	task asmG2 ();
		input [2:0] S1;
		input [5:0] OP;
		input [2:0] S2;
		input [5:0] K;
		progMem[IP]	<= (S1 << 15) | (OP << 9) | (S2 << 6) | K;
		IP = IP + 1;
	endtask

	always @ (address, reset)
	begin
		if (reset)
		begin
			loadComplete <= 1'b0;
			IP = 0;
			// ***************************************************
			// *** Simple T&D Program for initial CPU checkout ***
			// ***************************************************
			// This program is designed to do some basic instruction tests to ensure at
			// least some confidence that all the instructions are working to some extent.
			//
			// The program will run until it encounters a problem or finishes.
			// If a problem is encountered, the "special" diagnostic halt is executed
			// with a failure code indicating the point in the test the failure occurred.
			//
			// The DIAG op code is really just special handling of the illegal op code 0.
			// When executed, the displacement field is loaded into the DIAG register
			// and the CPU is placed in a halted state.
			//
			// The diagnostic code 0 is not used and octal 777 indicates test completed successfully.
			// ***************************************************
			
			// Setup - TRA to the start of Test 1 and skip over the data working area defined here
			`TRA	`DIR	`tIC	9'o005		`_			// 000
			`DATA					18'o0			`_			// 001 - Note that if the TRA above doesn't work, this will actually cause DIAG 0 to occur
			`DATA					18'o1			`_			// 002
			`DATA					18'o777		`_			// 003
			`DATA					18'o777777	`_			// 004
			
			// *******************************************************
			// Test 1: ILA/TZE/TNZ/TRA
			// *******************************************************
			// - Load a non-zero value and do a transfer on non-zero - Failure = 001
			`ILA					1				`_			// 005
			`TNZ	`DIR	`tIC	2				`_			// 006
			`DIAG	`DIR	`tIC	9'o001		`_			// 007
			// - Load a zero value and do a transfer on zero - Failure = 002
			`ILA					0				`_			// 010
			`TZE	`DIR	`tIC	2				`_			// 011
			`DIAG	`DIR	`tIC	9'o002		`_			// 012
			// - Load a non-zero value and do a transfer on zero - Failure = 003
			`ILA					7				`_			// 013
			`TZE	`DIR	`tIC	2				`_			// 014
			`TRA	`DIR	`tIC	2				`_			// 015
			`DIAG	`DIR	`tIC	9'o003		`_			// 016
			// - Load a zero value and do a transfer on non-zero - Failure = 004
			`ILA					0				`_			// 017
			`TNZ	`DIR	`tIC	2				`_			// 020
			`TRA	`DIR	`tIC	2				`_			// 021
			`DIAG	`DIR	`tIC	9'o004		`_			// 022
			
			// *******************************************************
			// Test 2: ICMPA/TNC/TMI/TPL
			// *******************************************************
			// - Load a known value, compare to same value do transfer on zero - Failure = 005
			`ILA					7				`_			// 023
			`ICMPA				7				`_			// 024
			`TZE	`DIR	`tIC	2				`_			// 025
			`DIAG	`DIR	`tIC	9'o005		`_			// 026
			// - Carry should also be set so do a TNC to a failure - Failure = 006
			`TNC	`DIR	`tIC	2				`_			// 027
			`TRA	`DIR	`tIC	2				`_			// 030
			`DIAG	`DIR	`tIC	9'o006		`_			// 031
			// - Negative should not be set so do a TMI to a failure - Failure = 010
			`TMI	`DIR	`tIC	2				`_			// 032
			`TRA	`DIR	`tIC	2				`_			// 033
			`DIAG	`DIR	`tIC	9'o010		`_			// 034
			// - Load a known value, compare to lesser value do transfer on non-zero - Failure = 011
			`ILA					7				`_			// 035
			`ICMPA				4				`_			// 036
			`TNZ	`DIR	`tIC	2				`_			// 037
			`DIAG	`DIR	`tIC	9'o011		`_			// 040
			// - Carry should be set so do TNC to failure code - Failure = 012
			`TNC	`DIR	`tIC	2				`_			// 041
			`TRA	`DIR	`tIC	2				`_			// 042
			`DIAG	`DIR	`tIC	9'o012		`_			// 043
			// - Negative should not be set so do TPL - Failure = 013
			`TPL	`DIR	`tIC	2				`_			// 044
			`DIAG	`DIR	`tIC	9'o013		`_			// 045
			// - Load a known value, compare to greater value do transfer on non-zero - Failure = 014
			`ILA					6				`_			// 046
			`ICMPA				8				`_			// 047
			`TNZ	`DIR	`tIC	2				`_			// 050
			`DIAG	`DIR	`tIC	9'o014		`_			// 051
			// - Carry should not be set so do TNC - Failure = 015
			`TNC	`DIR	`tIC	2				`_			// 052
			`DIAG	`DIR	`tIC	9'o015		`_			// 053
			// - Negative should be set so do TMI - Failure = 016
			`TMI	`DIR	`tIC	2				`_			// 054
			`DIAG	`DIR	`tIC	9'o016		`_			// 055
			// - Load a negative value in A and compare to a positive value do transfer on non-zero - Failure = 017
			`ILA					9'o702		`_			// 056
			`ICMPA				9'o20			`_			// 057
			`TNZ	`DIR	`tIC	2				`_			// 060
			`DIAG	`DIR	`tIC	9'o017		`_			// 061
			// - Carry should be set so do TNC to failure - Failure = 020
			`TNC	`DIR	`tIC	2				`_			// 062
			`TRA	`DIR	`tIC	2				`_			// 063
			`DIAG	`DIR	`tIC	9'o020		`_			// 064
			// - Negative should be set so do TMI - Failure = 021
			`TMI	`DIR	`tIC	2				`_			// 065
			`DIAG	`DIR	`tIC	9'o021		`_			// 066
			// - Load a positive value in A and compare to a negative value do transfer on non-zero - Failure 022
			`ILA					9'o020		`_			// 067
			`ICMPA				9'o702		`_			// 070
			`TNZ	`DIR	`tIC	2				`_			// 071
			`DIAG	`DIR	`tIC	9'o022		`_			// 072
			// - Carry should not be set so do TNC - Failure = 023
			`TNC	`DIR	`tIC	2				`_			// 073
			`DIAG	`DIR	`tIC	9'o023		`_			// 074
			// - Negative should not be set so do TPL - Failure = 024
			`TPL	`DIR	`tIC	2				`_			// 075
			`DIAG	`DIR	`tIC	9'o024		`_			// 076
			
			// Test 3: ILA Negative flag check
			// - Load a small positive value and do TPL - Failure = 025
			`ILA					9'o033		`_			// 077
			`TPL	`DIR	`tIC	2				`_			// 100
			`DIAG	`DIR	`tIC	9'o025		`_			// 101
			// - Load a negative valaue and do TMI - Failure = 026
			`ILA					9'o733		`_			// 102
			`TMI	`DIR	`tIC	2				`_			// 103
			`DIAG	`DIR	`tIC	9'o026		`_			// 104
			
			// *******************************************************
			// Test 4: IAA/TOV
			// *******************************************************
			// - Load one into A and then add one and do TNZ - Failure = 027
			`ILA					9'o001		`_			// 105
			`IAA					9'o001		`_			// 106
			`TNZ	`DIR	`tIC	2				`_			// 107
			`DIAG	`DIR	`tIC	9'o027		`_			// 110
			// - Negative should not be set so do TPL - Failure = 030
			`TPL	`DIR	`tIC	2				`_			// 111
			`DIAG	`DIR	`tIC	9'o030		`_			// 112
			// - Overflow should not be set do TOV to failure - Failure = 031
			`TOV	`DIR	`tIC	2				`_			// 113
			`TRA	`DIR	`tIC	2				`_			// 114
			`DIAG	`DIR	`tIC	9'o031		`_			// 115
			// - Carry should not be set so do TNC - Failure = 032
			`TNC	`DIR	`tIC	2				`_			// 116
			`DIAG	`DIR	`tIC	9'o032		`_			// 117
			// - Make sure 1+1=2 so do ICMPA and TZE - Failure = 033
			`ICMPA				9'o002		`_			// 120
			`TZE	`DIR	`tIC	2				`_			// 121
			`DIAG	`DIR	`tIC	9'o033		`_			// 122
			// - Should not be negative so do TPL - Failure = 034
			`TPL	`DIR	`tIC	2				`_			// 123
			`DIAG	`DIR	`tIC	9'o034		`_			// 124
			// - Carry should be set so do TNC to failure - Failure = 035
			`TNC	`DIR	`tIC	2				`_			// 125
			`TRA	`DIR	`tIC	2				`_			// 126
			`DIAG	`DIR	`tIC	9'o035		`_			// 127
			// - Overflow should not be set so do TOV to failure - Failure = 036
			`TOV	`DIR	`tIC	2				`_			// 130
			`TRA	`DIR	`tIC	2				`_			// 131
			`DIAG	`DIR	`tIC	9'o036		`_			// 132
			// - Add -2 to A, zero should be set so do TZE - Failure = 037
			`IAA					9'o776		`_			// 133
			`TZE	`DIR	`tIC	2				`_			// 134
			`DIAG	`DIR	`tIC	9'o037		`_			// 135
			// - Should not be negative so do TPL - Failure = 040
			`TPL	`DIR	`tIC	2				`_			// 136
			`DIAG	`DIR	`tIC	9'o040		`_			// 137
			// - Carry should not be set so do TNC - Failure = 041
			`TNC	`DIR	`tIC	2				`_			// 140
			`DIAG	`DIR	`tIC	9'o041		`_			// 141
			// - Overflow should not be set so do TOV to failure - Failure = 042
			`TOV	`DIR	`tIC	2				`_			// 142
			`TRA	`DIR	`tIC	2				`_			// 143
			`DIAG	`DIR	`tIC	9'o042		`_			// 144
			// - Add -4 to A, zero should not be set so do TNZ - Failure = 043
			`IAA					9'o774		`_			// 145
			`TNZ	`DIR	`tIC	2				`_			// 146
			`DIAG	`DIR	`tIC	9'o043		`_			// 147
			// - Should be negative so do TMI - Failure = 044
			`TMI	`DIR	`tIC	2				`_			// 150
			`DIAG	`DIR	`tIC	9'o044		`_			// 151
			// - Carry should be set so do TNC to failure - Failure = 045
			`TNC	`DIR	`tIC	2				`_			// 152
			`TRA	`DIR	`tIC	2				`_			// 153
			`DIAG	`DIR	`tIC	9'o045		`_			// 154
			// - Overflow should not be set so do TOV to failure - Failure = 046
			`TOV	`DIR	`tIC	2				`_			// 155
			`TRA	`DIR	`tIC	2				`_			// 156
			`DIAG	`DIR	`tIC	9'o046		`_			// 157
			
			// *******************************************************
			// Test 5: LDA
			// *******************************************************
			// - Load one into A from memory then do ICMPA - Failure = 047
			`LDA	`DIR	`tIC	4				`_			// 160 (Load from 164)
			`ICMPA				1				`_			// 161
			`TZE	`DIR	`tIC	3				`_			// 162
			`DIAG	`DIR	`tIC	9'o047		`_			// 163
			`DATA					18'o1			`_			// 164
			// - Carry should also be set so do a TNC to a failure - Failure = 050
			`TNC	`DIR	`tIC	2				`_			// 165
			`TRA	`DIR	`tIC	2				`_			// 166
			`DIAG	`DIR	`tIC	9'o050		`_			// 167
			// - Negative should not be set so do a TPL - Failure = 051
			`TPL	`DIR	`tIC	2				`_			// 170
			`DIAG	`DIR	`tIC	9'o051		`_			// 171
			// - Load all ones into A from memory then do ICMPA - Failure = 052
			// -- With sign extension this is a signed compare of -1 == -1
			`LDA	`DIR	`tIC	4				`_			// 172 (Load from 176)
			`ICMPA				9'o777		`_			// 173
			`TZE	`DIR	`tIC	3				`_			// 174
			`DIAG	`DIR	`tIC	9'o052		`_			// 175
			`DATA					18'o777777	`_			// 176
			// - Carry should also be set so do a TNC to a failure - Failure = 053
			`TNC	`DIR	`tIC	2				`_			// 177
			`TRA	`DIR	`tIC	2				`_			// 200
			`DIAG	`DIR	`tIC	9'o053		`_			// 201
			// - Negative should not be set so do a TPL - Failure = 054
			`TPL	`DIR	`tIC	2				`_			// 202
			`DIAG	`DIR	`tIC	9'o054		`_			// 203

			// *******************************************************
			// Test 6: STA
			// *******************************************************
			// - Immediate load one into A, do STA, immediate load 2 to A, LDA and ICMPA - Failure = 055
			`ILA					9'o1			`_			// 204
			`STA	`DIR	`tIC	6				`_			// 205 (store to 213)
			`ILA					9'o2			`_			// 206
			`LDA	`DIR	`tIC	4				`_			// 207 (load from 213)
			`ICMPA				9'o1			`_			// 210
			`TZE	`DIR	`tIC	3				`_			// 211
			`DIAG	`DIR	`tIC	9'o055		`_			// 212
			`DATA					18'o777777	`_			// 213
			// - Carry should also be set so do a TNC to a failure - Failure = 056
			`TNC	`DIR	`tIC	2				`_			// 214
			`TRA	`DIR	`tIC	2				`_			// 215
			`DIAG	`DIR	`tIC	9'o056		`_			// 216
			// - Negative should not be set so do a TPL - Failure = 057
			`TPL	`DIR	`tIC	2				`_			// 217
			`DIAG	`DIR	`tIC	9'o057		`_			// 220
			
			// *******************************************************
			// Test 7: SZN
			// *******************************************************
			// - SZN from location with negative number, do TNZ - Failure = 060
			`SZN	`DIR	`tIC	3				`_			// 221 (from 224)
			`TNZ	`DIR	`tIC	3				`_			// 222
			`DIAG	`DIR	`tIC	9'o060		`_			// 223
			`DATA					18'o700000	`_			// 224
			// - Negative should be set so do a TMI - Failure = 061
			`TMI	`DIR	`tIC	2				`_			// 225
			`DIAG	`DIR	`tIC	9'o061		`_			// 226
			// - SZN from location with zero, do TZE - Failure = 062
			`SZN	`DIR	`tIC	3				`_			// 227 (from 232)
			`TZE	`DIR	`tIC	3				`_			// 230
			`DIAG	`DIR	`tIC	9'o062		`_			// 231
			`DATA					18'o000000	`_			// 232
			// - Negative should not be set so do a TMI - Failure = 063
			`TPL	`DIR	`tIC	2				`_			// 233
			`DIAG	`DIR	`tIC	9'o063		`_			// 234
			// - SZN from location with positive number, do TZE - Failure = 064
			`SZN	`DIR	`tIC	3				`_			// 235 (from 240)
			`TNZ	`DIR	`tIC	3				`_			// 236
			`DIAG	`DIR	`tIC	9'o064		`_			// 237
			`DATA					18'o000053	`_			// 240
			// - Negative should not be set so do a TMI - Failure = 065
			`TPL	`DIR	`tIC	2				`_			// 241
			`DIAG	`DIR	`tIC	9'o065		`_			// 242
			
			// *******************************************************
			// Test 8: CMPA
			// *******************************************************
			// - Immediate load a and compare with same value from memory, TZE - Failure = 066
			`ILA					9'o1			`_			// 243
			`CMPA	`DIR	`tIC	3				`_			// 244 (from 246)
			`TZE	`DIR	`tIC	3				`_			// 245
			`DIAG	`DIR	`tIC	9'o066		`_			// 246
			`DATA					18'o000001	`_			// 247
			// - Negative should not be set so do a TPL - Failure = 067
			`TPL	`DIR	`tIC	2				`_			// 250
			`DIAG	`DIR	`tIC	9'o067		`_			// 251
			// - Carry should be set so do TNC to failure - Failure = 070
			`TNC	`DIR	`tIC	2				`_			// 252
			`TRA	`DIR	`tIC	2				`_			// 253
			`DIAG	`DIR	`tIC	9'o070		`_			// 254
			// - Load a known value, compare to lesser value do transfer on non-zero - Failure = 071
			`ILA					7				`_			// 255
			`CMPA	`DIR	`tIC	3				`_			// 256 (from 261)
			`TNZ	`DIR	`tIC	3				`_			// 257
			`DIAG	`DIR	`tIC	9'o071		`_			// 260
			`DATA					18'o000003	`_			// 261
			// - Carry should be set so do TNC to failure code - Failure = 072
			`TNC	`DIR	`tIC	2				`_			// 262
			`TRA	`DIR	`tIC	2				`_			// 263
			`DIAG	`DIR	`tIC	9'o072		`_			// 264
			// - Negative should not be set so do TPL - Failure = 073
			`TPL	`DIR	`tIC	2				`_			// 265
			`DIAG	`DIR	`tIC	9'o073		`_			// 266
			// - Load a known value, compare to greater value do transfer on non-zero - Failure = 074
			`ILA					6				`_			// 267
			`CMPA	`DIR	`tIC	3				`_			// 270 (from 273)
			`TNZ	`DIR	`tIC	3				`_			// 271
			`DIAG	`DIR	`tIC	9'o074		`_			// 272
			`DATA					18'o004000	`_			// 273
			// - Carry should not be set so do TNC - Failure = 075
			`TNC	`DIR	`tIC	2				`_			// 274
			`DIAG	`DIR	`tIC	9'o075		`_			// 275
			// - Negative should be set so do TMI - Failure = 076
			`TMI	`DIR	`tIC	2				`_			// 276
			`DIAG	`DIR	`tIC	9'o076		`_			// 277
			// - Load a negative value in A and compare to a positive value do transfer on non-zero - Failure = 077
			`ILA					9'o702		`_			// 300
			`CMPA	`DIR	`tIC	3				`_			// 301 (from 304)
			`TNZ	`DIR	`tIC	3				`_			// 302
			`DIAG	`DIR	`tIC	9'o077		`_			// 303
			`DATA					18'o000020	`_			// 304
			// - Carry should be set so do TNC to failure - Failure = 100
			`TNC	`DIR	`tIC	2				`_			// 305
			`TRA	`DIR	`tIC	2				`_			// 306
			`DIAG	`DIR	`tIC	9'o100		`_			// 307
			// - Negative should be set so do TMI - Failure = 101
			`TMI	`DIR	`tIC	2				`_			// 310
			`DIAG	`DIR	`tIC	9'o101		`_			// 311
			// - Load a positive value in A and compare to a negative value do transfer on non-zero - Failure 102
			`ILA					9'o020		`_			// 312
			`CMPA	`DIR	`tIC	3				`_			// 313 (from 316)
			`TNZ	`DIR	`tIC	3				`_			// 314
			`DIAG	`DIR	`tIC	9'o102		`_			// 315
			`DATA					18'o770031	`_			// 316
			// - Carry should not be set so do TNC - Failure = 103
			`TNC	`DIR	`tIC	2				`_			// 317
			`DIAG	`DIR	`tIC	9'o103		`_			// 320
			// - Negative should not be set so do TPL - Failure = 104
			`TPL	`DIR	`tIC	2				`_			// 321
			`DIAG	`DIR	`tIC	9'o104		`_			// 322
			
			// *******************************************************
			// Test 9: ILQ / CQA
			// *******************************************************
			// - Load a non-zero value and do a transfer on non-zero - Failure = 105
			`ILQ					1				`_			// 323
			`TNZ	`DIR	`tIC	2				`_			// 324
			`DIAG	`DIR	`tIC	9'o105		`_			// 325
			// - Load a zero value and do a transfer on zero - Failure = 106
			`ILQ					0				`_			// 326
			`TZE	`DIR	`tIC	2				`_			// 327
			`DIAG	`DIR	`tIC	9'o106		`_			// 330
			// - Load a non-zero value and do a transfer on zero - Failure = 107
			`ILQ					7				`_			// 331
			`TZE	`DIR	`tIC	2				`_			// 332
			`TRA	`DIR	`tIC	2				`_			// 333
			`DIAG	`DIR	`tIC	9'o107		`_			// 334
			// - Load a zero value and do a transfer on non-zero - Failure = 110
			`ILQ					0				`_			// 335
			`TNZ	`DIR	`tIC	2				`_			// 336
			`TRA	`DIR	`tIC	2				`_			// 337
			`DIAG	`DIR	`tIC	9'o110		`_			// 340
			// - Load a non-zero value in A
			// - Load a different value in Q
			// - Copy Q to A
			// - Verify value in A is what was loaded into Q
			// - Zero should be set - Failure 111
			`ILA					7				`_			// 341
			`ILQ					1				`_			// 342
			`CQA									`_			// 343
			`ICMPA				1				`_			// 344
			`TZE	`DIR	`tIC	2				`_			// 345
			`DIAG	`DIR	`tIC	9'o111		`_			// 346
			// - Carry should also be set so do a TNC to a failure - Failure = 112
			`TNC	`DIR	`tIC	2				`_			// 347
			`TRA	`DIR	`tIC	2				`_			// 350
			`DIAG	`DIR	`tIC	9'o112		`_			// 351
			// - Negative should not be set so do a TMI to a failure - Failure = 113
			`TMI	`DIR	`tIC	2				`_			// 352
			`TRA	`DIR	`tIC	2				`_			// 352
			`DIAG	`DIR	`tIC	9'o113		`_			// 354
			// - Load a positive value into Q
			// - Copy Q to A
			// - Zero should not be set - Failure 114
			`ILQ					5				`_			// 355
			`CQA									`_			// 356
			`TNZ	`DIR	`tIC	2				`_			// 357
			`DIAG	`DIR	`tIC	9'o114		`_			// 360
			// - Negative should not be set - Failure = 115
			`TPL	`DIR	`tIC	2				`_			// 361
			`DIAG	`DIR	`tIC	9'o115		`_			// 362
			// - Load a negative value into Q
			// - Copy Q to A
			// - Zero should not be set - Failure 116
			`ILQ					9'o772		`_			// 363
			`CQA									`_			// 364
			`TNZ	`DIR	`tIC	2				`_			// 365
			`DIAG	`DIR	`tIC	9'o114		`_			// 366
			// - Negative should be set - Failure = 117
			`TMI	`DIR	`tIC	2				`_			// 367
			`DIAG	`DIR	`tIC	9'o117		`_			// 370
			// - Load a zero into Q
			// - Copy Q to A
			// - Zero should be set - Failure 120
			`ILQ					0				`_			// 371
			`CQA									`_			// 372
			`TZE	`DIR	`tIC	2				`_			// 373
			`DIAG	`DIR	`tIC	9'o120		`_			// 374
			// - Negative should not be set - Failure = 121
			`TPL	`DIR	`tIC	2				`_			// 375
			`DIAG	`DIR	`tIC	9'o121		`_			// 376
			
			// *******************************************************
			// Test 10: CAX1 / CX1A
			// *******************************************************
			// - Load a small positive number in A
			// - CAX1
			// - Check flags
			// - Load a zero in A
			// - CX1A
			// - Check flags
			// - Compare A to correct value
			// - CAX1 again to make sure carry doesn't change
			`ILA					9'o020		`_			// 377
			`CAX1									`_			// 400
			// - Zero should not be set - Failure = 122
			`TNZ	`DIR	`tIC	2				`_			// 401
			`DIAG	`DIR	`tIC	9'o122		`_			// 402
			// - Negative should not be set - Failure = 123
			`TPL	`DIR	`tIC	2				`_			// 403
			`DIAG	`DIR	`tIC	9'o123		`_			// 404
			`ILA					9'o000		`_			// 405
			`CX1A									`_			// 406
			// - Zero should not be set - Failure = 124
			`TNZ	`DIR	`tIC	2				`_			// 407
			`DIAG	`DIR	`tIC	9'o124		`_			// 410
			// - Check the value to make sure it's correct
			`ICMPA				9'o020		`_			// 411
			// - Zero should be set - Failure = 125
			`TZE	`DIR	`tIC	2				`_			// 412
			`DIAG	`DIR	`tIC	9'o125		`_			// 413
			// - Negative should not be set - Failure = 126
			`TPL	`DIR	`tIC	2				`_			// 414
			`DIAG	`DIR	`tIC	9'o126		`_			// 415
			// - Carry should be set, TNC to failure - Failure = 127
			`TNC	`DIR	`tIC	2				`_			// 416
			`TRA	`DIR	`tIC	2				`_			// 417
			`DIAG	`DIR	`tIC	9'o127		`_			// 420
			// - Since carry is set we can make sure it's not affected by CAX1
			`CAX1									`_			// 421
			// - Carry should still be set, TNC to failure - Failure = 130
			`TNC	`DIR	`tIC	2				`_			// 422
			`TRA	`DIR	`tIC	2				`_			// 423
			`DIAG	`DIR	`tIC	9'o130		`_			// 424
			
			// *******************************************************
			// Test 11: CAX2 / CX2A
			// *******************************************************
			// - Load a small positive number in A
			// - CAX2
			// - Check flags
			// - Load a zero in A
			// - CX2A
			// - Check flags
			// - Compare A to correct value
			// - CAX2 again to make sure carry doesn't change
			`ILA					9'o020		`_			// 425
			`CAX2									`_			// 426
			// - Zero should not be set - Failure = 131
			`TNZ	`DIR	`tIC	2				`_			// 427
			`DIAG	`DIR	`tIC	9'o131		`_			// 430
			// - Negative should not be set - Failure = 132
			`TPL	`DIR	`tIC	2				`_			// 431
			`DIAG	`DIR	`tIC	9'o132		`_			// 432
			`ILA					9'o000		`_			// 433
			`CX2A									`_			// 434
			// - Zero should not be set - Failure = 133
			`TNZ	`DIR	`tIC	2				`_			// 435
			`DIAG	`DIR	`tIC	9'o133		`_			// 436
			// - Check the value to make sure it's correct
			`ICMPA				9'o020		`_			// 437
			// - Zero should be set - Failure = 134
			`TZE	`DIR	`tIC	2				`_			// 440
			`DIAG	`DIR	`tIC	9'o134		`_			// 441
			// - Negative should not be set - Failure = 135
			`TPL	`DIR	`tIC	2				`_			// 442
			`DIAG	`DIR	`tIC	9'o135		`_			// 443
			// - Carry should be set, TNC to failure - Failure = 136
			`TNC	`DIR	`tIC	2				`_			// 444
			`TRA	`DIR	`tIC	2				`_			// 445
			`DIAG	`DIR	`tIC	9'o136		`_			// 446
			// - Since carry is set we can make sure it's not affected by CAX1
			`CAX2									`_			// 447
			// - Carry should still be set, TNC to failure - Failure = 137
			`TNC	`DIR	`tIC	2				`_			// 450
			`TRA	`DIR	`tIC	2				`_			// 451
			`DIAG	`DIR	`tIC	9'o137		`_			// 452

			// *******************************************************
			// Test 12: CAX3 / CX23
			// *******************************************************
			// - Load a small positive number in A
			// - CAX3
			// - Check flags
			// - Load a zero in A
			// - CX3A
			// - Check flags
			// - Compare A to correct value
			// - CAX3 again to make sure carry doesn't change
			`ILA					9'o020		`_			// 453
			`CAX3									`_			// 454
			// - Zero should not be set - Failure = 140
			`TNZ	`DIR	`tIC	2				`_			// 455
			`DIAG	`DIR	`tIC	9'o140		`_			// 456
			// - Negative should not be set - Failure = 141
			`TPL	`DIR	`tIC	2				`_			// 457
			`DIAG	`DIR	`tIC	9'o141		`_			// 460
			`ILA					9'o000		`_			// 461
			`CX3A									`_			// 462
			// - Zero should not be set - Failure = 142
			`TNZ	`DIR	`tIC	2				`_			// 463
			`DIAG	`DIR	`tIC	9'o142		`_			// 464
			// - Check the value to make sure it's correct
			`ICMPA				9'o020		`_			// 465
			// - Zero should be set - Failure = 143
			`TZE	`DIR	`tIC	2				`_			// 466
			`DIAG	`DIR	`tIC	9'o143		`_			// 467
			// - Negative should not be set - Failure = 144
			`TPL	`DIR	`tIC	2				`_			// 470
			`DIAG	`DIR	`tIC	9'o144		`_			// 471
			// - Carry should be set, TNC to failure - Failure = 145
			`TNC	`DIR	`tIC	2				`_			// 472
			`TRA	`DIR	`tIC	2				`_			// 473
			`DIAG	`DIR	`tIC	9'o145		`_			// 474
			// - Since carry is set we can make sure it's not affected by CAX1
			`CAX3									`_			// 475
			// - Carry should still be set, TNC to failure - Failure = 146
			`TNC	`DIR	`tIC	2				`_			// 476
			`TRA	`DIR	`tIC	2				`_			// 477
			`DIAG	`DIR	`tIC	9'o146		`_			// 500
			
			// *******************************************************
			// Test 13: CMPQ
			// *******************************************************
			// - Immediate load Q and compare with same value from memory, TZE - Failure = 147
			`ILQ					9'o001		`_			// 501
			`CMPQ	`DIR	`tIC	3				`_			// 502 (from 505)
			`TZE	`DIR	`tIC	3				`_			// 503
			`DIAG	`DIR	`tIC	9'o147		`_			// 504
			`DATA					18'o000001	`_			// 505
			// - Negative should not be set so do a TPL - Failure = 150
			`TPL	`DIR	`tIC	2				`_			// 506
			`DIAG	`DIR	`tIC	9'o150		`_			// 507
			// - Carry should be set so do TNC to failure - Failure = 151
			`TNC	`DIR	`tIC	2				`_			// 510
			`TRA	`DIR	`tIC	2				`_			// 511
			`DIAG	`DIR	`tIC	9'o151		`_			// 512
			// - Load a known value, compare to lesser value do transfer on non-zero - Failure = 152
			`ILQ					7				`_			// 513
			`CMPQ	`DIR	`tIC	3				`_			// 514 (from 517)
			`TNZ	`DIR	`tIC	3				`_			// 515
			`DIAG	`DIR	`tIC	9'o152		`_			// 516
			`DATA					18'o000003	`_			// 517
			// - Carry should be set so do TNC to failure code - Failure = 153
			`TNC	`DIR	`tIC	2				`_			// 520
			`TRA	`DIR	`tIC	2				`_			// 521
			`DIAG	`DIR	`tIC	9'o153		`_			// 522
			// - Negative should not be set so do TPL - Failure = 154
			`TPL	`DIR	`tIC	2				`_			// 523
			`DIAG	`DIR	`tIC	9'o154		`_			// 524
			// - Load a known value, compare to greater value do transfer on non-zero - Failure = 155
			`ILQ					6				`_			// 525
			`CMPQ	`DIR	`tIC	3				`_			// 526 (from 531)
			`TNZ	`DIR	`tIC	3				`_			// 527
			`DIAG	`DIR	`tIC	9'o155		`_			// 530
			`DATA					18'o004000	`_			// 531
			// - Carry should not be set so do TNC - Failure = 156
			`TNC	`DIR	`tIC	2				`_			// 532
			`DIAG	`DIR	`tIC	9'o156		`_			// 533
			// - Negative should be set so do TMI - Failure = 157
			`TMI	`DIR	`tIC	2				`_			// 534
			`DIAG	`DIR	`tIC	9'o157		`_			// 535
			// - Load a negative value in A and compare to a positive value do transfer on non-zero - Failure = 160
			`ILQ					9'o702		`_			// 536
			`CMPQ	`DIR	`tIC	3				`_			// 537 (from 542)
			`TNZ	`DIR	`tIC	3				`_			// 540
			`DIAG	`DIR	`tIC	9'o160		`_			// 541
			`DATA					18'o000020	`_			// 542
			// - Carry should be set so do TNC to failure - Failure = 161
			`TNC	`DIR	`tIC	2				`_			// 543
			`TRA	`DIR	`tIC	2				`_			// 544
			`DIAG	`DIR	`tIC	9'o161		`_			// 545
			// - Negative should be set so do TMI - Failure = 162
			`TMI	`DIR	`tIC	2				`_			// 546
			`DIAG	`DIR	`tIC	9'o162		`_			// 547
			// - Load a positive value in A and compare to a negative value do transfer on non-zero - Failure 163
			`ILQ					9'o020		`_			// 550
			`CMPQ	`DIR	`tIC	3				`_			// 551 (from 554)
			`TNZ	`DIR	`tIC	3				`_			// 552
			`DIAG	`DIR	`tIC	9'o163		`_			// 553
			`DATA					18'o770031	`_			// 554
			// - Carry should not be set so do TNC - Failure = 164
			`TNC	`DIR	`tIC	2				`_			// 555
			`DIAG	`DIR	`tIC	9'o164		`_			// 556
			// - Negative should not be set so do TPL - Failure = 165
			`TPL	`DIR	`tIC	2				`_			// 557
			`DIAG	`DIR	`tIC	9'o165		`_			// 560
			
			// *******************************************************
			// Test 14: CAQ
			// *******************************************************
			// - Load positive value in A
			// - CAQ
			// - Verify flags
			// - CMPQ
			// - Verify flags
			// - Load zero value in A
			// - CAQ
			// - Verify flags
			// - CMPQ
			// - Verify flags
			// - Load negative value in A
			// - CAQ
			// - Verify flags
			// - CMPQ
			// - Verify flags
			
			// - Load postive value into A
			`ILA					9'o031		`_			// 561
			`CAQ									`_			// 562
			// - Zero should not be set - Failure = 166
			`TNZ	`DIR	`tIC	2				`_			// 563
			`DIAG	`DIR	`tIC	9'o166		`_			// 564
			// - Negative should not be set - Failure = 167
			`TPL	`DIR	`tIC	2				`_			// 565
			`DIAG	`DIR	`tIC	9'o167		`_			// 566
			// - Check value in Q, zero should be set - Failure 170
			`CMPQ	`DIR	`tIC	3				`_			// 567 (from 572)
			`TZE	`DIR	`tIC	3				`_			// 570
			`DIAG	`DIR	`tIC	9'o170		`_			// 571
			`DATA					18'o000031	`_			// 572
			// - Negative should not be set - Failure = 171
			`TPL	`DIR	`tIC	2				`_			// 573
			`DIAG	`DIR	`tIC	9'o171		`_			// 574
			// - Carry should be set so do TNC to failure - Failure = 172
			`TNC	`DIR	`tIC	2				`_			// 575
			`TRA	`DIR	`tIC	2				`_			// 576
			`DIAG	`DIR	`tIC	9'o172		`_			// 577
			
			// - Load zero value into A
			`ILA					9'o000		`_			// 600
			`CAQ									`_			// 601
			// - Zero should be set - Failure = 173
			`TZE	`DIR	`tIC	2				`_			// 602
			`DIAG	`DIR	`tIC	9'o173		`_			// 603
			// - Negative should not be set - Failure = 174
			`TPL	`DIR	`tIC	2				`_			// 604
			`DIAG	`DIR	`tIC	9'o174		`_			// 605
			// - Check value in Q, zero should be set - Failure 175
			`CMPQ	`DIR	`tIC	3				`_			// 606 (from 611)
			`TZE	`DIR	`tIC	3				`_			// 607
			`DIAG	`DIR	`tIC	9'o175		`_			// 610
			`DATA					18'o000000	`_			// 611
			// - Negative should not be set - Failure = 176
			`TPL	`DIR	`tIC	2				`_			// 612
			`DIAG	`DIR	`tIC	9'o176		`_			// 613
			// - Carry should be set so do TNC to failure - Failure = 177
			`TNC	`DIR	`tIC	2				`_			// 614
			`TRA	`DIR	`tIC	2				`_			// 615
			`DIAG	`DIR	`tIC	9'o177		`_			// 616
			
			// - Load negative value into A
			`ILA					9'o701		`_			// 617
			`CAQ									`_			// 620
			// - Zero should not be set - Failure = 200
			`TNZ	`DIR	`tIC	2				`_			// 621
			`DIAG	`DIR	`tIC	9'o200		`_			// 622
			// - Negative should be set - Failure = 201
			`TMI	`DIR	`tIC	2				`_			// 623
			`DIAG	`DIR	`tIC	9'o201		`_			// 624
			// - Check value in Q, zero should be set - Failure 202
			`CMPQ	`DIR	`tIC	3				`_			// 625 (from 630)
			`TZE	`DIR	`tIC	3				`_			// 626
			`DIAG	`DIR	`tIC	9'o202		`_			// 627
			`DATA					18'o777701	`_			// 630
			// - Negative should not be set - Failure = 203
			`TPL	`DIR	`tIC	2				`_			// 631
			`DIAG	`DIR	`tIC	9'o203		`_			// 632
			// - Carry should be set so do TNC to failure - Failure = 204
			`TNC	`DIR	`tIC	2				`_			// 633
			`TRA	`DIR	`tIC	2				`_			// 634
			`DIAG	`DIR	`tIC	9'o204		`_			// 635
			
			// *******************************************************
			// Test 15: LDQ
			// *******************************************************
			// - Load positive value into Q from memory then do ICMPA - Failure = 205
			`LDQ	`DIR	`tIC	4				`_			// 636 (Load from 642)
			`CMPQ	`DIR	`tIC	3				`_			// 637 (From 642)
			`TZE	`DIR	`tIC	3				`_			// 640
			`DIAG	`DIR	`tIC	9'o205		`_			// 641
			`DATA					18'o000221	`_			// 642
			// - Carry should also be set so do a TNC to a failure - Failure = 206
			`TNC	`DIR	`tIC	2				`_			// 643
			`TRA	`DIR	`tIC	2				`_			// 644
			`DIAG	`DIR	`tIC	9'o206		`_			// 645
			// - Negative should not be set so do a TPL - Failure = 207
			`TPL	`DIR	`tIC	2				`_			// 646
			`DIAG	`DIR	`tIC	9'o207		`_			// 647
			// - Load all ones into Q from memory then do CMPQ - Failure = 210
			`LDQ	`DIR	`tIC	4				`_			// 650 (Load from 654)
			`CMPQ	`DIR	`tIC	3				`_			// 651 (From 654)
			`TZE	`DIR	`tIC	3				`_			// 652
			`DIAG	`DIR	`tIC	9'o210		`_			// 653
			`DATA					18'o777777	`_			// 654
			// - Carry should also be set so do a TNC to a failure - Failure = 211
			`TNC	`DIR	`tIC	2				`_			// 655
			`TRA	`DIR	`tIC	2				`_			// 656
			`DIAG	`DIR	`tIC	9'o211		`_			// 657
			// - Negative should not be set so do a TPL - Failure = 212
			`TPL	`DIR	`tIC	2				`_			// 660
			`DIAG	`DIR	`tIC	9'o212		`_			// 661

			// *******************************************************
			// Test 16: STQ
			// *******************************************************
			// - Immediate load one into Q, do STQ, immediate load 2 to Q, LDQ and CMPQ - Failure = 213
			`ILQ					9'o1			`_			// 662
			`STQ	`DIR	`tIC	6				`_			// 663 (store to 671)
			`ILQ					9'o2			`_			// 664
			`LDQ	`DIR	`tIC	4				`_			// 665 (load from 671)
			`CMPQ	`DIR	`tIC	3				`_			// 666
			`TZE	`DIR	`tIC	3				`_			// 667
			`DIAG	`DIR	`tIC	9'o213		`_			// 670
			`DATA					18'o777777	`_			// 671
			// - Carry should also be set so do a TNC to a failure - Failure = 214
			`TNC	`DIR	`tIC	2				`_			// 672
			`TRA	`DIR	`tIC	2				`_			// 673
			`DIAG	`DIR	`tIC	9'o214		`_			// 674
			// - Negative should not be set so do a TPL - Failure = 215
			`TPL	`DIR	`tIC	2				`_			// 675
			`DIAG	`DIR	`tIC	9'o215		`_			// 676

			// *******************************************************
			// Test 17: LDAQ
			// *******************************************************
			// - LDAQ with positive value (even address)
			// - Check flags
			// - Check value in A
			// - Check value in Q
			// - LDAQ with zero
			// - Check flags
			// - Check A for zero
			// - Check Q for zero
			// - LDAQ with negative value
			// - Check flags
			// - Check value in A
			// - Check value in Q
			// - Also need to check even/odd to make sure right addresses loaded
			
			// - LDAQ with positive value, zero should not be set - Failure = 216
			`LDAQ	`DIR	`tIC	3				`_			// 677 (from 702)
			`TNZ	`DIR	`tIC	4				`_			// 700
			`DIAG	`DIR	`tIC	9'o216		`_			// 701
			`DATA					18'o002003	`_			// 702
			`DATA					18'o051110	`_			// 703
			// - Negative should not be set so do a TPL - Failure = 217
			`TPL	`DIR	`tIC	2				`_			// 704
			`DIAG	`DIR	`tIC	9'o217		`_			// 705
			// - Check value in A, zero should be set - Failure 220
			`CMPA	`DIR	`tIC	3				`_			// 706 (from 711)
			`TZE	`DIR	`tIC	3				`_			// 707
			`DIAG	`DIR	`tIC	9'o220		`_			// 710
			`DATA					18'o002003	`_			// 711
			// - Negative should not be set so do a TPL - Failure = 221
			`TPL	`DIR	`tIC	2				`_			// 712
			`DIAG	`DIR	`tIC	9'o221		`_			// 713
			// - Carry should be set so do TNC to failure - Failure = 222
			`TNC	`DIR	`tIC	2				`_			// 714
			`TRA	`DIR	`tIC	2				`_			// 715
			`DIAG	`DIR	`tIC	9'o222		`_			// 716
			// - Check value in Q, zero should be set - Failure = 223
			`CMPQ	`DIR	`tIC	3				`_			// 717 (from 722)
			`TZE	`DIR	`tIC	3				`_			// 720
			`DIAG	`DIR	`tIC	9'o223		`_			// 721
			`DATA					18'o051110	`_			// 722
			// - Carry should also be set so do a TNC to a failure - Failure = 224
			`TNC	`DIR	`tIC	2				`_			// 723
			`TRA	`DIR	`tIC	2				`_			// 724
			`DIAG	`DIR	`tIC	9'o224		`_			// 725
			// - Negative should not be set so do a TPL - Failure = 225
			`TPL	`DIR	`tIC	2				`_			// 726
			`DIAG	`DIR	`tIC	9'o225		`_			// 727
			
			// - LDAQ with zero, zero should be set - Failure = 226
			`LDAQ	`DIR	`tIC	5				`_			// 730 (loading from odd address 735)
			`TZE	`DIR	`tIC	6				`_			// 731
			`DIAG	`DIR	`tIC	9'o226		`_			// 732
			`DATA					18'o777777	`_			// 733 (marker to make sure correct addresses load)
			`DATA					18'o000000	`_			// 734
			`DATA					18'o000000	`_			// 735
			`DATA					18'o777777	`_			// 736 (marker to make sure correct addresses load)
			// - Negative should not be set so do a TPL - Failure = 227
			`TPL	`DIR	`tIC	2				`_			// 737
			`DIAG	`DIR	`tIC	9'o227		`_			// 740
			// - Check value in A, zero should be set - Failure 230
			`CMPA	`DIR	`tIC	3				`_			// 741 (from 744)
			`TZE	`DIR	`tIC	3				`_			// 742
			`DIAG	`DIR	`tIC	9'o230		`_			// 743
			`DATA					18'o000000	`_			// 744
			// - Negative should not be set so do a TPL - Failure = 231
			`TPL	`DIR	`tIC	2				`_			// 745
			`DIAG	`DIR	`tIC	9'o231		`_			// 746
			// - Carry should be set so do TNC to failure - Failure = 232
			`TNC	`DIR	`tIC	2				`_			// 747
			`TRA	`DIR	`tIC	2				`_			// 750
			`DIAG	`DIR	`tIC	9'o232		`_			// 751
			// - Check value in Q, zero should be set - Failure = 233
			`CMPQ	`DIR	`tIC	3				`_			// 752 (from 755)
			`TZE	`DIR	`tIC	3				`_			// 753
			`DIAG	`DIR	`tIC	9'o233		`_			// 754
			`DATA					18'o000000	`_			// 755
			// - Carry should also be set so do a TNC to a failure - Failure = 234
			`TNC	`DIR	`tIC	2				`_			// 756
			`TRA	`DIR	`tIC	2				`_			// 757
			`DIAG	`DIR	`tIC	9'o234		`_			// 760
			// - Negative should not be set so do a TPL - Failure = 235
			`TPL	`DIR	`tIC	2				`_			// 761
			`DIAG	`DIR	`tIC	9'o235		`_			// 762

			// - LDAQ with negative number, zero should not be set - Failure = 236
			`LDAQ	`DIR	`tIC	3				`_			// 763 (loading from even address 766)
			`TNZ	`DIR	`tIC	6				`_			// 764
			`DIAG	`DIR	`tIC	9'o236		`_			// 765
			`DATA					18'o777777	`_			// 766 
			`DATA					18'o777722	`_			// 767 
			// - Negative should be set so do a TMI - Failure = 237
			`TMI	`DIR	`tIC	2				`_			// 770
			`DIAG	`DIR	`tIC	9'o237		`_			// 771
			// - Check value in A, zero should be set - Failure 240
			`CMPA	`DIR	`tIC	3				`_			// 772 (from 775)
			`TZE	`DIR	`tIC	3				`_			// 773
			`DIAG	`DIR	`tIC	9'o240		`_			// 774
			`DATA					18'o777777	`_			// 775
			// - Negative should not be set so do a TPL - Failure = 241
			`TPL	`DIR	`tIC	2				`_			// 776
			`DIAG	`DIR	`tIC	9'o241		`_			// 777
			// - Carry should be set so do TNC to failure - Failure = 242
			`TNC	`DIR	`tIC	2				`_			// 1000
			`TRA	`DIR	`tIC	2				`_			// 1001
			`DIAG	`DIR	`tIC	9'o242		`_			// 1002
			// - Check value in Q, zero should be set - Failure = 243
			`CMPQ	`DIR	`tIC	3				`_			// 1003 (from 1006)
			`TZE	`DIR	`tIC	3				`_			// 1004
			`DIAG	`DIR	`tIC	9'o243		`_			// 1005
			`DATA					18'o777722	`_			// 1006
			// - Carry should also be set so do a TNC to a failure - Failure = 244
			`TNC	`DIR	`tIC	2				`_			// 1007
			`TRA	`DIR	`tIC	2				`_			// 1010
			`DIAG	`DIR	`tIC	9'o244		`_			// 1011
			// - Negative should not be set so do a TPL - Failure = 245
			`TPL	`DIR	`tIC	2				`_			// 1012
			`DIAG	`DIR	`tIC	9'o245		`_			// 1013
			
			// *******************************************************
			// Test 18: STAQ
			// *******************************************************
			// - LDA/LDQ with known value
			// - STAQ
			// - LDA with zero
			// - LDA with stored even word
			// - CMPA with same value
			// - Check flags
			// - LDA with stored odd word
			// - CMPA with same value
			// - Check flags
			// - Repeat above for odd STAQ
			
			// - Load A & Q with known values then STAQ
			`LDA	`DIR	`tIC	8				`_			// 1014 (from 1024)
			`LDQ	`DIR	`tIC	8				`_			// 1015 (from 1025)
			`STAQ	`DIR	`tIC	8				`_			// 1016 (to 1026 & 1027)
			// - Clear A
			`ILA					9'o000		`_			// 1017
			// - Load A with stored even word and CMPA, zero should be set - Failure = 246
			`LDA	`DIR	`tIC	6				`_			// 1020 (from 1026)
			`CMPA	`DIR	`tIC	3				`_			// 1021 (from 1024)
			`TZE	`DIR	`tIC	6				`_			// 1022
			`DIAG	`DIR	`tIC	9'o246		`_			// 1023
			`DATA					18'o000123	`_			// 1024
			`DATA					18'o456111	`_			// 1025
			`DATA					18'o777777	`_			// 1026
			`DATA					18'o777777	`_			// 1027
			// - Negative should not be set so do a TPL - Failure = 247
			`TPL	`DIR	`tIC	2				`_			// 1030
			`DIAG	`DIR	`tIC	9'o247		`_			// 1031
			// - Carry should be set so do TNC to failure - Failure = 250
			`TNC	`DIR	`tIC	2				`_			// 1032
			`TRA	`DIR	`tIC	2				`_			// 1033
			`DIAG	`DIR	`tIC	9'o250		`_			// 1034
			// - Load A with stored odd word and CMPA, zero should be set - Failure = 251
			`LDA	`DIR	`tIC	9'o772		`_			// 1035 (from 1027)
			`CMPA	`DIR	`tIC	9'o767		`_			// 1036 (from 1025)
			`TZE	`DIR	`tIC	2				`_			// 1037
			`DIAG	`DIR	`tIC	9'o251		`_			// 1040
			// - Negative should not be set so do a TPL - Failure = 252
			`TPL	`DIR	`tIC	2				`_			// 1041
			`DIAG	`DIR	`tIC	9'o252		`_			// 1042
			// - Carry should be set so do TNC to failure - Failure = 253
			`TNC	`DIR	`tIC	2				`_			// 1043
			`TRA	`DIR	`tIC	2				`_			// 1044
			`DIAG	`DIR	`tIC	9'o253		`_			// 1045

			// - Load A & Q with known values then STAQ
			`LDA	`DIR	`tIC	8				`_			// 1046 (from 1056)
			`LDQ	`DIR	`tIC	8				`_			// 1047 (from 1057)
			`STAQ	`DIR	`tIC	9				`_			// 1050 (to 1060 & 1061) (store to odd address)
			// - Clear A
			`ILA					9'o000		`_			// 1051
			// - Load A with stored even word and CMPA, zero should be set - Failure = 246
			`LDA	`DIR	`tIC	6				`_			// 1052 (from 1060)
			`CMPA	`DIR	`tIC	3				`_			// 1053 (from 1056)
			`TZE	`DIR	`tIC	7				`_			// 1054
			`DIAG	`DIR	`tIC	9'o246		`_			// 1055
			`DATA					18'o222036	`_			// 1056
			`DATA					18'o062101	`_			// 1057
			`DATA					18'o777777	`_			// 1060
			`DATA					18'o777777	`_			// 1061
			`DATA					18'o0000000	`_			// 1062 (marker, should not change)
			// - Negative should not be set so do a TPL - Failure = 247
			`TPL	`DIR	`tIC	2				`_			// 1062
			`DIAG	`DIR	`tIC	9'o247		`_			// 1064
			// - Carry should be set so do TNC to failure - Failure = 250
			`TNC	`DIR	`tIC	2				`_			// 1065
			`TRA	`DIR	`tIC	2				`_			// 1066
			`DIAG	`DIR	`tIC	9'o250		`_			// 1067
			// - Load A with stored odd word and CMPA, zero should be set - Failure = 251
			`LDA	`DIR	`tIC	9'o771		`_			// 1070 (from 1061)
			`CMPA	`DIR	`tIC	9'o766		`_			// 1071 (from 1057)
			`TZE	`DIR	`tIC	2				`_			// 1072
			`DIAG	`DIR	`tIC	9'o251		`_			// 1073
			// - Negative should not be set so do a TPL - Failure = 252
			`TPL	`DIR	`tIC	2				`_			// 1074
			`DIAG	`DIR	`tIC	9'o252		`_			// 1075
			// - Carry should be set so do TNC to failure - Failure = 253
			`TNC	`DIR	`tIC	2				`_			// 1076
			`TRA	`DIR	`tIC	2				`_			// 1077
			`DIAG	`DIR	`tIC	9'o253		`_			// 1100
			// - Load marker into A, zero should be set - Failure = 254
			`LDA	`DIR	`tIC	9'o761		`_			// 1101 (from 1062)
			`TZE	`DIR	`tIC	2				`_			// 1102
			`DIAG	`DIR	`tIC	9'o254		`_			// 1103
			
			// *******************************************************
			// Test 19: ALS
			// *******************************************************
			// - Load 1 into A
			// - Left shift 1 place
			// - Check carry flag
			// - ICMPA with 2
			// - Repeat above for shifts of 3, 7, 15, 17, 18
			// - Also verify correct operation of carry flag
			`ILA					9'o001		`_			// 1104
			`ALS					6'o001		`_			// 1105
			// - Carry should not be set, Failure = 255
			`TNC	`DIR	`tIC	2				`_			// 1106
			`DIAG	`DIR	`tIC	9'o255		`_			// 1107
			// - Check to make sure value is correct
			`ICMPA				9'o002		`_			// 1110
			// - Zero should be set, Failure = 256
			`TZE	`DIR	`tIC	2				`_			// 1111
			`DIAG	`DIR	`tIC	9'o256		`_			// 1112
			// - Check shift of 3
			`ILA					9'o001		`_			// 1113
			`ALS					6'o003		`_			// 1114
			// - Carry should not be set, Failure = 257
			`TNC	`DIR	`tIC	2				`_			// 1115
			`DIAG	`DIR	`tIC	9'o257		`_			// 1116
			// - Check to make sure value is correct
			`ICMPA				9'o010		`_			// 1117
			// - Zero should be set, Failure = 260
			`TZE	`DIR	`tIC	2				`_			// 1120
			`DIAG	`DIR	`tIC	9'o260		`_			// 1121
			// - Check shift of 7
			`ILA					9'o001		`_			// 1122
			`ALS					6'o007		`_			// 1123
			// - Carry should not be set, Failure = 261
			`TNC	`DIR	`tIC	2				`_			// 1124
			`DIAG	`DIR	`tIC	9'o261		`_			// 1125
			// - Check to make sure value is correct
			`ICMPA				9'o200		`_			// 1126
			// - Zero should be set, Failure = 262
			`TZE	`DIR	`tIC	2				`_			// 1127
			`DIAG	`DIR	`tIC	9'o262		`_			// 1130
			// - Check shift of 15
			`ILA					9'o001		`_			// 1131
			`ALS					6'o017		`_			// 1132
			// - Carry should not be set, Failure = 263
			`TNC	`DIR	`tIC	2				`_			// 1133
			`DIAG	`DIR	`tIC	9'o263		`_			// 1134
			// - Check to make sure value is correct
			`CMPA	`DIR	`tIC	4				`_			// 1135
			// - Zero should be set, Failure = 264
			`TZE	`DIR	`tIC	2				`_			// 1136
			`DIAG	`DIR	`tIC	9'o264		`_			// 1137
			`TRA	`DIR	`tIC	2				`_			// 1140
			`DATA					18'o100000	`_			// 1141
			// - Check shift of 17
			`ILA					9'o001		`_			// 1142
			`ALS					6'o021		`_			// 1143
			// - Negative should be set, Failure = 265
			`TMI	`DIR	`tIC	2				`_			// 1144
			`DIAG	`DIR	`tIC	9'o265		`_			// 1145
			// - Carry should be set, Failure = 266
			`TNC	`DIR	`tIC	2				`_			// 1146
			`TRA	`DIR	`tIC	2				`_			// 1147
			`DIAG	`DIR	`tIC	9'o266		`_			// 1150
			// - Check to make sure value is correct
			`CMPA	`DIR	`tIC	4				`_			// 1151
			// - Zero should be set, Failure = 267
			`TZE	`DIR	`tIC	2				`_			// 1152
			`DIAG	`DIR	`tIC	9'o267		`_			// 1153
			`TRA	`DIR	`tIC	2				`_			// 1154
			`DATA					18'o400000	`_			// 1155
			// - Check shift of 18
			`ILA					9'o001		`_			// 1156
			`ALS					6'o022		`_			// 1157
			// - Negative should not be set, Failure = 270
			`TPL	`DIR	`tIC	2				`_			// 1160
			`DIAG	`DIR	`tIC	9'o270		`_			// 1161
			// - Carry should be set, Failure = 271
			`TNC	`DIR	`tIC	2				`_			// 1162
			`TRA	`DIR	`tIC	2				`_			// 1163
			`DIAG	`DIR	`tIC	9'o271		`_			// 1164
			// - Check to make sure value is correct
			`ICMPA				0				`_			// 1165
			// - Zero should be set, Failure = 272
			`TZE	`DIR	`tIC	2				`_			// 1166
			`DIAG	`DIR	`tIC	9'o272		`_			// 1167

			// *******************************************************
			// Test 20: QLS
			// *******************************************************
			// - Load 1 into Q
			// - Left shift 1 place
			// - Check carry flag
			// - CMPQ with 2
			// - Repeat above for shifts of 3, 7, 15, 17, 18
			// - Also verify correct operation of carry flag
			`ILQ					9'o001		`_			// 1170
			`QLS					6'o001		`_			// 1171
			// - Carry should not be set, Failure = 273
			`TNC	`DIR	`tIC	2				`_			// 1172
			`DIAG	`DIR	`tIC	9'o273		`_			// 1173
			// - Check to make sure value is correct
			`CMPQ	`DIR	`tIC	4				`_			// 1174 (from 1200)
			// - Zero should be set, Failure = 274
			`TZE	`DIR	`tIC	2				`_			// 1175
			`DIAG	`DIR	`tIC	9'o274		`_			// 1176
			`TRA	`DIR	`tIC	2				`_			// 1177
			`DATA					18'o000002	`_			// 1200
			// - Check shift of 3
			`ILQ					9'o001		`_			// 1201
			`QLS					6'o003		`_			// 1202
			// - Carry should not be set, Failure = 275
			`TNC	`DIR	`tIC	2				`_			// 1203
			`DIAG	`DIR	`tIC	9'o275		`_			// 1204
			// - Check to make sure value is correct
			`CMPQ	`DIR	`tIC	4				`_			// 1205
			// - Zero should be set, Failure = 276
			`TZE	`DIR	`tIC	2				`_			// 1206
			`DIAG	`DIR	`tIC	9'o276		`_			// 1207
			`TRA	`DIR	`tIC	2				`_			// 1210
			`DATA					18'o000010	`_			// 1211
			// - Check shift of 7
			`ILQ					9'o001		`_			// 1212
			`QLS					6'o007		`_			// 1213
			// - Carry should not be set, Failure = 277
			`TNC	`DIR	`tIC	2				`_			// 1214
			`DIAG	`DIR	`tIC	9'o277		`_			// 1215
			// - Check to make sure value is correct
			`CMPQ	`DIR	`tIC	4				`_			// 1216
			// - Zero should be set, Failure = 300
			`TZE	`DIR	`tIC	2				`_			// 1217
			`DIAG	`DIR	`tIC	9'o300		`_			// 1220
			`TRA	`DIR	`tIC	2				`_			// 1221
			`DATA					18'o000200	`_			// 1222
			// - Check shift of 15
			`ILQ					9'o001		`_			// 1223
			`QLS					6'o017		`_			// 1224
			// - Carry should not be set, Failure = 301
			`TNC	`DIR	`tIC	2				`_			// 1225
			`DIAG	`DIR	`tIC	9'o301		`_			// 1226
			// - Check to make sure value is correct
			`CMPQ	`DIR	`tIC	3				`_			// 1227
			// - Zero should be set, Failure = 302
			`TZE	`DIR	`tIC	3				`_			// 1230
			`DIAG	`DIR	`tIC	9'o302		`_			// 1231
			`DATA					18'o100000	`_			// 1232
			// - Check shift of 17
			`ILQ					9'o001		`_			// 1233
			`QLS					6'o021		`_			// 1234
			// - Negative should be set, Failure = 303
			`TMI	`DIR	`tIC	2				`_			// 1235
			`DIAG	`DIR	`tIC	9'o303		`_			// 1236
			// - Carry should be set, Failure = 304
			`TNC	`DIR	`tIC	2				`_			// 1237
			`TRA	`DIR	`tIC	2				`_			// 1240
			`DIAG	`DIR	`tIC	9'o304		`_			// 1241
			// - Check to make sure value is correct
			`CMPQ	`DIR	`tIC	4				`_			// 1242
			// - Zero should be set, Failure = 305
			`TZE	`DIR	`tIC	2				`_			// 1243
			`DIAG	`DIR	`tIC	9'o305		`_			// 1244
			`TRA	`DIR	`tIC	2				`_			// 1245
			`DATA					18'o400000	`_			// 1246
			// - Check shift of 18
			`ILQ					9'o001		`_			// 1247
			`QLS					6'o022		`_			// 1250
			// - Negative should not be set, Failure = 306
			`TPL	`DIR	`tIC	2				`_			// 1251
			`DIAG	`DIR	`tIC	9'o306		`_			// 1252
			// - Carry should be set, Failure = 307
			`TNC	`DIR	`tIC	2				`_			// 1253
			`TRA	`DIR	`tIC	2				`_			// 1254
			`DIAG	`DIR	`tIC	9'o307		`_			// 1255
			// - Check to make sure value is correct
			`CMPQ	`DIR	`tIC	3				`_			// 1256
			// - Zero should be set, Failure = 310
			`TZE	`DIR	`tIC	3				`_			// 1257
			`DIAG	`DIR	`tIC	9'o310		`_			// 1260
			`DATA					18'o000000	`_			// 1261
			
			// *******************************************************
			// Test 21: AOS
			// *******************************************************
			// - Store 1 to memory
			// - AOS
			// - Verify = 2
			// - Store 0777777 to memory
			// - AOS
			// - Verify Carry and Zero set
			// - Store 0377777 to memory
			// - AOS
			// - Verify Negative Set
			`ILA					1				`_			// 1262
			`STA	`DIR	`tIC	4				`_			// 1263 (to 1267)
			`AOS	`DIR	`tIC	3				`_			// 1264 (to 1267)
			// - Carry should be clear, failure = 311
			`TNC	`DIR	`tIC	3				`_			// 1265
			`DIAG	`DIR	`tIC	9'o311		`_			// 1266
			`DATA					18'o000000	`_			// 1267
			// - Zero should be clear, failure = 312
			`TNZ	`DIR	`tIC	2				`_			// 1270
			`DIAG	`DIR	`tIC	9'o312		`_			// 1271
			// - Negative should be clear, failure = 313
			`TPL	`DIR	`tIC	2				`_			// 1272
			`DIAG	`DIR	`tIC	9'o313		`_			// 1273
			// - Overflow should be clear, failure = 314
			`TOV	`DIR	`tIC	2				`_			// 1274
			`TRA	`DIR	`tIC	2				`_			// 1275
			`DIAG	`DIR	`tIC	9'o314		`_			// 1276			
			// - Verify value = 2
			`LDA	`DIR	`tIC	18'o777770	`_			// 1277 (from 1267)
			`ICMPA				9'o000002	`_			// 1300
			// - Zero should be set, failure = 315
			`TZE	`DIR	`tIC	2				`_			// 1301
			`DIAG	`DIR	`tIC	9'o315		`_			// 1302
			// - AOS to 0777777
			`LDA	`DIR	`tIC	5				`_			// 1303 (from 1310)
			`STA	`DIR	`tIC	5				`_			// 1304 (to 1311)
			`AOS	`DIR	`tIC	4				`_			// 1305 (to 1311)
			// - Zero should be set, failure = 316
			`TZE	`DIR	`tIC	4				`_			// 1306
			`DIAG	`DIR	`tIC	9'o316		`_			// 1307
			`DATA					18'o777777	`_			// 1310
			`DATA					18'o000000	`_			// 1311
			// - Carry should be set, failure = 317
			`TNC	`DIR	`tIC	2				`_			// 1312
			`TRA	`DIR	`tIC	2				`_			// 1313
			`DIAG	`DIR	`tIC	9'o317		`_			// 1314
			// - Negative should be clear, failure = 320
			`TPL	`DIR	`tIC	2				`_			// 1315
			`DIAG	`DIR	`tIC	9'o320		`_			// 1316
			// - Overflow should be set, failure 321
			`TOV	`DIR	`tIC	2				`_ 		// 1317
			`DIAG	`DIR	`tIC	9'o321		`_			// 1320
			// - AOS to 0377777
			`LDA	`DIR	`tIC	5				`_			// 1321 (from 1326)
			`STA	`DIR	`tIC	5				`_			// 1322 (to 1327)
			`AOS	`DIR	`tIC	4				`_			// 1323 (to 1327)
			// - Zero should not be set, failure = 322
			`TNZ	`DIR	`tIC	4				`_			// 1324
			`DIAG	`DIR	`tIC	9'o322		`_			// 1325
			`DATA					18'o377777	`_			// 1326
			`DATA					18'o000000	`_			// 1327
			// - Carry should not be set, failure = 323
			`TNC	`DIR	`tIC	2				`_			// 1330
			`DIAG	`DIR	`tIC	9'o323		`_			// 1331
			// - Negative should be set, failure = 324
			`TMI	`DIR	`tIC	2				`_			// 1332
			`DIAG	`DIR	`tIC	9'o324		`_			// 1333
			
			
			
			// ** Halt with a success code of 777 ***
			`DIAG	`DIR	`tIC	9'o777		`_
		end
		else
		begin
			data <= progMem[address];
			loadComplete <= ((address + 1) >= IP) ? 1'b1 : 1'b0;
		end
	end

endmodule
