// Perform AND mask of incoming data with given mask
module AndMask(
	input wire [0:17] mask,
	input wire [0:17] data,
	output wire [0:17] maskedData
);

assign maskedData = mask & data;

endmodule
