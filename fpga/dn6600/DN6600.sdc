#**************************************************************
# Create Clock
#**************************************************************
#create_clock -period "10.0 MHz" [get_ports ADC_CLK_10]
create_clock -period "50.0 MHz" [get_ports MAX10_CLK1_50]
#create_clock -period "50.0 MHz" [get_ports MAX10_CLK2_50]

#create_clock -period "50.0 MHz" -name MAX10_CLK1_50_VIRT

#set_false_path -from [get_ports {KEY[0]}] -to [all registers]
