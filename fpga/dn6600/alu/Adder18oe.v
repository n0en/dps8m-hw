// This module provides an 18 bit adder with an output enable
// that is suitable for OR'ing with other outputs.
module Addder18oe (
	input wire 	[0:17]	i_Addend1,
	input wire 	[0:17]	i_Addend2,
	input wire        	i_Carry,
	input wire				i_Add,
	input wire				i_Enable,
	output wire	[0:17] 	o_Sum,
	output wire       	o_Carry,
	output wire       	o_Overflow
);

	wire [0:18] w_Operand1;
	wire [0:18] w_Operand2;
	wire [0:19] w_Result;

   assign w_Operand1 = { i_Addend1[0], i_Addend1 };
	assign w_Operand2 = { i_Addend2[0], i_Addend2 };
	assign w_Result = (i_Add) ? (w_Operand1 + w_Operand2 + i_Carry)
									  : (w_Operand1 - w_Operand2 - i_Carry);
	assign o_Sum = (i_Enable) ? w_Result[2:19] : 18'b0;
	assign o_Carry = (i_Enable) ? ((i_Add) ? w_Result[0] : !w_Result[0]) : 1'b0;
	assign o_Overflow = (i_Enable) ? (w_Result[1] ^ w_Result[2]) : 1'b0;
	
endmodule