// This module will combine multiple OR enabled outputs to a single
// set of result lines.
module OutputCombiner(
	input wire	[0:17]	i_Adder18Result,
	input wire				i_Adder18Carry,
	input wire 				i_Adder18Overflow,
	
	input wire	[0:35]	i_Adder36Result,
	input wire				i_Adder36Carry,
	input wire 				i_Adder36Overflow,
	
	input wire				i_QuotientEnable,
	input wire	[0:35]	i_Quotient,
	
	input wire	[0:17]	i_AluResultLow,
	input wire	[0:17]	i_AluResultHigh,
	
	output wire	[0:17]	o_ResultLow,
	output wire	[0:17]	o_ResultHigh,
	output wire				o_Carry,
	output wire				o_Overflow
);

wire [0:35] w_Quotient;

assign w_Quotient = (i_QuotientEnable) ? i_Quotient : 36'b0;


assign o_ResultLow = i_Adder18Result | i_Adder36Result[18:35] | w_Quotient[18:35] | i_AluResultLow;
							
assign o_ResultHigh = i_Adder36Result[0:17] | w_Quotient[0:17] | i_AluResultHigh;

assign o_Carry = i_Adder18Carry | i_Adder36Carry;

assign o_Overflow = i_Adder18Overflow | i_Adder36Overflow;

endmodule
