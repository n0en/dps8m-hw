module AluController(
	input wire				i_Clock,					// System clock
	input wire				i_Reset,					// System reset signal
	input wire				i_Start,					// Start ALU operation signal
	input wire	[0:3]		i_AluOp,					// ALU Operation to perform
	input wire	[0:17]	i_RegA,					// Register A current value
	input wire	[0:17]	i_RegQ,					// Register Q current value
	input wire	[0:35] 	i_MemoryData,			// Value read from memory
	input wire	[0:5]		i_ShiftCount,			// Shift count for NRM/NRML
	input wire				i_Overflow,				// Overflow flag from CPU
	input wire	[0:35]	i_Quotient,				// Quotient from divide operation
	input wire	[0:17] 	i_Remainder,			// Remainder from divide operation
	
	output wire				o_Ready,					// ALU Operation complete signal
	output wire	[0:35]	o_Operand1,				// First operand for ALU Operation
	output wire	[0:35]	o_Operand2,				// Second operand for ALU Operation
	output wire				o_Carry,					// Carry input to adders
	output wire				o_Add,					// Add / not Subtract signal to adders
	output wire				o_Long,					// Long (36 bit) operation indicator
	output wire				o_AdderEnable18,		// Signal to enable 18 bit adder output
	output wire				o_AdderEnable36,		// Signal to enable 36 bit adder output
	output wire				o_QuotientEnable,		// Signal to enable Quotient output
	output wire [0:2]		o_State,					// Current state of ALU
	output wire	[0:17]	o_ResultLow,			// Low order 18 bits of result
	output wire [0:17]	o_ResultHigh,			// High order 18 bits of result
	output wire				o_DivideCheck			// Signal indicating that a divide check has occurred
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;
	
	// ALU Operations
	localparam ALU_OP_NOP				= 4'b0000;		// Do not perform any operation
	localparam ALU_OP_ADD_A_MEM		= 4'b0001;		// Add register A and memory
	localparam ALU_OP_SUB_A_MEM		= 4'b0010;		// Subtract memory from register A
	localparam ALU_OP_ADD_Q_MEM		= 4'b0011;		// Add register Q and memory
	localparam ALU_OP_SUB_Q_MEM		= 4'b0100;		// Subtract memory from register Q
	localparam ALU_OP_DVF				= 4'b0101;		// Divide AQ by memory
	localparam ALU_OP_NRM				= 4'b0110;		// Divide AQ by memory
	localparam ALU_OP_NRML				= 4'b0111;		// Divide AQ by memory
	localparam ALU_OP_ADD_AQ_MEM	 	= 4'b1000;		// Add register AQ and memory
	localparam ALU_OP_SUB_AQ_MEM	 	= 4'b1001;		// Subtract memory from register AQ
	
	// ALU States
	localparam STATE_IDLE				= 3'b000;		// Not doing anything
	localparam STATE_SETUP				= 3'b001;		// Set up for operation
	localparam STATE_EXTRA_CYCLE		= 3'b010;		// Wait extra cycle for external units
	localparam STATE_EXECUTE			= 3'b011;		// Execute operation
	localparam STATE_WAIT_NOT_START	= 3'b100;		// Wait for the start signal to go low
	
	// Internal registers
	reg r_Ready;
	reg r_LastStart;
	reg r_Carry;
	reg r_Add;
	reg r_Long;
	reg r_AdderEnable18;
	reg r_AdderEnable36;
	reg r_QuotientEnable;
	reg r_DivideCheck;
	reg [0:1] r_CycleCount;
	reg [0:2] r_CurrentState;
	reg [0:3] r_Operation;
	reg [0:35] r_Operand1;
	reg [0:35] r_Operand2;
	reg [0:17] r_ResultLow;
	reg [0:17] r_ResultHigh;
	
	// Event flags
	reg r_EventStart;
	
	// Wire outputs to registers
	assign o_Ready = r_Ready;
	assign o_Operand1 = r_Operand1;
	assign o_Operand2 = r_Operand2;
	assign o_Carry = r_Carry;
	assign o_Add = r_Add;
	assign o_Long = r_Long;
	assign o_AdderEnable18 = r_AdderEnable18;
	assign o_AdderEnable36 = r_AdderEnable36;
	assign o_QuotientEnable = r_QuotientEnable;
	assign o_DivideCheck = r_DivideCheck;
	assign o_State = r_CurrentState;
	assign o_ResultLow = r_ResultLow;
	assign o_ResultHigh = r_ResultHigh;


	always @ (posedge i_Clock, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			r_Ready <= LOW;
			r_CurrentState <= STATE_IDLE;
			r_LastStart <= LOW;
			r_Operand1 <= 36'b0;
			r_Operand2 <= 36'b0;
			r_AdderEnable18 <= LOW;
			r_AdderEnable36 <= LOW;
			r_QuotientEnable <= LOW;
			r_DivideCheck <= LOW;
			r_ResultLow <= 18'b0;
			r_ResultHigh <= 18'b0;
		end
		else
		begin
			if (!r_LastStart && i_Start && !r_EventStart)
			begin
				r_LastStart <= HIGH;
				r_EventStart <= HIGH;
			end
			else 
			begin
				r_LastStart <= i_Start;
			end
			
			case (r_CurrentState)
			
				STATE_IDLE:
				begin
					r_Ready <= LOW;
					r_AdderEnable18 <= LOW;
					r_AdderEnable36 <= LOW;
					r_QuotientEnable <= LOW;
					r_DivideCheck <= LOW;
					r_ResultLow <= 18'b0;
					r_ResultHigh <= 18'b0;
					r_CycleCount <= 2'b0;
					if (r_EventStart)
					begin
						r_Operation <= i_AluOp;
						r_CurrentState <= STATE_SETUP;
					end
					else
						r_CurrentState <= STATE_IDLE;
				end
				
				STATE_SETUP:
				begin
					case (r_Operation)
					
						ALU_OP_NOP:
						begin
							// Nothing to do
						end
							
						ALU_OP_ADD_A_MEM:
						begin
							r_Operand1[0:17] <= 18'b0;
							r_Operand1[18:35] <= i_RegA;
							r_Operand2[0:17] <= 18'b0;
							r_Operand2[18:35] <= i_MemoryData[18:35];
							r_Carry <= LOW;
							r_Add <= HIGH;
							r_Long <= LOW;
							r_AdderEnable18 <= HIGH;
						end
						
						ALU_OP_SUB_A_MEM:
						begin
							r_Operand1[0:17] <= 18'b0;
							r_Operand1[18:35] <= i_RegA;
							r_Operand2[0:17] <= 18'b0;
							r_Operand2[18:35] <= i_MemoryData[18:35];
							r_Carry <= LOW;
							r_Add <= LOW;
							r_Long <= LOW;
							r_AdderEnable18 <= HIGH;
						end
						
						ALU_OP_ADD_Q_MEM:
						begin
							r_Operand1[0:17] <= 18'b0;
							r_Operand1[18:35] <= i_RegQ;
							r_Operand2[0:17] <= 18'b0;
							r_Operand2[18:35] <= i_MemoryData[18:35];
							r_Carry <= LOW;
							r_Add <= HIGH;
							r_Long <= LOW;
							r_AdderEnable18 <= HIGH;
						end
						
						ALU_OP_SUB_Q_MEM:
						begin
							r_Operand1[0:17] <= 18'b0;
							r_Operand1[18:35] <= i_RegQ;
							r_Operand2[0:17] <= 18'b0;
							r_Operand2[18:35] <= i_MemoryData[18:35];
							r_Carry <= LOW;
							r_Add <= LOW;
							r_Long <= LOW;
							r_AdderEnable18 <= HIGH;
						end
							
						ALU_OP_DVF:
						begin
							// Check for negative
							if (i_RegA[0])
							begin
								r_Operand1 <= (~((i_RegA << 17) | (i_RegQ >> 1))) + 36'b1;
							end
							else
							begin
								r_Operand1 <= (i_RegA << 17) | (i_RegQ >> 1);
							end
							r_Operand2[0:17] <= 18'b0;
							r_Operand2[18:35] <= i_MemoryData[18:35];
//							r_QuotientEnable <= HIGH;
							r_CycleCount <= 2'b10;
						end
							
						ALU_OP_NRM:
						begin
							r_Long <= LOW;
						end
							
						ALU_OP_NRML:
						begin
							r_Long <= HIGH;
						end
						
						ALU_OP_ADD_AQ_MEM:
						begin
							r_Operand1[0:17] <= i_RegA;
							r_Operand1[18:35] <= i_RegQ;
							r_Operand2 <= i_MemoryData;
							r_Carry <= LOW;
							r_Add <= HIGH;
							r_Long <= HIGH;
							r_AdderEnable36 <= HIGH;
						end
						
						ALU_OP_SUB_AQ_MEM:
						begin
							r_Operand1[0:17] <= i_RegA;
							r_Operand1[18:35] <= i_RegQ;
							r_Operand2 <= i_MemoryData;
							r_Carry <= LOW;
							r_Add <= LOW;
							r_Long <= HIGH;
							r_AdderEnable36 <= HIGH;
						end
							
						default:
						begin
							// Nothing to do
						end
						
					endcase

					r_CurrentState <= STATE_EXTRA_CYCLE;
				end
				
				STATE_EXTRA_CYCLE:
					if (r_CycleCount == 0)
						r_CurrentState <= STATE_EXECUTE;
					else
						r_CycleCount <= r_CycleCount - 2'b01;
				
				STATE_EXECUTE:
				begin
					case (r_Operation)
					
						ALU_OP_NRM:
						begin
							if (i_RegA != 18'b0)
							begin
								if (i_Overflow)
									r_ResultLow <= i_RegA[0] ? (i_RegA >> 1) & 18'o377777 : (i_RegA >> 1) | 18'o400000;
								else
									r_ResultLow <= i_RegA << i_ShiftCount;
							end
							else
								r_ResultLow <= i_RegA;
						end

						ALU_OP_NRML:
						begin
							if ((i_RegA != 18'b0) || (i_RegQ != 18'b0))
								{r_ResultHigh,r_ResultLow} <= {i_RegA,i_RegQ} << i_ShiftCount;
							else
								{r_ResultHigh,r_ResultLow} <= {i_RegA,i_RegQ};
						end
						
						ALU_OP_DVF:
						begin
							if (r_Operand1 >= (r_Operand2 << 18))
							begin
								if (i_RegA[0])
									{r_ResultHigh,r_ResultLow} <= {~i_RegA,~i_RegQ} + 1;
								else
									{r_ResultHigh,r_ResultLow} <= {i_RegA,(i_RegQ << 1)};
								r_DivideCheck <= HIGH;
							end
							else
							begin
								if (i_RegA[0] ^ i_MemoryData[0])
									r_ResultHigh <= (~i_Quotient + 1) & 18'o777777;
								else
									r_ResultHigh <= i_Quotient[18:35];
								if (i_MemoryData[0])
									r_ResultLow <= (~i_Remainder + 1) & 18'o777777;
								else
									r_ResultLow <= i_Remainder;
							end
							
						end
						
						default:
						begin
							// Nothing to do here
						end

					endcase
					
					r_CurrentState <= STATE_WAIT_NOT_START;
				end
				
				STATE_WAIT_NOT_START:
				begin
					r_Ready <= HIGH;
					if (!i_Start)
					begin
						r_EventStart <= LOW;
						r_CurrentState <= STATE_IDLE;
					end
				end
				
				default:
					r_CurrentState <= STATE_IDLE;
			endcase
			
		end
	end

endmodule
