module IomCentral (
	input wire				i_Clk,							// System clock
	input wire				i_Reset,							// System reset signal
	input wire	[0:5]		i_Select,						// Select register
	input wire				i_Connect,						// CIOC signal
	input	wire	[0:35]   i_PCW,							// CIOC operand
	input wire				i_FaultAck,						// CPU acknowledges IOM fault
	input wire	[0:17]	i_ElapsedTimer,				// Current elapsed timer contents
	input wire	[0:17]	i_IntervalTimer,				// Current interval timer contents
	input wire				i_IntervalTimerRunout,		// Interval timer runnout signal
	input wire				i_ElapsedTimerRollover,		// Elapsed timer has rolled over signal
	input wire				i_unused0,						// [unused]
	input wire	[0:17]	i_IntLevelData,				// Interrupt sub-level flags for selected interrupt level
	input wire				i_MailBoxEnable,				// Signal that this is a mailbox cycle
	input wire	[0:17]	i_MailBoxIn,					// Incoming data to write to mailbox
	input wire	[0:15]	i_MailBoxAddress,				// Address of mailbox to read/write
	input wire				i_WriteEnable,					// Signal that this is a write cycle
	
	output reg				o_Fault,							// Signal to CPU of an IOM fault
	output reg	 			o_IntervalTimerEnable,		// Signal to enable interval timer
	output wire	 			o_IntervalTimerLoad,			// Signal to interval timer to load o_IntervalTimerData
	output reg	[0:17] 	o_IntervalTimerData,			// Data for interval timer to load on o_IntervalTimerLoad
	output reg	 			o_IntervalTimerRunoutAck,	// Signal to acknowledge i_IntervalTimerRunout
	output reg				o_IntSvc,						// Signal to Interrupt Controller that IOM is requesting an interrupt service
	output reg	[0:1]		o_unused1,						// [unused]
	output reg	[0:7]		o_IntSvcVec,					// The interrupt vector the service is being requested for when o_IntSvc is active
	output wire [0:3]		o_State,							// Current IOM Central State
	output reg	[0:17]	o_MailBoxOut,					// Output contents of mailbox address
	output reg				o_tp,								// Test point
	output wire	 			o_ElapsedTimerLoad,			// Signal to elapsed timer to load o_ElapsedTimerData
	output reg	[0:17] 	o_ElapsedTimerData,			// Data for elapsed timer to load on o_ElapsedTimerLoad
	output reg	[0:17]	o_MailboxWriteData,			// Last data captured to write to mailbox
	
	// Memory Controller I/O
	input	wire	[0:17]	i_MemDataIn,					// Data from the MC for a read operation
	input wire				i_MemFault,						// Fault indication from MC
	input wire				i_MemReady,						// Signal from MC that operation is complete
	output reg				o_MemStart,						// Start signal for MC to begin operation
	output reg	[0:2]		o_MOP,							// Memory operation for MC to perform
	output reg	[0:15]	o_MemAddrOut,					// Address for MC to operate on
	output reg	[0:17]	o_MemDataOut,					// Data for MC to write to memory on write operation
	
	output reg				o_MailBoxReady,				// Signal indicating mailbox read/write is complete
	
	input wire				i_unused2,
	input wire	[0:3]		i_unused3,
	input wire	[0:15]	i_unused4,
	
	output reg				o_unused5,
	output reg	[0:3]		o_unused6,
	output reg	[0:15]	o_unused7,
	
	output reg	[0:1]		o_AnswerBits,					// Value to write to the answer bits on an INT_SVC_WRITE_REG (non-SIC)
	output reg				o_SetResetInt,					// Flag indicating if an interrupt should be set or reset
	output reg	[0:3]		o_AnswerLevel,					// Interrupt level to write the answer bits to
	output reg				o_WriteAnswer,					// Signal to write the answer bits
	output reg	[0:3]		o_IntReadLevel					// Interrupt level register to read

);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// Interrupt Service Requests
	localparam INT_SVC_SET_INT		= 2'b00;		// Set an interrupt
	localparam INT_SVC_RESET_INT	= 2'b01;		// Clear an interrupt (ack)
	localparam INT_SVC_READ_REG	= 2'b10;		// Read the sub-level flags for a specific interrupt level
	localparam INT_SVC_WRITE_REG	= 2'b11;		// Write the "answered" bits for a specific interrupt level
	
	// Main FSM States
	localparam MAIN_STATE_IDLE									= 4'b0000;
	localparam MAIN_STATE_SETTING_INTERVAL_INTERRUPT	= 4'b0001;
	localparam MAIN_STATE_SETTING_ROLLOVER_INTERRUPT	= 4'b0010;
	localparam MAIN_STATE_REQUESTING_INT_SVC				= 4'b0011;
	localparam MAIN_STATE_PROCESSING_CONNECT_PCW			= 4'b0100;
	localparam MAIN_STATE_MAILBOX_READ_WRITE				= 4'b0101;
	localparam MAIN_STATE_WAITING_MAILBOX_LOW				= 4'b0110;
	localparam MAIN_STATE_READ_INT_CELLS					= 4'b1001;
	
	// Input signal state tracking flags
	reg r_LastIntervalTimerRunnout;
	reg r_LastElapsedTimerRollover;
	reg r_LastConnect;
	reg r_LastMailBoxEnable;
	
	// Event flags
	reg r_EventIntervalTimerRunnout;
	reg r_EventElapsedTimerRollover;
	reg r_EventCpuConnected;
	reg r_EventMailBoxAccess;
	
	// Current Main FSM State
	reg [0:3] r_MainState;
	
	// Captured PCW & Select Register
	reg [0:35] r_CapturedPCW;
	reg [0:5] r_CapturedSelect;
	
	// Output signal generation
	reg [0:1] r_ElapsedTimerLoadCount;
	reg [0:1] r_IntervalTimerLoadCount;
	
	assign o_ElapsedTimerLoad = (r_ElapsedTimerLoadCount != 0) ? HIGH : LOW;
	assign o_IntervalTimerLoad = (r_IntervalTimerLoadCount != 0) ? HIGH : LOW;
	assign o_State = r_MainState;
	
	always @ (posedge i_Clk, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			o_Fault <= LOW;
			o_IntervalTimerEnable <= LOW;
			o_IntSvc <= LOW;
			o_IntervalTimerData <= LOW;
			o_IntervalTimerRunoutAck <= LOW;
			o_IntSvcVec <= 8'b0;
			o_tp <= LOW;
			o_MailBoxReady <= LOW;
			r_LastIntervalTimerRunnout <= LOW;
			r_LastElapsedTimerRollover <= LOW;
			r_LastConnect <= LOW;
			r_LastMailBoxEnable <= LOW;
			r_EventIntervalTimerRunnout <= LOW;
			r_EventElapsedTimerRollover <= LOW;
			r_EventMailBoxAccess <= LOW;
			r_CapturedPCW <= 36'b0;
			r_ElapsedTimerLoadCount <= 2'b0;
			r_IntervalTimerLoadCount <= 2'b0;
			r_MainState <= MAIN_STATE_IDLE;
		end
		else
		begin
			// Count down output load signals if needed
			if (r_ElapsedTimerLoadCount != 0) r_ElapsedTimerLoadCount <= r_ElapsedTimerLoadCount - 2'b01;
			if (r_IntervalTimerLoadCount != 0) r_IntervalTimerLoadCount <= r_IntervalTimerLoadCount - 2'b01;

			// Then do event detection
			r_EventMailBoxAccess <= (!r_LastMailBoxEnable & i_MailBoxEnable) ? HIGH : r_EventMailBoxAccess;
			r_LastMailBoxEnable <= i_MailBoxEnable;
			
			r_EventIntervalTimerRunnout <= (!r_LastIntervalTimerRunnout & i_IntervalTimerRunout) ? HIGH : r_EventIntervalTimerRunnout;
			r_LastIntervalTimerRunnout <= i_IntervalTimerRunout;

			r_EventElapsedTimerRollover <= (!r_LastElapsedTimerRollover & i_ElapsedTimerRollover) ? HIGH : r_EventElapsedTimerRollover;
			r_LastElapsedTimerRollover <= i_ElapsedTimerRollover;
			
			if (!r_LastConnect & i_Connect)
			begin
				// Capture the CIOC parameters
				r_CapturedPCW <= i_PCW;
				r_CapturedSelect <= i_Select;
				r_EventCpuConnected <= HIGH;
			end
			else
			begin
				r_EventCpuConnected <= r_EventCpuConnected;
				r_LastConnect <= i_Connect;
			end
			

			// Now process Main state machine
			case (r_MainState)
			
				MAIN_STATE_IDLE:
				begin
					o_MailBoxReady <= LOW;				
					o_IntervalTimerRunoutAck <= LOW;
					o_IntSvc <= LOW;
					o_WriteAnswer <= LOW;
					if (r_EventIntervalTimerRunnout)
					begin
						r_MainState <= MAIN_STATE_SETTING_INTERVAL_INTERRUPT;
						r_EventIntervalTimerRunnout <= LOW;
					end
					else if (r_EventElapsedTimerRollover)
					begin
						r_MainState <= MAIN_STATE_SETTING_ROLLOVER_INTERRUPT;
						r_EventElapsedTimerRollover <= LOW;
					end
					else if (r_EventCpuConnected)
					begin
						r_MainState <= MAIN_STATE_PROCESSING_CONNECT_PCW;
						r_EventCpuConnected <= LOW;
					end
					else if (r_EventMailBoxAccess)
					begin
						r_MainState <= MAIN_STATE_MAILBOX_READ_WRITE;
						r_EventMailBoxAccess <= LOW;
					end
					else
						r_MainState <= MAIN_STATE_IDLE;
				end
				
				MAIN_STATE_SETTING_INTERVAL_INTERRUPT:
				begin
					o_IntSvcVec <= 8'o361;
					o_SetResetInt <= HIGH;
					o_IntervalTimerRunoutAck <= HIGH;	
					r_MainState <= MAIN_STATE_REQUESTING_INT_SVC;
				end
				
				MAIN_STATE_SETTING_ROLLOVER_INTERRUPT:
				begin
					o_IntSvcVec <= 8'o362;
					o_SetResetInt <= HIGH;
					r_MainState <= MAIN_STATE_REQUESTING_INT_SVC;
				end
				
				MAIN_STATE_REQUESTING_INT_SVC:
				begin
					o_IntSvc <= HIGH;
					r_MainState <= MAIN_STATE_IDLE;
				end
				
				MAIN_STATE_PROCESSING_CONNECT_PCW:
				begin
					// Channel o77 is the timer control channel
					if (r_CapturedSelect == 6'o77)
					begin
						o_IntervalTimerEnable <= (r_CapturedPCW & 36'o000000010000) == 0;
					end
					r_MainState <= MAIN_STATE_IDLE;
				end
				
				MAIN_STATE_MAILBOX_READ_WRITE:
				begin
					if (i_WriteEnable)
					begin
						o_MailboxWriteData <= i_MailBoxIn;
						casez (i_MailBoxAddress)
							16'b000000010000????:		// Matches o400 thru 0417
								begin
									o_AnswerLevel <= i_MailBoxAddress[12:15];
									o_AnswerBits <= i_MailBoxIn[16:17];
									o_WriteAnswer <= HIGH;
									r_MainState <= MAIN_STATE_IDLE;
								end
							16'o00450:
								begin
									o_IntervalTimerData <= i_MailBoxIn;
									r_IntervalTimerLoadCount <= 2'b10;
									r_MainState <= MAIN_STATE_WAITING_MAILBOX_LOW;
								end
							16'o00451:
								begin
									o_ElapsedTimerData <= i_MailBoxIn;
									r_ElapsedTimerLoadCount <= 2'b10;
									r_MainState <= MAIN_STATE_WAITING_MAILBOX_LOW;
								end
							default: 
							begin
								o_Fault <= HIGH;
								r_MainState <= MAIN_STATE_WAITING_MAILBOX_LOW;
							end
						endcase
					end
					else
					begin
						casez (i_MailBoxAddress)						
							16'b000000010000????:		// Matches o400 thru 0417
								begin
									o_IntReadLevel <= i_MailBoxAddress[12:15];
									r_MainState <= MAIN_STATE_READ_INT_CELLS;
								end
							
							16'o00450:
								begin
									o_MailBoxOut <= i_IntervalTimer;
									r_MainState <= MAIN_STATE_WAITING_MAILBOX_LOW;
								end
								
							16'o00451:	
								begin
									o_MailBoxOut <= i_ElapsedTimer;
									r_MainState <= MAIN_STATE_WAITING_MAILBOX_LOW;
								end
						endcase
					end
				end
				
				MAIN_STATE_WAITING_MAILBOX_LOW:
				begin
					o_MailBoxReady <= HIGH;
					r_MainState <= (i_MailBoxEnable) ? MAIN_STATE_WAITING_MAILBOX_LOW : MAIN_STATE_IDLE;
				end

				MAIN_STATE_READ_INT_CELLS:
				begin
					o_MailBoxOut <= i_IntLevelData;
					r_MainState <= MAIN_STATE_WAITING_MAILBOX_LOW;						
				end
				
				default: r_MainState <= MAIN_STATE_IDLE;
			endcase
		end
	end

		
endmodule