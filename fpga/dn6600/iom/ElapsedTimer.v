module ElapsedTimer(
	input wire 				i_Clk,			// System clock
	input wire 				i_Reset,			// System reset
	input wire 				i_Load,			// Load i_DataIn into timer
	input wire 	[0:17] 	i_DataIn,		// Value to load into timer on i_Load

	output wire				o_Rollover,		// Timer rollover signal
	output reg	[0:17]	o_Timer			// Timer current value
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	reg [0:5] r_RolloverIRQcount;
	reg r_TimerClkHigh;
	reg [0:15] r_ClockGateCounter;
	
	wire w_GatedClockEvent;
	
	assign o_Rollover = (r_RolloverIRQcount != 0) ? HIGH : LOW;
	
	assign w_GatedClockEvent = (r_ClockGateCounter == 16'b0) ? HIGH : LOW;
	
	always @ (posedge i_Clk, posedge i_Reset)
	begin
		if (i_Reset)
			r_ClockGateCounter <= 16'b1;
		else if (r_ClockGateCounter == 50000)
			r_ClockGateCounter <= 16'b0;
		else
			r_ClockGateCounter <= r_ClockGateCounter + 16'b1;
	end
	
	always @ (posedge i_Clk, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			r_RolloverIRQcount <= 2'b0;
			r_TimerClkHigh <= LOW;
			o_Timer <= 18'b0;
		end
		else if (i_Load)
			o_Timer <= i_DataIn;
		else if (w_GatedClockEvent && !r_TimerClkHigh)
		begin
			r_TimerClkHigh <= HIGH;
			if (o_Timer == 18'o777777)
			begin
				r_RolloverIRQcount <= 6'b000011;
			end
			o_Timer <= o_Timer + 18'b1;
		end
		else if (!w_GatedClockEvent && r_TimerClkHigh)
			r_TimerClkHigh <= LOW;
		else if (r_RolloverIRQcount != 0)
			r_RolloverIRQcount <= r_RolloverIRQcount - 2'b01;
		else
			r_RolloverIRQcount <= r_RolloverIRQcount;
	end

endmodule