module IOChannelMux(
	input wire 				i_Clk,				// System clock
	input wire 				i_Reset,				// System reset
	input wire	[0:15] 	i_IOCAddress,		// Address from IOM Central
	input wire 	[0:17] 	i_IOCData,			// Data from IOM Central
	input wire	[0:5] 	i_IOCChanSelect,	// Channel select from IOM Central
	input wire 	[0:34]	i_Slot0,				// Slot 0 Bus Connection
	input wire 	[0:34]	i_Slot1,				// Slot 1 Bus Connection
	input wire 	[0:34]	i_Slot2,				// Slot 2 Bus Connection
	input wire 	[0:34]	i_Slot3,				// Slot 3 Bus Connection
	
	output reg 	[0:15]	o_Address,			// Captured address output
	output reg 	[0:17] 	o_Data,				// Captured data output
	output reg	[0:5]		o_ChanSelect,		// Captured channel selection
	output reg				o_ChanStrobe,		// Strobe to indicate successful capture
	output reg				o_Slot0Ack,			// Ack signal to slot 0
	output reg				o_Slot1Ack,			// Ack signal to slot 1
	output reg				o_Slot2Ack,			// Ack signal to slot 2
	output reg				o_Slot3Ack			// Ack signal to slot 3
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;


endmodule