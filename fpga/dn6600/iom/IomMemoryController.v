// This module is the memory controller used to support the CPU in memory operations
module IomMemoryController (
	// General I/O
	input wire 				i_Clk,					// System clock
	input wire 				i_Reset,					// System i_Reset signal
	output reg 	[0:3] 	o_ControllerState,	// Current MEMORY cycle state
	
	// I/O between Controller and IOM Central
	input wire 				i_Start,					// Initiate current i_MOP on rising edge
	input wire 	[0:2] 	i_MOP,					// Memory Operation
	input wire 	[0:15] 	i_AddrIn,				// Address input
	input wire 	[0:17] 	i_DataIn,				// Word input for writing to memory
	output reg 	[0:17] 	o_DataOut,				// Word memory read results
	output reg 				o_Fault,					// Flag indicating memory o_Fault has occurred
	output reg 				o_Ready,					// Flag indicating operation is complete
	
	// DMA I/O Control Signals
	input wire				i_DmaAck,				// DMA Acknowledge from CPU
	output reg				o_DmaReq,				// DMA Request to CPU
	
	// I/O between Controller and Memory Module
	input wire 	[0:17] 	i_MemReadData,			// Data read from memory on read cycle
	output reg 				o_MemChipSelect, 		// Chip select signal for selecting the RAM module
	output reg 				o_MemWriteEnable,		// Write / not Read signal for the memory cycle
	output reg 	[0:15]	o_MemAddress, 			// The memory address the controller wants to read or write
	output reg 	[0:17] 	o_MemWriteData, 		// Data to write on memory write cycle
	input wire 				i_MemFault				// Memory module indicated o_Fault
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// Memory Operation Requested
	`define MOP_NOP					3'b000
	`define MOP_REQUEST_DMA			3'b001
	`define MOP_READ_WORD			3'b010
	`define MOP_READ_WORD_DONE		3'b011
	`define MOP_WRITE_WORD			3'b100
	`define MOP_WRITE_WORD_DONE	3'b101

	// Controller cycle state
	`define MC_IDLE						4'b0000
	`define MC_REQUESTING_DMA			4'b0001
	`define MC_DMA_GRANTED				4'b0010
	`define MC_WRITING_WORD				4'b0100
	`define MC_FINISH_WRITE				4'b0101
	`define MC_READING_WORD				4'b1001
	`define MC_FINISHING_READ			4'b1010
		
	// Internal storage
	reg r_lastStart;
	reg r_endDmaCycle;

	always @ (posedge(i_Clk), posedge(i_Reset))
	begin
		if (i_Reset)
		begin
			o_ControllerState <= `MC_IDLE;
			r_lastStart <= LOW;
			r_endDmaCycle <= LOW;
			o_Ready <= LOW;
			o_MemAddress <= 16'b0;
			o_MemWriteData <= 18'b0;
			o_MemWriteEnable <= LOW;
			o_MemChipSelect <= LOW;
			o_DmaReq <= LOW;
		end
		else
		begin
			case (o_ControllerState)
				`MC_IDLE:
				begin
					// Drop our o_Ready line when i_Start goes away
					if (!i_Start)
						o_Ready <= LOW;
						
					// Check if we have a i_Start signal rising edge
					if (!r_lastStart & i_Start)
					begin
						case (i_MOP)
							`MOP_NOP:
							begin
								o_MemAddress <= i_AddrIn;
								o_MemWriteData <= 18'b0;
								o_MemWriteEnable <= LOW;
								o_MemChipSelect <= LOW;
								o_ControllerState <= `MC_IDLE;
								o_Fault <= LOW;
							end
							`MOP_REQUEST_DMA:
							begin
								o_ControllerState <= `MC_REQUESTING_DMA;
								o_Fault <= LOW;
							end
							`MOP_READ_WORD:
							begin
								o_MemAddress <= i_AddrIn;
								o_MemWriteData <= 18'b0;
								o_MemWriteEnable <= LOW;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_READING_WORD;
								o_Fault <= LOW;
								r_endDmaCycle <= LOW;
							end
							`MOP_READ_WORD_DONE:
							begin
								o_MemAddress <= i_AddrIn;
								o_MemWriteData <= 18'b0;
								o_MemWriteEnable <= LOW;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_READING_WORD;
								o_Fault <= LOW;
								r_endDmaCycle <= HIGH;
							end
							`MOP_WRITE_WORD:
							begin
								o_MemAddress <= i_AddrIn;
								o_MemWriteData <= i_DataIn;
								o_MemWriteEnable <= HIGH;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_WRITING_WORD;
								o_Fault <= LOW;
								r_endDmaCycle <= LOW;
							end
							`MOP_WRITE_WORD_DONE:
							begin
								o_MemAddress <= i_AddrIn;
								o_MemWriteData <= i_DataIn;
								o_MemWriteEnable <= HIGH;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_WRITING_WORD;
								o_Fault <= LOW;
								r_endDmaCycle <= HIGH;
							end
							default:
							begin
								o_MemAddress <= 16'b0;
								o_MemWriteData <= 18'b0;
								o_MemWriteEnable <= LOW;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_IDLE;
								o_Fault <= HIGH;
							end
						endcase
					end
					r_lastStart <= i_Start;
				end
				
				`MC_REQUESTING_DMA:
				begin
					o_DmaReq <= HIGH;
					o_ControllerState <= (i_DmaAck) ? `MC_WRITING_WORD : `MC_DMA_GRANTED;
				end
				
				`MC_DMA_GRANTED:
				begin
					o_Ready <= HIGH;
					o_ControllerState <= `MC_IDLE;
				end
				
				`MC_WRITING_WORD:
				begin
					if (i_MemFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_FINISH_WRITE;
				end
				
				`MC_FINISH_WRITE:
				begin
					o_MemWriteEnable <= LOW;
					o_MemChipSelect <= LOW;
					o_Ready <= HIGH;
					o_DmaReq <= !r_endDmaCycle;
					if (i_MemFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_IDLE;
				end
				
				`MC_READING_WORD:
				begin
					if (i_MemFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_FINISHING_READ;
				end
				
				`MC_FINISHING_READ:
				begin
					o_DataOut <= i_MemReadData;
					o_MemChipSelect <= LOW;
					o_Ready <= HIGH;
					o_DmaReq <= !r_endDmaCycle;
					if (i_MemFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_IDLE;
				end
				
				default: 
				begin
					o_Fault <= HIGH;
					o_ControllerState <= `MC_IDLE;
				end
				
			endcase
		end
	end
endmodule
