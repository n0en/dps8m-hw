module IomProcessor (
	input  wire				clk,					// System clock
	input  wire				reset,				// System reset signal

	input  wire [5:0]		select,				// Select register
	
	output reg [17:0]		stexData,			// STEX data

	input  wire				connectSignal,		// CIOC signal
	input	 wire [35:0]   pcw,					// CIOC operand
	
	input  wire				intAck,				// CPU acknowledges interrupt
	
	output wire				fault,
	input  wire				faultAck,
	
	input wire [17:0]		elapsedTimer,
	
	input wire [17:0]		intervalTimer,
	input wire				intervalTimerRunout,
	output reg	 			intervalTimerEnable,
	output reg	 			intervalTimerLoad,
	output reg [17:0] 	intervalTimerData,
	output wire	 			intervalTimerRunoutAck,
	output wire				intervalTimerIRQ
	
);

	// Timer IRQ generation
	reg [1:0] elapsedTimerIRQCount;
	reg [1:0] intervalTimerIRQCount;
	
	assign intervalTimerIRQ = intervalTimerIRQCount != 0;
	assign intervalTimerRunoutAck = intervalTimerIRQCount != 0;
	
	
	// connect state
	reg connect;
	reg lastConnect;
	
	reg intReqReg;
	reg intAckReg;
	
	reg [5:0] selectReg;
	reg [35:0] pcwReg;
	
	// Timers
	//		Interval timer
	//			mbox 450
	//			runout vector 361
	
	//		Elapsed timer
	//			mbx	451
	//			rollover vector 362
	
	//		Channel 77
	//			PCW mask bit (23)
	//				1: Interval timer turned off
	//				0:	Interval timer turned on
	//			STEX returns maintenance channel switches
	
	
	// Connect signal state machine
	reg [2:0] iomState;
	localparam IOM_IDLE				= 3'b000;
	localparam IOM_CONNECT			= 3'b001;	// CIOC connect received
	localparam IOM_CONNECT_DONE 	= 3'b010;
	localparam IOM_FAULT    		= 3'b111;
	
	always @ (posedge(clk), posedge(reset))
		begin
			if (reset)
				begin
					iomState <= IOM_IDLE;
					connect <= 0;
					lastConnect <= 0;
					intervalTimerEnable <= 0; // TODO unsure of the initial state
					intervalTimerIRQCount <= 0;
					elapsedTimerIRQCount <= 0;
				end
			else
				// Timer housekeeping
				
				//TODO: Fix this, problem with clock edges
/*				if ((intervalTimerIRQCount == 0) && intervalTimerRunout)
					intervalTimerIRQCount <= 2'b11;	// Generate a 3 clock interrupt pulse
				else if (intervalTimerIRQCount != 0)
					intervalTimerIRQCount <= intervalTimerIRQCount - 2'b01;*/
				
				case (iomState)
					IOM_IDLE:
						begin
							// Connect signal
							if (connectSignal && ! lastConnect)
								begin
									connect <= 1;
									lastConnect <= 1;
									
									iomState <= IOM_CONNECT;
									selectReg <= select;
									pcwReg <= pcw;
								end
							else if (! connectSignal && lastConnect)
								begin
									connect <= 0;
									lastConnect <= 0;
								end
								
						end
					IOM_CONNECT:
						begin
							case (selectReg)
								6'o77: // Timer
									begin
										if (pcwReg[35-23])
											intervalTimerEnable <= 0;
										else
											intervalTimerEnable <= 1;
									end
							endcase
							iomState <= IOM_CONNECT_DONE;
						end
					IOM_CONNECT_DONE:
						begin
							connect <= 0;
							iomState <= IOM_IDLE;
						end
				endcase
				
				case (select)
					6'o77:
						stexData = 18'o123456; // TODO implement data switches
					default:
						stexData = 0;
				endcase
		end // iomState machine
		
	endmodule