module ConnectLatch(
	input wire reset,				// System reset
	input wire cpuConnect,		// Connect signal from CPU
	input wire [35:0] cpuPCW,	// PCW from CPU when Connect signal goes high
	input wire [5:0] cpuSreg,	// S Register from CPU when Connect signal goes high
	input wire clear,				// Clear signal from IOM (drops iomConnect)
	output reg iomConnect,		// Connect signal to IOM
	output reg [35:0] iomPCW,	// PCW to IOM
	output reg [5:0] iomSreg	// S Register to IOM
);

always @ (posedge reset, posedge cpuConnect, posedge clear)
begin
	if (reset || clear)
	begin
		iomPCW <= 36'b0;
		iomSreg <= 6'b0;
		iomConnect <= 1'b0;
	end
	else if (cpuConnect)
	begin
		iomPCW <= cpuPCW;
		iomSreg <= cpuSreg;
		iomConnect <= 1'b1;
	end
	else
	begin
		iomPCW <= iomPCW;
		iomSreg <= iomSreg;
		iomConnect <= iomConnect;
	end
end

endmodule