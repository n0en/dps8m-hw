// This module monitors the I/O bus for a specific channel number
// and captures the bus data when that channel number is seen.
module ChannelInterfaceBase(
	input wire				i_Reset,				// System reset
	input wire	[0:5]		i_ChannelSelect,	// Channel select from IOM Central
	input wire	[0:15]	i_ChannelAddress,	// Channel address from IOM Central to capture
	input wire	[0:17]	i_ChannelData,		// Channel data from IOM Central to capture
	input wire				i_ChannelWrite,	// When active the operation is a write, otherwise it's a read
	input wire				i_ChannelStrobe,	// Strobe to trigger channel comparison and capture on match
	
	output reg				o_ChannelRequest,	// Signal that a request has been captured from the I/O bus
	output reg	[0:15]	o_Address,			// The address that was captured
	output reg	[0:17]	o_Data,				// The data that was captured
	output reg				o_Write				// When active operation is a write
);

	parameter CHANNEL	= -1;
	
	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	always @ (posedge i_ChannelStrobe, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			o_ChannelRequest <= LOW;
		end
		else
		begin
			if (i_ChannelSelect == CHANNEL)
			begin
				o_Address <= i_ChannelAddress;
				o_Data <= i_ChannelData;
				o_Write <= i_ChannelWrite;
			end
		end
	end

endmodule