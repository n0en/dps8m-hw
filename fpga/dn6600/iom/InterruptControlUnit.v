module InterruptControlUnit(
	input wire				i_Reset,				// System reset signal
	input wire				i_Clock,				// System clock
	input wire 	[0:15] 	i_IntEnable,		// Interrupt level enables from CPU
	input wire				i_SIC,				// Signal from CPU to execute a Set Interrupt Cell instruction
	input wire	[0:3]		i_SicLevel,			// Indicates which interrupt level to set for SIC
	input wire	[0:15]	i_SicMask,			// Indicates which bits to set for SIC
	input wire				i_SetInt,			// Signal to set a specific interrupt
	input wire				i_SetReset,			// When high indicates to set the vector otherwise reset the vector
	input wire	[0:7]		i_SetVector,		// Interrupt vector to set or reset on i_SetInt
	input wire				i_IntAck,			// Signal from CPU acknowledging receipt of pending interrupt
	input wire				i_WriteAnswer,		// Signal to write answer bits for specifc level
	input wire	[0:3]		i_AnswerLevel,		// Interrupt level to write answer bits
	input wire	[0:1]		i_Answer,			// Answer bits to write
	input wire	[0:3]		i_ReadLevel,		// Determine which interrupt register is currently being output
	input wire				i_IntLock,			// A signal that indicates the CPU may be reading the interrupt vector
	
	output reg				o_IntPending,		// Flag to CPU to indicate a pending interrupt
	output reg	[0:7]		o_IntVector,		// Pending interrupt vector
	output wire	[0:17]	o_InterruptReg		// Contents of interrupt register indicated by i_ReadLevel
);

	/* Important note:
	 *   When the CPU is going to respond to a pending interrupt it must do these things in order:
	 *     1. Assert i_IntLock to prevent the interrupt vector from changing.
	 *     2. Read the o_IntVector and persist the contents somewhere else.
	 *     3. Assert i_IntAck to acknowledge that the interrupt vector has been read and accepted.
	 *     4. Release i_IntAck and i_IntLock (in any order or at the same time).
	 *
	 *   o_IntPending will always go low for at least one cycle after i_IntAck has been asserted, even
	 *   if there is another interrupt pending. If that is the case, o_IntPending will be asserted again
	 *   on the next clock cycle.
	 *
	 *   i_IntLock can be asserted and released, even if not responding to an interrupt. This will just cause
	 *   interrupt priorities to not be constantly evalutated while i_IntLock is asserted. By doing this,
	 *   i_IntLock can be associated unconditionally with CPU Operation unit states.
	*/

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;	

	// Interrupt registers with answer bits
	reg [0:17] r_InterruptRegister[0:15];

	// SIC status and capture
	reg 			r_EventSIC;			// Positive edge seen on i_SIC
	reg			r_LastEventSIC;	// Track changes in EventSIC
	reg [0:3]	r_SavedSicLevel;
	reg [0:15]	r_SavedSicMask;
	
	// Set/Reset Int Vector status and capture
	reg			r_EventSetInt;
	reg			r_LastEventSetInt;
	reg			r_SavedSetReset;
	reg [0:7]	r_SavedSetVector;
	
	// Interrupt Acknowledge status and capture
	reg			r_EventIntAck;
	reg			r_LastEventIntAck;
	
	// Write Answer status and capture
	reg			r_EventWriteAnswer;
	reg			r_LastEventWriteAnswer;
	reg [0:3]	r_SavedAnswerLevel;
	reg [0:1]	r_SavedAnswer;
	
	// Mux the interrupt register output
	assign o_InterruptReg = r_InterruptRegister[i_ReadLevel];
	
	// Function to determine the highest level bit set
	function automatic [0:3] HighBit;
      input [0:15] intCell; 
      casez (intCell)
         16'b1???????????????:  HighBit = 4'b0000;
         16'b01??????????????:  HighBit = 4'b0001;
         16'b001?????????????:  HighBit = 4'b0010;
         16'b0001????????????:  HighBit = 4'b0011;
         16'b00001???????????:  HighBit = 4'b0100;
         16'b000001??????????:  HighBit = 4'b0101;
         16'b0000001?????????:  HighBit = 4'b0110;
         16'b00000001????????:  HighBit = 4'b0111;
         16'b000000001???????:  HighBit = 4'b1000;
         16'b0000000001??????:  HighBit = 4'b1001;
         16'b00000000001?????:  HighBit = 4'b1010;
         16'b000000000001????:  HighBit = 4'b1011;
         16'b0000000000001???:  HighBit = 4'b1100;
         16'b00000000000001??:  HighBit = 4'b1101;
         16'b000000000000001?:  HighBit = 4'b1110;
         16'b0000000000000001:  HighBit = 4'b1111;
			default: HighBit = 4'b0000;
      endcase
   endfunction
	
	// These are flags indicating which levels are active (masked by the interrupt enable flags)
	wire [0:15] w_LevelActive;
	
	assign w_LevelActive[0] = (r_InterruptRegister[0][0:15] != 0) & i_IntEnable[0];
	assign w_LevelActive[1] = (r_InterruptRegister[1][0:15] != 0) & i_IntEnable[1];
	assign w_LevelActive[2] = (r_InterruptRegister[2][0:15] != 0) & i_IntEnable[2];
	assign w_LevelActive[3] = (r_InterruptRegister[3][0:15] != 0) & i_IntEnable[3];
	assign w_LevelActive[4] = (r_InterruptRegister[4][0:15] != 0) & i_IntEnable[4];
	assign w_LevelActive[5] = (r_InterruptRegister[5][0:15] != 0) & i_IntEnable[5];
	assign w_LevelActive[6] = (r_InterruptRegister[6][0:15] != 0) & i_IntEnable[6];
	assign w_LevelActive[7] = (r_InterruptRegister[7][0:15] != 0) & i_IntEnable[7];
	assign w_LevelActive[8] = (r_InterruptRegister[8][0:15] != 0) & i_IntEnable[8];
	assign w_LevelActive[9] = (r_InterruptRegister[9][0:15] != 0) & i_IntEnable[9];
	assign w_LevelActive[10] = (r_InterruptRegister[10][0:15] != 0) & i_IntEnable[10];
	assign w_LevelActive[11] = (r_InterruptRegister[11][0:15] != 0) & i_IntEnable[11];
	assign w_LevelActive[12] = (r_InterruptRegister[12][0:15] != 0) & i_IntEnable[12];
	assign w_LevelActive[13] = (r_InterruptRegister[13][0:15] != 0) & i_IntEnable[13];
	assign w_LevelActive[14] = (r_InterruptRegister[14][0:15] != 0) & i_IntEnable[14];
	assign w_LevelActive[15] = (r_InterruptRegister[15][0:15] != 0) & i_IntEnable[15];
	
	// These indicate the highest active sublevel for each level
	wire [0:4] w_SubLevelActive[16];
	
	assign w_SubLevelActive[0] = HighBit(r_InterruptRegister[0][0:15]);
	assign w_SubLevelActive[1] = HighBit(r_InterruptRegister[1][0:15]);
	assign w_SubLevelActive[2] = HighBit(r_InterruptRegister[2][0:15]);
	assign w_SubLevelActive[3] = HighBit(r_InterruptRegister[3][0:15]);
	assign w_SubLevelActive[4] = HighBit(r_InterruptRegister[4][0:15]);
	assign w_SubLevelActive[5] = HighBit(r_InterruptRegister[5][0:15]);
	assign w_SubLevelActive[6] = HighBit(r_InterruptRegister[6][0:15]);
	assign w_SubLevelActive[7] = HighBit(r_InterruptRegister[7][0:15]);
	assign w_SubLevelActive[8] = HighBit(r_InterruptRegister[8][0:15]);
	assign w_SubLevelActive[9] = HighBit(r_InterruptRegister[9][0:15]);
	assign w_SubLevelActive[10] = HighBit(r_InterruptRegister[10][0:15]);
	assign w_SubLevelActive[11] = HighBit(r_InterruptRegister[11][0:15]);
	assign w_SubLevelActive[12] = HighBit(r_InterruptRegister[12][0:15]);
	assign w_SubLevelActive[13] = HighBit(r_InterruptRegister[13][0:15]);
	assign w_SubLevelActive[14] = HighBit(r_InterruptRegister[14][0:15]);
	assign w_SubLevelActive[15] = HighBit(r_InterruptRegister[15][0:15]);
	
	
	// Event Detection
	
	always @ (negedge i_SIC)
	begin
		r_SavedSicLevel <= i_SicLevel;
		r_SavedSicMask <= i_SicMask;
		r_EventSIC <= !r_EventSIC;
	end
	
	always @ (posedge i_SetInt)
	begin
		r_EventSetInt <= !r_EventSetInt;
		r_SavedSetReset <= i_SetReset;
		r_SavedSetVector <= i_SetVector;
	end
	
	always @ (posedge i_IntAck)
	begin
		if (i_IntLock)	r_EventIntAck <= !r_EventIntAck;
	end

	always @ (negedge i_WriteAnswer)
	begin
		r_SavedAnswerLevel <= i_AnswerLevel;
		r_SavedAnswer <= i_Answer;
		r_EventWriteAnswer <= !r_EventWriteAnswer;
	end

	// Reset and event handling
	always @ (posedge i_Clock, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			r_InterruptRegister[0] <= 18'b0;
			r_InterruptRegister[1] <= 18'b0;
			r_InterruptRegister[2] <= 18'b0;
			r_InterruptRegister[3] <= 18'b0;
			r_InterruptRegister[4] <= 18'b0;
			r_InterruptRegister[5] <= 18'b0;
			r_InterruptRegister[6] <= 18'b0;
			r_InterruptRegister[7] <= 18'b0;
			r_InterruptRegister[8] <= 18'b0;
			r_InterruptRegister[9] <= 18'b0;
			r_InterruptRegister[10] <= 18'b0;
			r_InterruptRegister[11] <= 18'b0;
			r_InterruptRegister[12] <= 18'b0;
			r_InterruptRegister[13] <= 18'b0;
			r_InterruptRegister[14] <= 18'b0;
			r_InterruptRegister[15] <= 18'b0;
			r_LastEventSIC <= r_EventSIC;
			r_LastEventSetInt <= r_EventSetInt;
			r_LastEventIntAck <= r_EventIntAck;
			r_LastEventWriteAnswer <= r_EventWriteAnswer;
			o_IntPending <= LOW;
		end
		else
		begin
			// Check if we need to do a SIC instruction.
			// Note that a transition between r_EventSIC and r_LastEventSIC indicates the event
			// has occurred, not just a high value.
			if (r_EventSIC != r_LastEventSIC)
			begin
				r_InterruptRegister[r_SavedSicLevel] <= r_InterruptRegister[r_SavedSicLevel] | (r_SavedSicMask << 2) | 18'o000003;
				r_LastEventSIC <= r_EventSIC;
			end
			
			// Check if we need to set or reset an interrupt vector
			if (r_EventSetInt != r_LastEventSetInt)
			begin
				r_InterruptRegister[r_SavedSetVector[4:7]] <= (r_SavedSetReset) ?
					r_InterruptRegister[r_SavedSetVector[4:7]] | (18'o400000 >> r_SavedSetVector[0:3]) | 18'o000003 :
					r_InterruptRegister[r_SavedSetVector[4:7]] & !(18'o400000 >> r_SavedSetVector[0:3]);
				r_LastEventSetInt <= r_EventSetInt;
			end
			
			// Check if answer bits need to be updated
			if (r_EventWriteAnswer != r_LastEventWriteAnswer)
			begin
				r_InterruptRegister[r_SavedAnswerLevel] <= (r_InterruptRegister[r_SavedAnswerLevel] & 18'o777774)
																		| r_SavedAnswer;
				r_LastEventWriteAnswer <= r_EventWriteAnswer;
			end
			
			// See if we need to check for a pending interrupt
			if (!o_IntPending || !i_IntLock)
			begin
				// Check if any levels are enabled and pending
				if (w_LevelActive != 16'b0)
				begin
					o_IntVector <= {w_SubLevelActive[HighBit(w_LevelActive)], HighBit(w_LevelActive)};
					o_IntPending <= HIGH;
				end
			end
			else if (r_EventIntAck != r_LastEventIntAck)	// Note that i_IntLock must be active to ack an interrupt
			begin
				r_InterruptRegister[o_IntVector[4:7]] <= 
					r_InterruptRegister[o_IntVector[4:7]] & !(18'o400000 >> o_IntVector[0:3]);
				o_IntPending <= LOW;
				r_LastEventIntAck <= r_EventIntAck;
			end
			
		end
	end

endmodule