// This module receives input from a Channel Interface Base and,
// when the selected address is detected, triggers a timer load.
module TimerController(
	input wire				i_Clock,				// System clock
	input wire				i_Reset,				// System reset
	input wire				i_AddressValid,	// Signal indicating that the address lines are now valid
	input wire	[15:0]	i_Address,			// Address to check if this controller is selected
	input wire				i_Write,				// Signal indicating a write cycle
	
	output wire				o_Load				// Load signal to timer
);

	parameter ADDRESS	= -1;
	
	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;
	
	reg [1:0] r_TimerLoadCount;		// Counter used to control the output strobe

	assign o_Load = (r_TimerLoadCount != 0) ? HIGH : LOW;


	always @ (posedge i_Clock, posedge i_Reset)
	begin
		if (i_Reset) 																	r_TimerLoadCount <= 2'b0;
		else if (r_TimerLoadCount != 0) 											r_TimerLoadCount <= r_TimerLoadCount - 2'b01;
		else if (i_AddressValid && i_Write && (i_Address == ADDRESS))	r_TimerLoadCount <= 2'b10;
	end

endmodule