module IntervalTimer(
	input wire 				i_Clk,			// System clock
	input wire 				i_Reset,			// System reset
	input wire 				i_Enable,		// Enable timer countdown
	input wire 				i_Load,			// Load timer from i_DataIn
	input wire 	[0:17] 	i_DataIn,		// Value to load into timer on i_DataIn
	input wire 				i_RunoutAck,	// Timer runout acknowledge
	output reg 				o_Runout,		// Signal timer has run out
	output reg 	[0:17] 	o_Timer			// Current timer value
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	reg r_TimerClkHigh;
	
	reg [0:15] r_ClockGateCounter;

	wire w_GatedClockEvent;

	assign w_GatedClockEvent = (r_ClockGateCounter == 16'b0) ? HIGH : LOW;
	
	always @ (posedge i_Clk, posedge i_Reset)
	begin
		if (i_Reset)
			r_ClockGateCounter <= 16'b1;
		else if (r_ClockGateCounter == 50000)
			r_ClockGateCounter <= 16'b0;
		else
			r_ClockGateCounter <= r_ClockGateCounter + 16'b1;

		if (i_Reset)
		begin
			o_Runout <= LOW;
			o_Timer <= 18'b0;
			r_TimerClkHigh <= LOW;
		end
		else if (i_Load)
		begin
			r_ClockGateCounter <= 16'b1;
			o_Runout <= LOW;
			o_Timer <= i_DataIn;
		end
		else if (i_RunoutAck)
		begin
			o_Runout <= LOW;
		end
		else if (w_GatedClockEvent && !r_TimerClkHigh)
		begin
			r_TimerClkHigh <= HIGH;
			if (i_Enable && o_Timer)
			begin
				if (o_Timer == 18'b1)
					o_Runout <= HIGH;
				o_Timer <= o_Timer - 18'b1;
			end
		end
		else if (!w_GatedClockEvent && r_TimerClkHigh)
			r_TimerClkHigh <= LOW;
	end

endmodule