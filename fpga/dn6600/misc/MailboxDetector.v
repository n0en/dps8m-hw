module MailboxDetector(
	input wire 				i_Clock,					// System clock
	input wire				i_Reset,					// System reset
	input wire	[0:15]	i_AddressIn,			// Input absolute address
	
	output wire				o_RAM_Enable,			// Signal to enable RAM memory module for this cycle
	output wire				o_Mailbox_Enable
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;
	
	// Selector constants
	localparam SELECT_NONE		= 2'b00;
	localparam SELECT_RAM 		= 2'b01;
	localparam SELECT_MAILBOX 	= 2'b10;
	
	reg [0:1] r_Selector;
	
	assign o_RAM_Enable 			= r_Selector[1];
	assign o_Mailbox_Enable 	= r_Selector[0];

	always @(posedge i_Clock, posedge i_Reset)
	begin
		if (i_Reset)
			r_Selector <= SELECT_NONE;
		else
			case (i_AddressIn)
			
				16'o000450:	r_Selector <= SELECT_MAILBOX;			// Interval timer mailbox

				16'o000451:	r_Selector <= SELECT_MAILBOX;			// Elapsed timer mailbox

				default:		r_Selector <= SELECT_RAM;				// If no special handling, then it's RAM

			endcase
	end
	
endmodule