module MemoryInterface(
	input wire				i_Clock,				// System Clock
	input wire				i_Reset,				// System Reset
	input wire				i_Enable,			// When high, initiates a memory operation
	input wire	[0:15]	i_Address,			// Address for memory operation
	input wire	[0:17]	i_Data,				// Data for memory write operation
	input wire				i_Write,				// When high, indicates a write operation, otherwise read
	
	output reg	[0:15]	o_Address,
	output reg	[0:17]	o_Data,
	output reg				o_Write,
	output reg				o_Ready				// Signal indicating that the memory operation is complete
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;
	
	// Interface states
	localparam STATE_IDLE		= 2'b00;
	localparam STATE_LATCHED	= 2'b01;
	localparam STATE_WAIT		= 2'b10;
	localparam STATE_READY		= 2'b11;
	
	// Current state register
	reg [0:1] r_CurrentState;

	always @ (posedge(i_Reset),posedge(i_Clock))
	begin
		if (i_Reset)
		begin
			r_CurrentState <= STATE_IDLE;
			o_Address <= 16'b0;
			o_Data <= 18'b0;
			o_Write <= LOW;
			o_Ready <= LOW;
		end
		else
		begin
			case (r_CurrentState)
				STATE_IDLE:
				begin
					o_Ready <= LOW;
					o_Write <= LOW;
					if (i_Enable)
					begin
						o_Address <= i_Address;
						o_Data <= i_Data;
						r_CurrentState <= STATE_LATCHED;
					end
				end
				STATE_LATCHED:
				begin
					o_Write <= i_Write;
					r_CurrentState <= i_Write ? STATE_WAIT : STATE_READY;
				end
				STATE_WAIT:
				begin
					r_CurrentState <= STATE_READY;
				end
				STATE_READY:
				begin
					o_Ready <= HIGH;
					r_CurrentState <= i_Enable ? STATE_READY : STATE_IDLE;
				end
				default:
					r_CurrentState <= STATE_IDLE;
			endcase
		end
	end

endmodule
