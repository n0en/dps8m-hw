module InterruptRegister(
	input wire 	[0:1]		i_RegisterOp,		// Requested register operation
	input wire 				i_ExecuteOp,		// Flag indicating the operation should be performed
	input wire 	[0:3]		i_IntLevel,			// Interrupt level to set/reset
	input wire 	[0:1]		i_AnsweredBits,	// Answered bits to write on a write reg operation
	input wire				i_Reset,				// System Reset
	input wire				i_SIC,				// Set interrupt cells by or'ing in the input levels
	input wire	[0:15]	i_IntLevelData,	// Input levels for i_SIC operation
	output wire 			o_IntPending,		// Flag indicating if an interrupt is pending in the register
	output wire [0:3]		o_HighestInt,		// The highest priority bit set when o_IntPending is high
	output wire	[0:17] 	o_IntLevelData,	// The contents of the interrupt level register
	output reg				o_TP					// Test point
);
	reg [0:15] r_LevelRegister;
	reg [0:1] r_AnsweredBits;
	
	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;	
	
	// Register operations
	localparam OP_SET_INT	= 2'b00;
	localparam OP_RESET_INT	= 2'b01;
	localparam OP_READ_REG	= 2'b10;
	localparam OP_WRITE_REG	= 2'b11;
	
	// Function to determine the highest level bit set
	function automatic [0:3] HighBit;
      input [0:15] intCell; 
      casex (intCell)
         16'b1???????????????:  HighBit = 4'b0000;
         16'b01??????????????:  HighBit = 4'b0001;
         16'b001?????????????:  HighBit = 4'b0010;
         16'b0001????????????:  HighBit = 4'b0010;
         16'b00001???????????:  HighBit = 4'b0011;
         16'b000001??????????:  HighBit = 4'b0100;
         16'b0000001?????????:  HighBit = 4'b0101;
         16'b00000001????????:  HighBit = 4'b0110;
         16'b000000001???????:  HighBit = 4'b0111;
         16'b0000000001??????:  HighBit = 4'b1000;
         16'b00000000001?????:  HighBit = 4'b1001;
         16'b000000000001????:  HighBit = 4'b1010;
         16'b0000000000001???:  HighBit = 4'b1011;
         16'b00000000000001??:  HighBit = 4'b1100;
         16'b000000000000001?:  HighBit = 4'b1101;
         16'b0000000000000001:  HighBit = 4'b1111;
			default: HighBit = 4'b0000;
      endcase
   endfunction
	
	// Function to decode the incoming interrupt level bit mask
	function automatic [0:15] BitMask;
		input [0:3] level;
		case (level)
			4'b0000: BitMask = 16'b1000000000000000;
			4'b0001: BitMask = 16'b0100000000000000;
			4'b0010: BitMask = 16'b0010000000000000;
			4'b0011: BitMask = 16'b0001000000000000;
			4'b0100: BitMask = 16'b0000100000000000;
			4'b0101: BitMask = 16'b0000010000000000;
			4'b0110: BitMask = 16'b0000001000000000;
			4'b0111: BitMask = 16'b0000000100000000;
			4'b1000: BitMask = 16'b0000000010000000;
			4'b1001: BitMask = 16'b0000000001000000;
			4'b1010: BitMask = 16'b0000000000100000;
			4'b1011: BitMask = 16'b0000000000010000;
			4'b1100: BitMask = 16'b0000000000001000;
			4'b1101: BitMask = 16'b0000000000000100;
			4'b1110: BitMask = 16'b0000000000000010;
			4'b1111: BitMask = 16'b0000000000000001;
		endcase
	endfunction
		

	
	assign o_IntPending = (r_LevelRegister != 0) ? HIGH : LOW;
	assign o_HighestInt = HighBit(r_LevelRegister);
	assign o_IntLevelData = (i_ExecuteOp == HIGH) ? {r_LevelRegister,r_AnsweredBits} : 18'b0;
	
	always @ (posedge i_ExecuteOp, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			r_LevelRegister <= 16'b0;
			r_AnsweredBits <= 2'b0;
			o_TP <= 1'b0;
		end
		else case (i_RegisterOp)
			OP_SET_INT: 	
			begin
				r_LevelRegister <= r_LevelRegister | BitMask(i_IntLevel);
				r_AnsweredBits <= 2'b11;
			end
			OP_RESET_INT:	r_LevelRegister <= r_LevelRegister & ~BitMask(i_IntLevel);
			OP_READ_REG:	
			begin
				//if (r_AnsweredBits == 2'b11) o_TP <= 1'b1;
				r_LevelRegister <= r_LevelRegister;
			end
			OP_WRITE_REG:
			begin
				if (i_SIC)
				begin
					r_LevelRegister <= r_LevelRegister | i_IntLevelData;
					r_AnsweredBits <= 2'b11;
					o_TP <= 1'b1;
				end
				else
				begin
					r_AnsweredBits <= i_AnsweredBits;
				end
			end
		endcase
	end

endmodule