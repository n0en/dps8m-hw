module InterruptLevelCombiner(
	input wire	[0:17]	i_Level0Data,
	input wire	[0:17]	i_Level1Data,
	input wire	[0:17]	i_Level2Data,
	input wire	[0:17]	i_Level3Data,
	input wire	[0:17]	i_Level4Data,
	input wire	[0:17]	i_Level5Data,
	input wire	[0:17]	i_Level6Data,
	input wire	[0:17]	i_Level7Data,
	input wire	[0:17]	i_Level8Data,
	input wire	[0:17]	i_Level9Data,
	input wire	[0:17]	i_Level10Data,
	input wire	[0:17]	i_Level11Data,
	input wire	[0:17]	i_Level12Data,
	input wire	[0:17]	i_Level13Data,
	input wire	[0:17]	i_Level14Data,
	input wire	[0:17]	i_Level15Data,
	
	output wire	[0:17]	o_IntLevel
);

assign o_IntLevel = i_Level0Data  | i_Level1Data  | i_Level2Data  | i_Level3Data
						| i_Level4Data  | i_Level5Data  | i_Level6Data  | i_Level7Data
						| i_Level8Data  | i_Level9Data  | i_Level10Data | i_Level11Data
						| i_Level12Data | i_Level13Data | i_Level14Data | i_Level15Data;

endmodule