module InterruptLatch_testbench;

	reg intReq;
	reg intAck;
	wire intOut;
	
	parameter stimdelay = 10;
	
	InterruptLatch DUT(intReq,intAck,intOut);
	
	initial
	begin
		intReq = 0;
		intAck = 0;
		
		#(stimdelay) intReq = 1;
		#(stimdelay) intAck = 1;
		#(stimdelay) intAck = 0;
		#(stimdelay) intReq = 0;
	end

endmodule