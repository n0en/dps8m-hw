module InterruptController(
	input wire 				i_Clk,						// System clock
	input wire 				i_Reset,						// System reset
	input wire 				i_IntAck,					// CPU acknowledges interrupt
	input wire 				i_IntPending,				// Interrupt is pending from prioritizer
	input wire 	[0:7]		i_IntVector,				// Interrupt vector from prioritizer
	input wire				i_IntSvc,					// Interrupt service is requested from IOM
	input wire	[0:1]		i_IntSvcReq,				// Interrupt service request from IOM
	input wire	[0:7]		i_IntSvcVec,				// Interrupt service vector from IOM
	input wire				i_SIC,						// Signal indicating SIC write cycle
	
	output reg 				o_CpuIntReq,				// Interrupt request to board output (to CPU)
	output reg 	[0:7]		o_IntVector,				// Interrupt transfer address vector to board output
	output reg				o_IntSvcAck,				// Interrupt service request acknowledge to IOM
	output reg				o_LevelDecoderEnable,	// Signal to activate interrupt register selection
	output reg	[0:3]		o_LevelDecoderLevel,		// Interrupt register level selection
	output reg	[0:1]		o_RegisterOp,				// Interrupt register operation
	output reg	[0:3]		o_IntSubLevel,				// Interrupt register sublevel to set/reset
	output reg				o_TP							// Test point

);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// Register operations
	localparam OP_SET_INT	= 2'b00;
	localparam OP_RESET_INT	= 2'b01;
	localparam OP_READ_REG	= 2'b10;
	localparam OP_WRITE_REG	= 2'b11;
	
	// FSM States
	localparam STATE_IDLE							= 3'b000;
	localparam STATE_SENDING_CPU_INTERRUPT		= 3'b001;
	localparam STATE_CLEARING_CPU_INTERRUPT	= 3'b010;
	localparam STATE_SERVICING_IOM				= 3'b011;
	localparam STATE_EXECUTING_REGISTER_OP		= 3'b100;
	localparam STATE_SIC_WRITE						= 3'b101;
	
	reg [0:2] r_CurrentState;
	
	// Events
	reg r_EventIntPending;
	reg r_EventIntAck;
	reg r_EventIOMSvcReq;
	
	// Flags
	reg r_ServicingIOM;
	reg r_LastIntPending;
	reg r_LastIntAck;
	reg r_LastIntSvc;


	always @ (posedge i_Clk, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			o_CpuIntReq <= LOW;
			o_IntVector <= 8'b0;
			o_IntSvcAck <= LOW;
			o_LevelDecoderEnable <= LOW;
			o_LevelDecoderLevel <= 4'b0;
			r_CurrentState <= STATE_IDLE;
			r_EventIntPending <= LOW;
			r_EventIntAck <= LOW;
			r_EventIOMSvcReq <= LOW;
			r_ServicingIOM <= LOW;
			r_LastIntPending <= LOW;
			o_TP <= LOW;
		end
		else
		begin
			
			// Start with event detection
			r_EventIntPending <= (!r_LastIntPending && i_IntPending) ? HIGH : r_EventIntPending;
			r_LastIntPending <= i_IntPending;
			
			r_EventIntAck <= (!r_LastIntAck && i_IntAck) ? HIGH : r_EventIntAck;
			r_LastIntAck <= i_IntAck;
			
			r_EventIOMSvcReq <= (!r_LastIntSvc && i_IntSvc) ? HIGH : r_EventIOMSvcReq;
			r_LastIntSvc <= i_IntSvc;
			
			// Now run the state machine
			case (r_CurrentState)

				STATE_IDLE:
				begin
					o_LevelDecoderEnable <= LOW;
					r_ServicingIOM <= LOW;
					//o_TP <= LOW;
					if (r_EventIntPending) 		r_CurrentState <= STATE_SENDING_CPU_INTERRUPT;
					else if (r_EventIntAck) 	r_CurrentState <= STATE_CLEARING_CPU_INTERRUPT;
					else if (r_EventIOMSvcReq) r_CurrentState <= STATE_SERVICING_IOM;
					else 								r_CurrentState <= STATE_IDLE;
				end

				STATE_SENDING_CPU_INTERRUPT:
				begin
					r_EventIntPending <= LOW;
					o_IntVector <= i_IntVector;
					o_CpuIntReq <= HIGH;
					r_CurrentState <= STATE_IDLE;
				end

				STATE_CLEARING_CPU_INTERRUPT:
				begin
					r_EventIntAck <= LOW;
					o_IntVector <= 8'b0;
					o_CpuIntReq <= LOW;
					o_LevelDecoderLevel <= o_IntVector[4:7];
					o_IntSubLevel <= o_IntVector[0:3];
					o_RegisterOp <= OP_RESET_INT;
					r_ServicingIOM <= LOW;
					r_CurrentState <= STATE_EXECUTING_REGISTER_OP;
				end

				STATE_SERVICING_IOM:
				begin
					if ((i_IntSvcReq == OP_WRITE_REG) && (i_IntSvcVec[4:7] != 4'b0001) && i_SIC) o_TP <= HIGH;
//					if ((i_IntSvcReq == OP_READ_REG) && (i_IntSvcVec[4:7] == 4'b0011)) o_TP <= HIGH;
					r_EventIOMSvcReq <= LOW;
					o_LevelDecoderLevel <= i_IntSvcVec[4:7];
					o_IntSubLevel <= i_IntSvcVec[0:3];
					o_RegisterOp <= i_IntSvcReq;
					r_ServicingIOM <= HIGH;
					r_CurrentState <= STATE_EXECUTING_REGISTER_OP;
				end

				STATE_EXECUTING_REGISTER_OP:
				begin
					o_LevelDecoderEnable <= HIGH;
					o_IntSvcAck <= r_ServicingIOM;
					r_CurrentState <= STATE_IDLE;
				end
				
				default:	r_CurrentState <= STATE_IDLE;
				
			endcase
		end
	end
	
endmodule