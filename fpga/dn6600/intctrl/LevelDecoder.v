module LevelDecoder(
	input wire				i_Enable,		// Enable decoder output
	input wire	[0:3]		i_Level,			// Input level to decode
	output wire	[0:15]	o_LevelEnable	// Output to enable specific level register
);

	// Function to decode the incoming interrupt level
	function automatic [0:15] BitMask;
		input [0:3] level;
		case (level)
			4'b0000: BitMask = 16'b1000000000000000;
			4'b0001: BitMask = 16'b0100000000000000;
			4'b0010: BitMask = 16'b0010000000000000;
			4'b0011: BitMask = 16'b0001000000000000;
			4'b0100: BitMask = 16'b0000100000000000;
			4'b0101: BitMask = 16'b0000010000000000;
			4'b0110: BitMask = 16'b0000001000000000;
			4'b0111: BitMask = 16'b0000000100000000;
			4'b1000: BitMask = 16'b0000000010000000;
			4'b1001: BitMask = 16'b0000000001000000;
			4'b1010: BitMask = 16'b0000000000100000;
			4'b1011: BitMask = 16'b0000000000010000;
			4'b1100: BitMask = 16'b0000000000001000;
			4'b1101: BitMask = 16'b0000000000000100;
			4'b1110: BitMask = 16'b0000000000000010;
			4'b1111: BitMask = 16'b0000000000000001;
		endcase
	endfunction

	assign o_LevelEnable = (i_Enable) ? BitMask(i_Level) : 16'b0;

endmodule