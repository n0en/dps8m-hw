// This module picks out the highest level, unmasked interrupt level and presents
// the highest active sub-level for that level.
module InterruptPrioritizer(
	input wire	[0:15]	i_IntLevels,		// Active interrupt level flags
	input wire 	[0:63] 	i_IntSubLevels,	// Highest active interrupt sub-levels for each level
	input wire 	[0:15] 	i_IntEnable,		// Interrupt level enables from CPU
	output wire 			o_IntPending,		// Interrupt is pending
	output wire [0:7]		o_IntVector			// Interrupt vector
);

	// Function to determine the highest level bit set
	//  Result[3:0] indicates the highest level when Result[4] == 1
	function automatic [0:4] HighBit;
      input [0:15] activeLevels; 
      casex (activeLevels)
         16'b1???????????????:  HighBit = 5'b10000;
         16'b01??????????????:  HighBit = 5'b10001;
         16'b001?????????????:  HighBit = 5'b10010;
         16'b0001????????????:  HighBit = 5'b10011;
         16'b00001???????????:  HighBit = 5'b10100;
         16'b000001??????????:  HighBit = 5'b10101;
         16'b0000001?????????:  HighBit = 5'b10110;
         16'b00000001????????:  HighBit = 5'b10111;
         16'b000000001???????:  HighBit = 5'b11000;
         16'b0000000001??????:  HighBit = 5'b11001;
         16'b00000000001?????:  HighBit = 5'b11010;
         16'b000000000001????:  HighBit = 5'b11011;
         16'b0000000000001???:  HighBit = 5'b11100;
         16'b00000000000001??:  HighBit = 5'b11101;
         16'b000000000000001?:  HighBit = 5'b11110;
         16'b0000000000000001:  HighBit = 5'b11111;
			default: HighBit = 5'b00000;
      endcase
   endfunction
	
	// These are the highest sublevel bit at each level
	wire [0:3] w_Sublevel[16];
	
	assign w_Sublevel[0] = i_IntSubLevels[0:3];
	assign w_Sublevel[1] = i_IntSubLevels[4:7];
	assign w_Sublevel[2] = i_IntSubLevels[8:11];
	assign w_Sublevel[3] = i_IntSubLevels[12:15];
	assign w_Sublevel[4] = i_IntSubLevels[16:19];
	assign w_Sublevel[5] = i_IntSubLevels[20:23];
	assign w_Sublevel[6] = i_IntSubLevels[24:27];
	assign w_Sublevel[7] = i_IntSubLevels[28:31];
	assign w_Sublevel[8] = i_IntSubLevels[32:35];
	assign w_Sublevel[9] = i_IntSubLevels[36:39];
	assign w_Sublevel[10] = i_IntSubLevels[40:43];
	assign w_Sublevel[11] = i_IntSubLevels[44:47];
	assign w_Sublevel[12] = i_IntSubLevels[48:51];
	assign w_Sublevel[13] = i_IntSubLevels[52:55];
	assign w_Sublevel[14] = i_IntSubLevels[56:59];
	assign w_Sublevel[15] = i_IntSubLevels[60:63];
	
	// These are flags indicating which levels are active (masked by the interrupt enable flags)
	wire [0:15] w_LevelActive;
	
	assign w_LevelActive[0] = i_IntLevels[0] & i_IntEnable[0];
	assign w_LevelActive[1] = i_IntLevels[1] & i_IntEnable[1];
	assign w_LevelActive[2] = i_IntLevels[2] & i_IntEnable[2];
	assign w_LevelActive[3] = i_IntLevels[3] & i_IntEnable[3];
	assign w_LevelActive[4] = i_IntLevels[4] & i_IntEnable[4];
	assign w_LevelActive[5] = i_IntLevels[5] & i_IntEnable[5];
	assign w_LevelActive[6] = i_IntLevels[6] & i_IntEnable[6];
	assign w_LevelActive[7] = i_IntLevels[7] & i_IntEnable[7];
	assign w_LevelActive[8] = i_IntLevels[8] & i_IntEnable[8];
	assign w_LevelActive[9] = i_IntLevels[9] & i_IntEnable[9];
	assign w_LevelActive[10] = i_IntLevels[10] & i_IntEnable[10];
	assign w_LevelActive[11] = i_IntLevels[11] & i_IntEnable[11];
	assign w_LevelActive[12] = i_IntLevels[12] & i_IntEnable[12];
	assign w_LevelActive[13] = i_IntLevels[13] & i_IntEnable[13];
	assign w_LevelActive[14] = i_IntLevels[14] & i_IntEnable[14];
	assign w_LevelActive[15] = i_IntLevels[15] & i_IntEnable[15];
	
	
	// This is the higest level indicator
	wire [0:4] w_HighestLevel;
	
	assign w_HighestLevel = HighBit(w_LevelActive);

	// Map the outputs
	assign o_IntPending = w_HighestLevel[0];
	assign o_IntVector[0:3] = w_Sublevel[w_HighestLevel];
	assign o_IntVector[4:7] = w_HighestLevel[1:4];


endmodule