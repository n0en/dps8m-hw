module SpiControlUnit(
	input wire				i_Clock,
	input wire				i_Reset,
	input wire				i_SPI_Enable,
	input wire	[0:35]	i_SPI_TxData,
	input wire				i_SPI_TxReady,
	input wire				i_SPI_RxDataValid,
	input wire	[0:7]		i_SPI_RxData,
	
	output reg				o_SPI_TxDataValid,
	output reg	[0:7]		o_SPI_TxData,
	output reg				o_SPI_RxValid,
	output reg	[0:35]	o_SPI_RxData
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// FSM States
	localparam STATE_IDLE				= 4'b0000;
	localparam STATE_SETUP_BYTE_0		= 4'b0001;
	localparam STATE_SENDING_BYTE_0	= 4'b0010;
	localparam STATE_SETUP_BYTE_1		= 4'b0011;
	localparam STATE_SENDING_BYTE_1	= 4'b0100;
	localparam STATE_SETUP_BYTE_2		= 4'b0101;
	localparam STATE_SENDING_BYTE_2	= 4'b0110;
	localparam STATE_SETUP_BYTE_3		= 4'b0111;
	localparam STATE_SENDING_BYTE_3	= 4'b1000;
	localparam STATE_WAITING_READ		= 4'b1001;

	// Assignments to map inputs properly
	// Bit 0 = Write / not Read
	// Bit 1 = Unused
	// Bit 2-3 = Port
	//			2'b00 = Ethernet
	//			2'b01 = SD Card
	//			[rest are currently invalid]
	// Bit 4-35 = 32-bit word to send on SPI
	assign w_TxByte0 = i_SPI_TxData[4:11];			// Byte 0 to send on SPI
	assign w_TxByte1 = i_SPI_TxData[12:19];		// Byte 1 to send on SPI
	assign w_TxByte2 = i_SPI_TxData[20:27];		// Byte 2 to send on SPI
	assign w_TxByte3 = i_SPI_TxData[28:35];		// Byte 3 to send on SPI
	assign w_WriteOp = i_SPI_TxData[0];				// Write / not Read SPI operation
	assign w_PortSelect = i_SPI_TxData[2:3];		// SPI Port to access

	reg [0:3] r_State;

	reg r_NextEnable;

	always @ (posedge i_Clock, posedge i_Reset)
	begin
		if (i_Reset)
		begin
			o_SPI_RxValid <= LOW;
			r_State <= STATE_IDLE;
		end
		else
		begin
			case (r_State)

				STATE_IDLE:
				begin
					o_SPI_RxValid <= LOW;
					r_State <= i_SPI_Enable ? STATE_SETUP_BYTE_0 : STATE_IDLE;
				end

				STATE_SETUP_BYTE_0:
				begin
					o_SPI_RxData <= 36'b0;
					o_SPI_TxData <= w_TxByte0;
					r_NextEnable <= HIGH;
					r_State <= STATE_SENDING_BYTE_0;
				end

				STATE_SENDING_BYTE_0:
				begin
					o_SPI_TxDataValid <= r_NextEnable;
					r_NextEnable <= LOW;
					r_State <= i_SPI_TxReady ? STATE_SETUP_BYTE_1 : STATE_SENDING_BYTE_0;
				end

				STATE_SETUP_BYTE_1:
				begin
					o_SPI_TxData <= w_TxByte1;
					r_NextEnable <= HIGH;
					r_State <= STATE_SENDING_BYTE_1;
				end

				STATE_SENDING_BYTE_1:
				begin
					o_SPI_TxDataValid <= r_NextEnable;
					r_NextEnable <= LOW;
					r_State <= i_SPI_TxReady ? STATE_SETUP_BYTE_2 : STATE_SENDING_BYTE_1;
				end

				STATE_SETUP_BYTE_2:
				begin
					o_SPI_TxData <= w_TxByte2;
					r_NextEnable <= HIGH;
					r_State <= STATE_SENDING_BYTE_2;
				end

				STATE_SENDING_BYTE_2:
				begin
					o_SPI_TxDataValid <= r_NextEnable;
					r_NextEnable <= LOW;
					r_State <= i_SPI_TxReady ? STATE_SETUP_BYTE_3 : STATE_SENDING_BYTE_2;
				end

				STATE_SETUP_BYTE_3:
				begin
					o_SPI_TxData <= w_WriteOp ? w_TxByte3 : 8'b0;;
					r_NextEnable <= HIGH;
					r_State <= STATE_SENDING_BYTE_3;
				end

				STATE_SENDING_BYTE_3:
				begin
					o_SPI_TxDataValid <= r_NextEnable;
					r_NextEnable <= LOW;
					r_State <= i_SPI_TxReady ? (w_WriteOp ? STATE_IDLE : STATE_WAITING_READ) 
								: STATE_SENDING_BYTE_3;
				end

				STATE_WAITING_READ:
				begin
					if (i_SPI_RxDataValid)
					begin
						o_SPI_RxData[0] <= LOW;
						o_SPI_RxData[1] <= HIGH;
						o_SPI_RxData[2:27] <= 26'b0;
						o_SPI_RxData[28:35] <= o_SPI_RxData;
						r_State <= STATE_IDLE;
					end
					else
					begin
						r_State <= STATE_WAITING_READ;
					end
				end

				default: r_State <= STATE_IDLE;

			endcase
		end
	end



endmodule