module PageAddressTableRegister(
	input wire				i_Clock,			// System clock
	input wire				i_Reset,			// System reset
	input wire				i_Write,			// Write / not Read of register
	input wire [0:17]		i_DataIn,		// Incoming data to capture in register
	input wire [0:15] 	i_AddressIn,	// Raw CPU address to determine if a write to this register is happening
	
	output wire [0:15]	o_DataOut		// Output register contents for use by Page Control Unit
);

	reg [0:15] r_Data;
	
	assign o_DataOut = r_Data;
	
	always @ (posedge i_Clock, posedge i_Reset)
	begin
		if (i_Reset)
			r_Data <= 16'b0;
		else if (i_Write && (i_AddressIn == 18'o000475))
			r_Data <= i_DataIn[2:17];
	end

endmodule