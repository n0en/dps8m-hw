module MmuHistoryRegisters(
	input wire				i_Reset,					// System reset signal
	input wire				i_MemoryWrite,			// When high, indicates a memory write cycle
	input wire	[0:15]	i_MemoryAddress,		// Memory address being acccessed
	input wire	[0:17]	i_MemoryWriteData,	// Data to be written
	input wire				i_RamEnable,			// When high, indicates a RAM access
	input wire				i_MailBoxEnable,		// When high, indicates a Mail Box access
	input wire	[0:3]		i_IndexSelect,			// Indicates which index should be output (Note that even index shows address and odd index shows data)
	input wire				i_MemReady,				// Signal indicating memory module is ready with requested operation
	input wire				i_MailBoxReady,		// Signal indicating IOM is ready with mail box operation
	input wire	[0:17]	i_MemoryReadData,		// Data read from memory
	input wire				i_DisplaySelect,		// When high, show memory read data or when low show memory write data
	
	output wire	[0:23]	o_DisplayOut,			// Octal to hex conversion of selected output
	output wire [0:5]		o_DPOut					// Decimal point output for display
);

reg [0:2] r_NextIndex;
reg r_RamEnable[0:7];
reg r_MailBoxEnable[0:7];
reg r_MemoryWrite[0:7];
reg r_MailBoxReady[0:7];
reg r_MemReady[0:7];
reg [0:15] r_MemoryAddress[0:7];
reg [0:17] r_MemoryWriteData[0:7];
reg [0:17] r_MemoryReadData[0:7];
wire [0:17] w_DisplayValue;
wire [0:23] w_DisplayOut;
wire w_Ready;

assign o_DisplayOut = w_DisplayOut;
assign o_DPOut[0] = r_RamEnable[r_NextIndex - i_IndexSelect[0:2]];
assign o_DPOut[1] = r_MailBoxEnable[r_NextIndex - i_IndexSelect[0:2]];
assign o_DPOut[2] = r_MemoryWrite[r_NextIndex - i_IndexSelect[0:2]];
assign o_DPOut[3] = r_MemReady[r_NextIndex - i_IndexSelect[0:2]];
assign o_DPOut[4] = r_MailBoxReady[r_NextIndex - i_IndexSelect[0:2]];
assign o_DPOut[5] = 1'b1;

assign w_DisplayOut[0:3] = {1'b0,w_DisplayValue[0:2]};
assign w_DisplayOut[4:7] = {1'b0,w_DisplayValue[3:5]};
assign w_DisplayOut[8:11] = {1'b0,w_DisplayValue[6:8]};
assign w_DisplayOut[12:15] = {1'b0,w_DisplayValue[9:11]};
assign w_DisplayOut[16:19] = {1'b0,w_DisplayValue[12:14]};
assign w_DisplayOut[20:23] = {1'b0,w_DisplayValue[15:17]};

assign w_Ready = i_MemReady | i_MailBoxReady;

assign w_DisplayValue = (i_IndexSelect[3] ?
	((i_DisplaySelect) ? r_MemoryReadData[r_NextIndex - i_IndexSelect[0:2]] : r_MemoryWriteData[r_NextIndex - i_IndexSelect[0:2]])
	:
	({2'b00,r_MemoryAddress[r_NextIndex - i_IndexSelect[0:2]]})
	);

	always @ (posedge(w_Ready), posedge(i_Reset))
	begin
		if (i_Reset)
		begin
			r_NextIndex <= 3'b000;
		end
		else
		begin
			r_RamEnable[r_NextIndex] <= i_RamEnable;
			r_MailBoxEnable[r_NextIndex] <= i_MailBoxEnable;
			r_MemoryWrite[r_NextIndex] <= i_MemoryWrite;
			r_MemoryAddress[r_NextIndex] <= i_MemoryAddress;
			r_MemoryWriteData[r_NextIndex] <= i_MemoryWriteData;
			r_MemoryReadData[r_NextIndex] <= i_MemoryReadData;
			r_MailBoxReady[r_NextIndex] <= i_MailBoxReady;
			r_MemReady[r_NextIndex] <= i_MemReady;
			r_NextIndex <= r_NextIndex + 3'b001;
		end
	end

endmodule