// This module is responsible for converting a potentially paged address into an absolute address
module MmuPageAddressResolver(
	input wire 				i_Clock,
	input wire 				i_Reset,
	input wire 				i_PageModeEnable,
	input wire 				i_WriteIn,
	input wire 	[0:17] 	i_AddressIn,
	input wire 				i_Start,
	input wire 	[0:15] 	i_PatReg,
	input wire 	[0:17] 	i_MemoryIn,
	input wire				i_AbsoluteAddress,
	input wire				i_MemoryReady,

	output reg 				o_WriteOut,
	output reg 	[0:15] 	o_AddressOut,
	output reg 				o_Ready,
	output reg 				o_Fault,
	output reg				o_MemoryEnable,
	output reg				o_TP
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// FSM States
	localparam PCU_IDLE						= 4'b0000;
	localparam PCU_CHECKING_PATREG		= 4'b0001;
	localparam PCU_COMPLETING_CYCLE		= 4'b0010;
	localparam PCU_WAITING_ENABLE_LOW	= 4'b0011;
	localparam PCU_READING_PTE				= 4'b0100;
	localparam PCU_PTE_WAITING_MEM_READY		= 4'b0101;
	localparam PCU_RESOLVING_PTE			= 4'b0110;
	localparam PCU_WAITING_FAULT_CLEAR	= 4'b0111;
	localparam PCU_WAITING_FINAL_MEM_READY				= 4'b1000;

	reg [0:3] r_CurrentState = PCU_IDLE;
	reg [0:17] r_PageTableEntry = 0;
	reg r_LastEnable = LOW;
	reg r_WriteCycle;
	
	wire w_PTE_Active;
	wire w_PTE_Security;
	wire w_PTE_ReadOnly;
	wire [0:1] w_PTE_BadAddress;
	
	assign w_PTE_Active = r_PageTableEntry[12];
	assign w_PTE_Security = r_PageTableEntry[11];
	assign w_PTE_ReadOnly = r_PageTableEntry[10];
	assign w_PTE_BadAddress = r_PageTableEntry[0:1];
	
	always @(posedge i_Clock, posedge i_Reset)
	begin
		
		if (i_Reset)
		begin
			r_CurrentState <= PCU_IDLE;
			r_WriteCycle <= LOW;
			o_WriteOut <= LOW;
			o_Ready <= LOW;
			o_Fault <= LOW;
			o_MemoryEnable <= LOW;
			o_TP <= LOW;
		end
		else
		begin
			case (r_CurrentState)
				PCU_IDLE:
				begin
					o_Ready <= LOW;
					o_WriteOut <= LOW;
					o_Fault <= LOW;
					o_MemoryEnable <= LOW;
					if (i_Start) r_CurrentState <= PCU_CHECKING_PATREG;
				end
				
				PCU_CHECKING_PATREG:
				begin
					if (!i_PageModeEnable || (i_PatReg == 0) /*|| i_AbsoluteAddress*/)
					begin
						if (i_AddressIn & 18'o600000)
						begin
							o_Fault <= HIGH;
							r_CurrentState <= PCU_WAITING_FAULT_CLEAR;
						end
						else
						begin
							o_AddressOut <= i_AddressIn[2:17];
							o_WriteOut <= i_WriteIn;
							r_CurrentState <= PCU_WAITING_FINAL_MEM_READY;
						end
					end
					else
					begin
						r_WriteCycle <= i_WriteIn;
						o_AddressOut <= i_PatReg[0:15] | (i_AddressIn >> 8);
						o_WriteOut <= LOW;
						r_CurrentState <= PCU_PTE_WAITING_MEM_READY;
					end
				end
				
				PCU_WAITING_FINAL_MEM_READY:
				begin
					o_MemoryEnable <= HIGH;
					r_CurrentState <= (i_MemoryReady) ? PCU_COMPLETING_CYCLE : PCU_WAITING_FINAL_MEM_READY;
				end
				
				PCU_PTE_WAITING_MEM_READY:
				begin
					o_MemoryEnable <= HIGH;
					r_CurrentState <= (i_MemoryReady) ? PCU_READING_PTE : PCU_PTE_WAITING_MEM_READY;
				end
				
				PCU_COMPLETING_CYCLE:
				begin
					o_Ready <= HIGH;
					r_CurrentState <= (i_Start) ? PCU_COMPLETING_CYCLE : PCU_IDLE;
				end
				
				PCU_READING_PTE:
				begin
					if (i_MemoryIn == 16'o040040) o_TP <= HIGH;
					o_MemoryEnable <= LOW;
					r_PageTableEntry <= i_MemoryIn;
					r_CurrentState <= PCU_RESOLVING_PTE;
				end
				
				PCU_RESOLVING_PTE:
				begin
					if (!w_PTE_Active)
					begin
						o_AddressOut <= i_AddressIn[2:17];
						o_WriteOut <= r_WriteCycle;
						r_CurrentState <= PCU_WAITING_FINAL_MEM_READY;
					end
					else if (w_PTE_Security)
					begin
						o_Fault <= HIGH;
						r_CurrentState <= PCU_WAITING_FAULT_CLEAR;
					end
					else if (w_PTE_ReadOnly && r_WriteCycle)
					begin
						o_Fault <= HIGH;
						r_CurrentState <= PCU_WAITING_FAULT_CLEAR;
					end
					else if (w_PTE_BadAddress)
					begin
						o_Fault <= HIGH;
						r_CurrentState <= PCU_WAITING_FAULT_CLEAR;
					end
					else
					begin
						o_AddressOut <= (r_PageTableEntry & 16'o777400) | (i_AddressIn & 16'o00377);
						o_WriteOut <= r_WriteCycle;
						r_CurrentState <= PCU_WAITING_FINAL_MEM_READY;
					end
				end
				
				PCU_WAITING_FAULT_CLEAR:
				begin
					o_Fault <= HIGH;
					o_MemoryEnable <= LOW;
					r_CurrentState <= (i_Start) ? PCU_WAITING_FAULT_CLEAR : PCU_IDLE;
				end
				
				default: r_CurrentState <= PCU_IDLE;
				
			endcase
			
			r_LastEnable <= i_Start;
		end
	end


endmodule
