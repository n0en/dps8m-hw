// This module is the port selector for the MMU and arbitrates which 
// control port will be serviced next.
module MmuPortSelector (

	// General Inputs
	input wire				i_Clock,							// System clock
	input wire				i_Reset,							// System reset

	// CPU Input Port
	input wire				i_CpuStart,						// When high, indicates the CPU is requesting an execution
	input wire 	[0:2] 	i_CpuMop,						// Memory Operation
	input wire 	[0:3] 	i_CpuRmwOp,						// Specific operation to carry out when doing read/modify/write cycle
	input wire 	[0:14] 	i_CpuAddress,					// Address input
	input wire 	[0:35] 	i_CpuData,						// Double word input for writing to memory
	input wire 	[0:17] 	i_CpuRegisterA,				// The current value in the A register (used in read/modify/write operations)
	input wire 	[0:17] 	i_CpuRegisterQ,				// The current value in the Q register (used in read/modify/write operations)
	input wire 	[0:2] 	i_CpuCharAddressMode,		// Mode select for writing character positions in memory word
	
	
	// IOM Input Port
	input wire				i_IomStart,						// When high, indicates the IOM is requesting an execution
	input wire 	[0:2] 	i_IomMop,						// Memory Operation
	input wire 	[0:3] 	i_IomRmwOp,						// Specific operation to carry out when doing read/modify/write cycle
	input wire 	[0:17] 	i_IomAddress,					// Address input
	input wire 	[0:35] 	i_IomData,						// Double word input for writing to memory
	input wire 	[0:17] 	i_IomRegisterA,				// The current value in the A register (used in read/modify/write operations)
	input wire 	[0:17] 	i_IomRegisterQ,				// The current value in the Q register (used in read/modify/write operations)
	input wire 	[0:2] 	i_IomCharAddressMode,		// Mode select for writing character positions in memory word
	input wire				i_IomAbsoluteAddress,		// Indicates that the IOM is requesting an absolute address access with no paging
	
	
	// MMU Memory Controller Input Port
	input wire				i_MmcReady,						// Indicates when a memory controller operation is complete
	input wire				i_MmcFault,						// Indicates when a memory controller operation results in a memory fault
	
	
	// CPU Output Port
	output reg				o_CpuReady,						// Indicates that the result of a CPU requested operation is ready
	output reg				o_CpuMmcFault,					// Indicates to the CPU that the result of the operation was a memory fault
	
	
	// IOM Output Port
	output reg				o_IomReady,						// Indicates that the result of an IOM requested operation is ready
	output reg				o_IomMmcFault,					// Indicates to the IOM that the result of the operation was a memory fault
	
	
	// MMU Memory Controller Output Port
	output reg				o_MmcStart,						// Indicates that the Memory Controller should execute the operation
	output wire	[0:2] 	o_MmcMop,						// Memory Operation
	output wire	[0:3] 	o_MmcRmwOp,						// Specific operation to carry out when doing read/modify/write cycle
	output wire	[0:17] 	o_MmcAddress,					// Address input
	output wire	[0:35] 	o_MmcData,						// Double word input for writing to memory
	output wire	[0:17] 	o_MmcRegisterA,				// The current value in the A register (used in read/modify/write operations)
	output wire	[0:17] 	o_MmcRegisterQ,				// The current value in the Q register (used in read/modify/write operations)
	output wire	[0:2] 	o_MmcCharAddressMode,		// Mode select for writing character positions in memory word
	output wire				o_MmcAbsoluteAddress			// Indicates that the IOM is requesting an absolute address access with no paging
	
	
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// Port selection
	localparam PORT_CPU = 1'b1;
	localparam PORT_IOM = 1'b0;
	reg r_PortSelect;
	
	// Memory sharing counter
	reg [0:1] r_IomAccessCount;
	
	// State machine
	localparam STATE_IDLE		= 3'b000;
	localparam STATE_IOM_BUSY	= 3'b001;
	localparam STATE_CPU_BUSY	= 3'b010;
	localparam STATE_IOM_WAIT	= 3'b011;
	localparam STATE_CPU_WAIT	= 3'b100;
	reg [0:2] r_State;


	// Set up asynchrounous assignments based on port select register
	assign o_MmcMop = (r_PortSelect == PORT_IOM) ? i_IomMop : i_CpuMop;
	assign o_MmcRmwOp = (r_PortSelect == PORT_IOM) ? i_IomRmwOp : i_CpuRmwOp;
	assign o_MmcAddress[3:17] = (r_PortSelect == PORT_IOM) ? i_IomAddress[3:17] : i_CpuAddress;
	assign o_MmcAddress[0:2] = (r_PortSelect == PORT_IOM) ? i_IomAddress[0:2] : 3'b0;
	assign o_MmcData = (r_PortSelect == PORT_IOM) ? i_IomData : i_CpuData;
	assign o_MmcRegisterA = (r_PortSelect == PORT_IOM) ? i_IomRegisterA : i_CpuRegisterA;
	assign o_MmcRegisterQ = (r_PortSelect == PORT_IOM) ? i_IomRegisterQ : i_CpuRegisterQ;
	assign o_MmcCharAddressMode = (r_PortSelect == PORT_IOM) ? i_IomCharAddressMode : i_CpuCharAddressMode;
	assign o_MmcAbsoluteAddress = LOW; //(r_PortSelect == PORT_IOM) ? i_IomAbsoluteAddress : 1'b0;
	
	always @ (posedge(i_Clock), posedge(i_Reset))
	begin
		if (i_Reset)
		begin
			r_State <= STATE_IDLE;
			r_IomAccessCount <= 2'b0;
			o_MmcStart <= LOW;
		end
		else
		begin
			case (r_State)
			
				STATE_IDLE:
				begin
					if (i_IomStart)
					begin
						if ((r_IomAccessCount == 2'b11) && (i_CpuStart))
						begin
							r_PortSelect <= PORT_CPU;
							r_IomAccessCount <= 2'b00;
							r_State <= STATE_CPU_BUSY;
						end
						else
						begin
							r_PortSelect <= PORT_IOM;
							r_IomAccessCount <= r_IomAccessCount + 2'b01;
							r_State <= STATE_IOM_BUSY;
						end
						o_MmcStart <= HIGH;
					end
					else if (i_CpuStart)
					begin
						r_PortSelect <= PORT_CPU;
						r_IomAccessCount <= 2'b00;
						r_State <= STATE_CPU_BUSY;
						o_MmcStart <= HIGH;
					end
					else
					begin
						o_MmcStart <= LOW;
						r_State <= STATE_IDLE;
					end
				end
				
				STATE_IOM_BUSY:
				begin
					if (i_MmcReady)
					begin
						o_IomReady <= HIGH;
						r_State <= STATE_IOM_WAIT;
					end
					else if (i_MmcFault)
					begin
						o_IomMmcFault <= HIGH;
						o_MmcStart <= LOW;
						r_State <= STATE_IOM_WAIT;
					end
					else
						r_State <= STATE_IOM_BUSY;
				end
				
				STATE_CPU_BUSY:
				begin
					if (i_MmcReady)
					begin
						o_CpuReady <= HIGH;
						r_State <= STATE_CPU_WAIT;
					end
					else if (i_MmcFault)
					begin
						o_CpuMmcFault <= HIGH;
						o_MmcStart <= LOW;
						r_State <= STATE_CPU_WAIT;
					end
					else
						r_State <= STATE_CPU_BUSY;
				end
				
				STATE_IOM_WAIT:
				begin
					if (!i_IomStart)
					begin
						o_IomReady <= LOW;
						o_IomMmcFault <= LOW;
						o_MmcStart <= LOW;
						r_State <= STATE_IDLE;
					end
					else
						r_State <= STATE_IOM_WAIT;
				end
				
				STATE_CPU_WAIT:
				begin
					if (!i_CpuStart)
					begin
						o_CpuReady <= LOW;
						o_CpuMmcFault <= LOW;
						o_MmcStart <= LOW;
						r_State <= STATE_IDLE;
					end
					else
						r_State <= STATE_CPU_WAIT;
				end
				
				default:
					r_State <= STATE_IDLE;
				
			endcase
		end
	end
	
endmodule
