module MmuMailboxDetector(
	input wire 				i_Clock,					// System clock
	input wire				i_Reset,					// System reset
	input wire	[0:15]	i_AddressIn,			// Input absolute address
	
	output wire				o_RAM_Enable,			// Signal to enable RAM memory module for this cycle
	output wire				o_Mailbox_Enable
);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;
	
	// Selector constants
	localparam SELECT_NONE		= 2'b00;
	localparam SELECT_RAM 		= 2'b01;
	localparam SELECT_MAILBOX 	= 2'b10;
	
	wire [0:1] w_Selector;
	
	assign o_RAM_Enable 			= w_Selector[1];
	assign o_Mailbox_Enable 	= w_Selector[0];
	
	assign w_Selector = (
								(i_AddressIn == 16'o000400) ||
								(i_AddressIn == 16'o000401) ||
								(i_AddressIn == 16'o000402) ||
								(i_AddressIn == 16'o000403) ||
								(i_AddressIn == 16'o000404) ||
								(i_AddressIn == 16'o000405) ||
								(i_AddressIn == 16'o000406) ||
								(i_AddressIn == 16'o000407) ||
								(i_AddressIn == 16'o000410) ||
								(i_AddressIn == 16'o000411) ||
								(i_AddressIn == 16'o000412) ||
								(i_AddressIn == 16'o000413) ||
								(i_AddressIn == 16'o000414) ||
								(i_AddressIn == 16'o000415) ||
								(i_AddressIn == 16'o000416) ||
								(i_AddressIn == 16'o000417) ||
								(i_AddressIn == 16'o000450) || 
								(i_AddressIn == 16'o000451)
		) ? SELECT_MAILBOX : SELECT_RAM;

endmodule