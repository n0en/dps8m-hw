// This module is the memory controller used to support the CPU in memory operations
module MmuMemoryController (
	// General I/O
	input wire 				i_Clock,						// System clock
	input wire 				i_Reset,						// System i_Reset signal
	output reg 	[0:3] 	o_ControllerState,		// Current MEMORY cycle state
	
	// I/O between Controller and CPU
	input wire 				i_Start,						// Initiate current i_Mop on rising edge
	input wire 	[0:2] 	i_Mop,						// Memory Operation
	input wire 	[0:3] 	i_RmwOp,						// Specific operation to carry out when doing read/modify/write cycle
	input wire 	[0:17] 	i_Address,					// Address input
	input wire 	[0:35] 	i_Data,						// Double word input for writing to memory
	input wire 	[0:17] 	i_RegisterA,				// The current value in the A register (used in read/modify/write operations)
	input wire 	[0:17] 	i_RegisterQ,				// The current value in the Q register (used in read/modify/write operations)
	input wire 	[0:2] 	i_CharAddressMode,		// Mode select for writing character positions in memory word
	output reg 	[0:35] 	o_Data,						// Double word memory read results
	output reg 				o_Fault,						// Flag indicating memory o_Fault has occurred
	output reg 				o_Ready,						// Flag indicating operation is complete
	
	// I/O between Controller and Memory Module
	input wire 				i_MemoryOpComplete,		// Memory operation complete signal
	input wire 	[0:17] 	i_MemoryReadData,			// Data read from memory on read cycle
	output reg 				o_MemChipSelect, 			// Chip select signal for selecting the RAM module
	output reg 				o_MemoryWriteEnable,		// Write / not Read signal for the memory cycle
	output reg 	[0:17]	o_MemoryAddress, 			// The memory address the controller wants to read or write
	output reg 	[0:17] 	o_MemoryWriteData, 		// Data to write on memory write cycle
	output reg 				o_CarryFlag,				// Carry flag from read/modify/write cycle
	output reg 				o_OverflowFlag,			// Overflow flag from read/modify/write cycle
	input wire 				i_MemoryModuleFault,		// Memory module indicated o_Fault
	
	// 18 bit Adder interface
	input wire	[0:17] 	i_Sum18,						// Sum from the 18 bit adder
	input wire       		i_Carry18,					// Carry from the 18 bit adder
	input wire       		i_Overflow18,				// Overflow indicator from the 18 bit adder
	output reg 	[0:17]	o_Addend1,					// First number for adding to 18 bit adder
	output reg	[0:17]	o_Addend2,					// Second number for adding to 18 bit adder
	output reg	       	o_Carry18,					// Carry for adding to 18 bit adder
	output reg				o_Add18,						// Add / Not subtract for 18 bit adder
	
	// Diagnostics
	output reg				o_TP1							// Test point 1

);

	// Some handy constants
	localparam HIGH 	= 1'b1;
	localparam LOW		= 1'b0;

	// Memory Operation Requested
	`define MOP_NOP					3'b000
	`define MOP_READ_WORD			3'b001
	`define MOP_READ_DOUBLE_WORD	3'b010
	`define MOP_WRITE_WORD			3'b011
	`define MOP_WRITE_DOUBLE_WORD	3'b100
	`define MOP_READ_MOD_WRITE		3'b101

	// Controller cycle state
	`define MC_IDLE						4'b0000
	`define MC_WRITING_DOUBLE_WORD	4'b0001
	`define MC_FINISH_FIRST_WRITE		4'b0010
	`define MC_START_SECOND_WRITE		4'b0011
	`define MC_WRITING_WORD				4'b0100
	`define MC_FINISH_WRITE				4'b0101
	`define MC_READING_DOUBLE_WORD	4'b0110
	`define MC_FINISH_FIRST_READ		4'b0111
	`define MC_STARTING_SECOND_READ	4'b1000
	`define MC_READING_WORD				4'b1001
	`define MC_FINISHING_READ			4'b1010
	`define MC_RMW_READING				4'b1011
	`define MC_RMW_MODIFYING			4'b1100
	`define MC_RMW_MOD2					4'b1101
	`define MC_RMW_WAITING				4'b1110
	`define MC_RMW_WRITING				4'b1111
		
	// Read/Modify/Write Operation Codes
	`define RMW_NOP				4'b0001
	`define RMW_ADD_A				4'b0001
	`define RMW_ADD_ONE			4'b0010
	`define RMW_AND_A				4'b0011
	`define RMW_OR_A				4'b0100
	`define RMW_EOR_A				4'b0101
	`define RMW_SUB_A				4'b0110
	`define RMW_WRITE_CHAR_A	4'b0111
	`define RMW_WRITE_CHAR_Q	4'b1000

	// Macros to simplify high/low word addressing
	`define DIN_LOW 	i_Data[18:35]
	`define DIN_HIGH	i_Data[0:17]
	`define DOUT_LOW	o_Data[18:35]
	`define DOUT_HIGH	o_Data[0:17]
	
	// Internal storage
	reg r_LastStart;

	always @ (posedge(i_Clock), posedge(i_Reset))
	begin
		if (i_Reset)
		begin
			o_ControllerState <= `MC_IDLE;
			r_LastStart <= LOW;
			o_Ready <= LOW;
			o_MemoryAddress <= 15'b0;
			o_MemoryWriteData <= 18'b0;
			o_MemoryWriteEnable <= LOW;
			o_MemChipSelect <= LOW;
			o_CarryFlag <= LOW;
			o_OverflowFlag <= LOW;
			o_Carry18 <= LOW;
			o_Add18 <= HIGH;
			o_TP1 <= LOW;
		end
		else
		begin
			case (o_ControllerState)
				`MC_IDLE:
				begin
					// Drop our o_Ready line when i_Start goes away
					if (!i_Start)
						o_Ready <= LOW;
						
					// Check if we have a i_Start signal rising edge
					if (!r_LastStart & i_Start)
					begin
						case (i_Mop)
							`MOP_NOP:
							begin
								o_MemoryAddress <= i_Address;
								o_MemoryWriteData <= 18'b0;
								o_MemoryWriteEnable <= LOW;
								o_MemChipSelect <= LOW;
								o_ControllerState <= `MC_IDLE;
								o_Fault <= LOW;
							end
							`MOP_READ_WORD:
							begin
								o_MemoryAddress <= i_Address;
								o_MemoryWriteData <= 15'b0;
								o_MemoryWriteEnable <= LOW;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_READING_WORD;
								o_Fault <= LOW;
							end
							`MOP_READ_DOUBLE_WORD:
							begin
								o_MemoryAddress <= i_Address & 15'o77776;
								o_MemoryWriteData <= 15'b0;
								o_MemoryWriteEnable <= LOW;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_READING_DOUBLE_WORD;
								o_Fault <= LOW;
							end
							`MOP_WRITE_WORD:
							begin
								o_MemoryAddress <= i_Address;
								o_MemoryWriteData <= `DIN_LOW;
								o_MemoryWriteEnable <= HIGH;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_WRITING_WORD;
								o_Fault <= LOW;
							end
							`MOP_WRITE_DOUBLE_WORD:
							begin
								o_MemoryAddress <= i_Address & 15'o77776;
								o_MemoryWriteData <= `DIN_HIGH;
								o_MemoryWriteEnable <= HIGH;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_WRITING_DOUBLE_WORD;
								o_Fault <= LOW;
							end
							`MOP_READ_MOD_WRITE:
							begin
								o_MemoryAddress <= i_Address;
								o_MemoryWriteData <= 15'b0;
								o_MemoryWriteEnable <= LOW;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_RMW_READING;
								o_Fault <= LOW;
							end
							default:
							begin
								o_MemoryAddress <= 15'b0;
								o_MemoryWriteData <= 15'b0;
								o_MemoryWriteEnable <= LOW;
								o_MemChipSelect <= HIGH;
								o_ControllerState <= `MC_IDLE;
								o_Fault <= HIGH;
							end
						endcase
					end
					r_LastStart <= i_Start;
				end
				
				`MC_WRITING_DOUBLE_WORD:
				begin
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_FINISH_FIRST_WRITE : `MC_WRITING_DOUBLE_WORD;
				end
				
				`MC_FINISH_FIRST_WRITE:
				begin
					o_MemoryWriteEnable <= LOW;
					o_MemChipSelect <= LOW;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_FINISH_FIRST_WRITE : `MC_START_SECOND_WRITE;
				end
				
				`MC_START_SECOND_WRITE:
				begin
					o_MemoryAddress <= i_Address | 15'b1;
					o_MemoryWriteData <= `DIN_LOW;
					o_MemoryWriteEnable <= HIGH;
					o_MemChipSelect <= HIGH;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_WRITING_WORD;
				end
				
				`MC_WRITING_WORD:
				begin
/*					if (o_MemoryAddress == 18'o2376)
					begin
						o_TP1 <= HIGH;
					end*/
					if (i_MemoryModuleFault) 
					begin
						o_TP1 <= HIGH;
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_FINISH_WRITE : `MC_WRITING_WORD;
				end
				
				`MC_FINISH_WRITE:
				begin
					o_MemoryWriteEnable <= LOW;
					o_MemChipSelect <= LOW;
					o_Ready <= HIGH;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_IDLE;
				end
				
				`MC_READING_DOUBLE_WORD:
				begin
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_FINISH_FIRST_READ : `MC_READING_DOUBLE_WORD;
				end
				
				`MC_FINISH_FIRST_READ:
				begin
					`DOUT_HIGH <= i_MemoryReadData;
					o_MemChipSelect <= LOW;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_FINISH_FIRST_READ : `MC_STARTING_SECOND_READ;
				end
				
				`MC_STARTING_SECOND_READ:
				begin
					o_MemoryAddress <= i_Address | 15'b1;
					o_MemChipSelect <= HIGH;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_READING_WORD;
				end
				
				`MC_READING_WORD:
				begin
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_FINISHING_READ : `MC_READING_WORD;
				end
				
				`MC_FINISHING_READ:
				begin
					`DOUT_LOW <= i_MemoryReadData;
					o_MemChipSelect <= LOW;
					o_Ready <= HIGH;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= `MC_IDLE;
				end
				
				`MC_RMW_READING:
				begin
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_MODIFYING : `MC_RMW_READING;
				end
				
				`MC_RMW_MODIFYING:
				begin
					o_MemChipSelect <= LOW;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else
					begin
						case (i_RmwOp)
							`RMW_ADD_A:
							begin
								o_Addend1 <= i_RegisterA;
								o_Addend2 <= i_MemoryReadData;
								o_Add18 <= HIGH;
								o_ControllerState <= `MC_RMW_MOD2;
							end
							`RMW_ADD_ONE:
							begin
								o_Addend1 <= 18'b1;
								o_Addend2 <= i_MemoryReadData;
								o_Add18 <= HIGH;
								o_ControllerState <= `MC_RMW_MOD2;
							end
							`RMW_AND_A:
							begin
								o_MemoryWriteData <= i_MemoryReadData & i_RegisterA;
								`DOUT_LOW <= i_MemoryReadData & i_RegisterA;
								o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_OR_A:
							begin
								o_MemoryWriteData <= i_MemoryReadData | i_RegisterA;
								`DOUT_LOW <= i_MemoryReadData | i_RegisterA;
								o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_EOR_A:
							begin
								o_MemoryWriteData <= i_MemoryReadData ^ i_RegisterA;
								`DOUT_LOW <= i_MemoryReadData ^ i_RegisterA;
								o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_SUB_A:
							begin
								o_Addend1 <= i_RegisterA;
								o_Addend2 <= i_MemoryReadData;
								o_Add18 <= LOW;
								o_ControllerState <= `MC_RMW_MOD2;
							end
							`RMW_WRITE_CHAR_A:
							begin
								case (i_CharAddressMode)
									3'b000: `DOUT_LOW <= i_MemoryReadData;
									3'b001: `DOUT_LOW <= `DOUT_LOW;
									3'b010: `DOUT_LOW <= {i_RegisterA[9:17],i_MemoryReadData[9:17]};
									3'b011: `DOUT_LOW <= {i_MemoryReadData[0:8],i_RegisterA[9:17]};
									3'b100: `DOUT_LOW <= {i_RegisterA[12:17],i_MemoryReadData[6:17]};
									3'b101: `DOUT_LOW <= {i_MemoryReadData[0:5],i_RegisterA[12:17],i_MemoryReadData[12:17]};
									3'b110: `DOUT_LOW <= {i_MemoryReadData[0:11],i_RegisterA[12:17]};
									3'b111: `DOUT_LOW <= `DOUT_LOW;
								endcase
								o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							`RMW_WRITE_CHAR_Q:
							begin
								case (i_CharAddressMode)
									3'b000: `DOUT_LOW <= i_MemoryReadData;
									3'b001: `DOUT_LOW <= `DOUT_LOW;
									3'b010: `DOUT_LOW <= {i_RegisterQ[9:17],i_MemoryReadData[9:17]};
									3'b011: `DOUT_LOW <= {i_MemoryReadData[0:8],i_RegisterQ[9:17]};
									3'b100: `DOUT_LOW <= {i_RegisterQ[12:17],i_MemoryReadData[6:17]};
									3'b101: `DOUT_LOW <= {i_MemoryReadData[0:5],i_RegisterQ[12:17],i_MemoryReadData[12:17]};
									3'b110: `DOUT_LOW <= {i_MemoryReadData[0:11],i_RegisterQ[12:17]};
									3'b111: `DOUT_LOW <= `DOUT_LOW;
								endcase
								o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
							end
							default: 
							begin
								o_Fault <= HIGH;
								o_ControllerState <= `MC_IDLE;
							end
						endcase
					end
				end
				
				`MC_RMW_MOD2:
				begin
					o_MemoryWriteData <= i_Sum18;
					`DOUT_LOW <= i_Sum18;
					o_CarryFlag <= i_Carry18;
					o_OverflowFlag <= i_Overflow18;
					o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
				end
				
				`MC_RMW_WAITING:
				begin
					o_ControllerState <= (i_MemoryOpComplete) ? `MC_RMW_WAITING : `MC_RMW_WRITING;
				end
				
				`MC_RMW_WRITING:
				begin
					o_MemoryWriteEnable <= HIGH;
					o_MemChipSelect <= HIGH;
					if (i_MemoryModuleFault) 
					begin
						o_Fault <= HIGH;
						o_ControllerState <= `MC_IDLE;
					end
					else o_ControllerState <= (i_MemoryOpComplete) ? `MC_FINISH_WRITE : `MC_RMW_WRITING;
				end
				
				default: 
				begin
					o_Fault <= HIGH;
					o_ControllerState <= `MC_IDLE;
				end
				
			endcase
		end
	end
endmodule
