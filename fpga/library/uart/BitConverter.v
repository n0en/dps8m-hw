module BitConverter(
	input wire enable,
	input wire [4:0] samples,
	output wire ready,
	output reg bout
);

	assign ready = enable;

	always @ (posedge enable)
	case (samples)
		5'b00000: bout = 0;
		5'b00001: bout = 0;
		5'b00010: bout = 0;
		5'b00011: bout = 0;
		5'b00100: bout = 0;
		5'b00101: bout = 0;
		5'b00110: bout = 0;
		5'b00111: bout = 1;
		5'b01000: bout = 0;
		5'b01001: bout = 0;
		5'b01010: bout = 0;
		5'b01011: bout = 1;
		5'b01100: bout = 0;
		5'b01101: bout = 1;
		5'b01110: bout = 1;
		5'b01111: bout = 1;
		5'b10000: bout = 0;
		5'b10001: bout = 0;
		5'b10010: bout = 0;
		5'b10011: bout = 1;
		5'b10100: bout = 0;
		5'b10101: bout = 1;
		5'b10110: bout = 1;
		5'b10111: bout = 1;
		5'b11000: bout = 0;
		5'b11001: bout = 1;
		5'b11010: bout = 1;
		5'b11011: bout = 1;
		5'b11100: bout = 1;
		5'b11101: bout = 1;
		5'b11110: bout = 1;
		5'b11111: bout = 1;
	endcase


endmodule
