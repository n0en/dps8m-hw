module BitSampler(
	input wire sclk,			// Sample clock, s/b 10 x baud rate
	input wire rx,				// Receive signal
	input wire start,			// Start signal to synchronize bit sampling

	output reg ready,			// Indicate when 5 samples have been read
	output reg [4:0] bout	// Output samples, valid when ready is high
);

	reg [2:0] sample_count;
	reg [4:0] bit_samples;
	reg [2:0] ready_count;
	reg take_sample;
	
	reg last_start;
	reg start_rising_edge;
	
	always @ (posedge sclk)
	begin
		start_rising_edge <= start & ~last_start;
		last_start <= start;

		if (start_rising_edge)
		begin
			sample_count <= 3'b0;
			bit_samples <= 5'b0;
			bout <= 5'b0;
			take_sample <= 1'b0;
			ready_count <= 3'b0;
			ready <= 1'b0;
			last_start <= start;
		end
		else
		begin
			if (ready_count > 0)
			begin
				ready <= 1'b1;
				ready_count <= ready_count - 3'b001;
			end
			else 
			begin
				ready <= 1'b0;
			end
			if (take_sample)
			begin
				bit_samples <= (bit_samples << 1'b1) | !rx;
				take_sample <= 1'b0;
				sample_count <= sample_count + 3'b001;
			end
			else
			begin
				if (sample_count == 5)
				begin
					bout <= bit_samples;
					sample_count <= 0;
					ready_count <= 3;
				end
				take_sample <= 1'b1;
			end
		end
	end
	
endmodule
