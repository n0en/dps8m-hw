module FrameSynchronizer(
	input wire clk,			// Fast system clock
	input wire bclk,			// Serial clock (10x baud rate)
	input wire rd,				// Raw received data line
	input wire reset,			// Reset signal
	
	output reg sync,			// High when synced with frame
	output reg frame_error	// Indicates if a framing error has been detected
	
);

	localparam STATE_IDLE					= 3'b00;
	localparam STATE_DETECT_START_BIT	= 3'b01;
	localparam STATE_IN_FRAME			 	= 3'b10;
	localparam STATE_DETECT_STOP_BIT		= 3'b11;

	reg last_bclk;				// Last sampled value of bclk (for edge detection)
	reg bclk_rising_edge;	// Flag indicating that the bclk had a rising edge on this cycle
	reg [6:0] bit_count;		// Number of bit clocks left until bit is complete
	reg [3:0] low_count;		// Number of bit clock samples that the signal was low

	reg [1:0] current_state;

	always @ (posedge clk or posedge reset)
	begin
		if (reset)
		begin
			sync = 1'b0;
			bit_count <= 7'b0;
			low_count <= 4'b0;
			frame_error <= 1'b0;
			current_state <= STATE_IDLE;
		end
		else
		begin
			bclk_rising_edge <= bclk & ~last_bclk;
			last_bclk <= bclk;
			
			case (current_state)
				
				STATE_IDLE:
				begin
					sync = 1'b0;
					bit_count <= 7'd9;
					low_count <= 4'b0;
					if (!rd) current_state <= STATE_DETECT_START_BIT;
				end
				
				STATE_DETECT_START_BIT:
				begin
					if (bclk_rising_edge)
					begin
						frame_error <= 1'b0;
						sync <= 1'b1;
						if (!rd) low_count <= low_count + 4'b1;
						if (bit_count == 0)
						begin
							if (low_count > 4)
							begin
								bit_count <= 7'd79;
								current_state <= STATE_IN_FRAME;
							end
							else
							begin
								frame_error <= 1'b1;
								current_state <= STATE_IDLE;
							end
						end
						else
						begin
							bit_count <= bit_count - 7'b1;
						end
					end
					
				end
				
				STATE_IN_FRAME:
				begin
					if (bclk_rising_edge)
					begin
						if (bit_count == 0)
						begin
							bit_count <= 7'd8;
							low_count <= 4'b0;
							current_state <= STATE_DETECT_STOP_BIT;
						end
						else
						begin
							bit_count <= bit_count - 7'b1;
						end
					end
				end
				
				STATE_DETECT_STOP_BIT:
				begin
					if (bclk_rising_edge)
					begin
						if (!rd) low_count <= low_count + 4'b0001;
						if (bit_count == 0)
						begin
							if (low_count > 3) 
							begin
								frame_error <= 1'b1;
								sync <= 1'b0;
							end
							current_state <= STATE_IDLE;
						end
						else
						begin
							bit_count <= bit_count - 7'b1;
						end
					end
				end
			
			endcase
		end
	end

endmodule
