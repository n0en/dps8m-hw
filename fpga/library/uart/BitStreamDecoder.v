module BitStreamDecoder(
		input wire clk,			// Bit sample clock
		input wire reset,
		input wire rd,				// Raw input receive data signal
		input wire bit_ready,	// Input signal from the Bit Sampler that a bit is ready
		input wire bin,			// Input bit from the Bit Sampler
		input wire read,			// Signal that the output byte has been read
		input wire sync,			// Signal indicating frame is in sync
		
		output reg frame_error,	// Indicate the stop bit was invalid
		output reg ready,			// Indicates when bout is valid, resets when byte is read
		output reg [7:0] bout,	// The received byte
		output reg overrun		// Signal that the previous byte had not yet been read when the next byte was ready
);

	localparam STATE_IDLE      	= 2'b00;
	localparam STATE_START     	= 2'b01;
	localparam STATE_RECEIVE   	= 2'b10;
	localparam STATE_CHECK_STOP	= 2'b11;
	
	reg [1:0] current_state;
	reg [7:0] output_byte;		// Holding register to shift the received bits into
	reg [4:0] bit_count;			// Number of bits currently in output_byte
	reg bit_ready_detected;		// When high, we've dectect the leading edge of the bit_ready signal
	reg bit_ready_seen;			// Set when we seen the leading edge of the bit_ready signal and reset when we've processed the bit_value
	reg bit_value;					// The level of the bit seen when the bit_ready signal went high
	reg last_sync;					// Last sampled value of the sync signal
	reg sync_rising_edge;		// Found a transition from low to high of the sync signal
	

	always @ (posedge clk or posedge reset)
	begin
		if (reset)
		begin
			current_state <= STATE_IDLE;
			frame_error <= 1'b0;
			ready <= 1'b0;
			output_byte <= 8'b0;
			bout <= 8'b0;
			bit_count <= 4'b0;					
			bit_ready_detected <= 1'b0;		
			bit_ready_seen <= 1'b0;				
			bit_value <= 1'b0;
			overrun <= 1'b0;
			last_sync <= sync;
		end
		else
		begin
		
			sync_rising_edge <= sync & ~last_sync;
			last_sync <= sync;
		
		
			// See if the previous byte has been read
			if (ready && read)
			begin
				ready <= 1'b0;
				overrun <= 1'b0;
			end

			// Check if there is a new bit to be processed
			if (bit_ready && !bit_ready_detected)
			begin
				bit_ready_detected <= 1'b1;
				bit_ready_seen <= 1'b1;
				bit_value <= ~bin;
			end
			else if (!bit_ready && bit_ready_detected)
			begin
				bit_ready_detected <= 1'b0;
			end
			
			case (current_state)
			
				// Line is now confirmed idle, set up to sync on next SPACE
				STATE_IDLE:
				begin
					frame_error <= 1'b0;
					bit_ready_seen <= 1'b0;			// We are going to ignore any bit received until this point until we hit the START state
					if (sync_rising_edge) current_state <= STATE_START;
				end
				
				// We are now sync'ed with the line, wait for the Start bit to be received
				STATE_START:
				begin
					if (!sync) current_state <= STATE_IDLE;
					output_byte <= 8'b0;
					bit_count <= 4'b0;
					if (bit_ready_seen) 
					begin
						if (bit_value == 1'b0)
						begin
							current_state <= STATE_RECEIVE;
						end
						else
						begin
							frame_error <= 1'b1;
							current_state <= STATE_IDLE;
						end
						bit_ready_seen <= 1'b0;
					end
				end
				
				// We are now receiving bits, collect them until we get all eight
				STATE_RECEIVE:
				begin
					if (!sync) current_state <= STATE_IDLE;
					else if (bit_ready_seen)
					begin
						if (bit_count == 4'd7)
						begin
							current_state <= STATE_CHECK_STOP;
						end
						
						output_byte <= (output_byte >> 1) | (bit_value << 7);
						bit_ready_seen <= 1'b0;
						bit_count <= bit_count + 4'b1;
					end
				end
				
				// Wait for the stop bit and make sure it's a MARK
				STATE_CHECK_STOP:
				begin
					if (bit_ready_seen) 
					begin
						bout <= output_byte;
						overrun <= ready;
						frame_error <= ~bit_value;
						ready <= 1'b1;
						bit_ready_seen <= 1'b0;
						current_state <= STATE_IDLE;
					end
				end
				
			endcase
		end
	end

endmodule
