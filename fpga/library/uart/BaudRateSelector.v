module BaudRateSelector(
	input wire clk,
	input wire high_speed,
	output reg [31:0] baud
);

	always @(posedge clk)
	begin
		baud <= high_speed ? 9600 : 1200;
//		baud <= high_speed ? 32'b11111111111111111111111111111111 : 0;
	end
	
endmodule