// This module is used to "unpack" bits so a hex display can be used to display octal
module OctalHexConverter18x24(
	input wire [0:17] oin,		// Binary value to be unpacked for octal display
	output wire [0:23] hout		// Hex display output
);

	assign hout[1:3] = oin[0:2];
	assign hout[0] = 1'b0;
	assign hout[5:7] = oin[3:5];
	assign hout[4] = 1'b0;
	assign hout[9:11] = oin[6:8];
	assign hout[8] = 1'b0;
	assign hout[13:15] = oin[9:11];
	assign hout[12] = 1'b0;
	assign hout[17:19] = oin[12:14];
	assign hout[16] = 1'b0;
	assign hout[21:23] = oin[15:17];
	assign hout[20] = 1'b0;
	
endmodule
