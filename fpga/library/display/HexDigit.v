module HexDigit (
	input wire [0:3] inputValue,
	input wire decPoint,
	output wire [0:7] displayDigit
);

   function automatic [0:6] digit;
      input [0:3] num; 
      case (num)
         0:  digit = 7'b0000001;  // 0
         1:  digit = 7'b1001111;  // 1
			2:  digit = 7'b0010010;  // 2
         3:  digit = 7'b0000110;  // 3
         4:  digit = 7'b1001100;  // 4
         5:  digit = 7'b0100100;  // 5
         6:  digit = 7'b0100000;  // 6
         7:  digit = 7'b0001111;  // 7
			8:  digit = 7'b0000000;  // 8
			9:  digit = 7'b0000100;  // 9
			10:  digit = 7'b0001000;  // A
			11:  digit = 7'b1100000;  // b
			12:  digit = 7'b0110001;  // C
			13:  digit = 7'b1000010;  // d
			14:  digit = 7'b0110000;  // E
			15:  digit = 7'b0111000;  // F
      endcase
   endfunction

	assign displayDigit[0:6] = digit(inputValue);
	assign displayDigit[7] = !decPoint;

endmodule
