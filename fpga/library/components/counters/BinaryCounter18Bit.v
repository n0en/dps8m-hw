// Provide a reset signal that is a constant length
module BinaryCounter18bit(
	input 	wire 				i_Clock,			// The clock to trigger counting
	input 	wire 				i_Reset,			// A reset signal to set the counter to 0
	output	reg	[0:17]	o_Count			// The current count	
);
	
	always @ (posedge i_Clock or posedge i_Reset)
	begin
		if (i_Reset) o_Count <= 0;
		else o_Count <= o_Count + 18'b1;
	end
	
endmodule
