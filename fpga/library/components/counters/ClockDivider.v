module ClockDivider(
	input wire clk,
	input wire [23:0] divisor,
	output reg div_clk
);

	reg [23:0] counter;
	
	always @ (posedge clk)
	begin
		if (counter == 0)
		begin
			counter <= divisor >> 1;
			div_clk <= ~div_clk;
		end
		else
		begin
			counter <= counter - 24'b1;
		end
	end

endmodule
