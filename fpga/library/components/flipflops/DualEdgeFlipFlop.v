// This module is a "Dual Edge Flip Flop". It will detect both the rising
// and falling edges of a clock signal.
module DualEdgeFlipFlop (
	input wire i_Clock, 
	input wire i_Reset, 
	input wire i_In,
	output o_Out
);

	reg r_Trig1;
	reg r_Trig2;

	assign o_Out = r_Trig1^r_Trig2;

	always @(posedge i_Clock, posedge i_Reset) 
	begin
	 if (i_Reset)  r_Trig1 <= 0;
	 else  r_Trig1 <= i_In^r_Trig2;
	end

	always @(negedge i_Clock, posedge i_Reset) 
	begin
	 if (i_Reset)  r_Trig2 <= 0;
	 else  r_Trig2 <= i_In^r_Trig1;
	end
  
endmodule