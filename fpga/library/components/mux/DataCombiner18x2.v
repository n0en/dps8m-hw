module DataCombiner18x2(
	input wire	[1:0]		i_Selector,			// Selects one of the inputs
	input wire	[17:0]	i_DataIn1,			// Input to use for selector = 1
	input wire	[17:0]	i_DataIn2,			// Input to use for selector = 2
	
	output wire	[17:0]	o_DataOut			// Output data based on selector
);

	wire [17:0] w_Data1;
	wire [17:0] w_Data2;
	
	assign w_Data1 = (i_Selector == 2'b01) ? i_DataIn1 : 18'b0;
	assign w_Data2 = (i_Selector == 2'b10) ? i_DataIn2 : 18'b0;
	
	assign o_DataOut = w_Data1 | w_Data2;

endmodule