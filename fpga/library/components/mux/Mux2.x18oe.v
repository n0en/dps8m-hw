// This module will select one output from up to two inputs
module Mux2x18oe(
	input wire i_Enable,
	input wire i_Select,
	input wire [0:17] i_Data0,
	input wire [0:17] i_Data1,
	
	output reg [0:17] o_Data
);

	always @(i_Select or i_Data0 or i_Data1 or i_Enable)
	begin
		if (i_Enable)
			case (i_Select)
				1'b0: 
				begin
					o_Data[0:17] <= i_Data0[0:17];
				end
				1'b1:
				begin
					o_Data[0:17] <= i_Data1[0:17];
				end
			endcase
		else
			o_Data <= 18'b0;
	end
	
endmodule
