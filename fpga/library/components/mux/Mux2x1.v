// This module will select one output from up to two inputs
module Mux2x1(
	input wire sel,
	input wire min0,
	input wire min1,
	
	output reg out
);

	always @(sel or min0 or min1)
	begin
		case (sel)
			1'b0: 
			begin
				out <= min0;
			end
			1'b1:
			begin
				out <= min1;
			end
		endcase
	end
	
endmodule
