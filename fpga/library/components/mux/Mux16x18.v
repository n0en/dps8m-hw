// This module will select one output from up to sixteen inputs
module Mux16x18(
	input wire [0:3] sel,
	input wire [0:17] min00,
	input wire [0:17] min01,
	input wire [0:17] min02,
	input wire [0:17] min03,
	input wire [0:17] min04,
	input wire [0:17] min05,
	input wire [0:17] min06,
	input wire [0:17] min07,
	input wire [0:17] min08,
	input wire [0:17] min09,
	input wire [0:17] min10,
	input wire [0:17] min11,
	input wire [0:17] min12,
	input wire [0:17] min13,
	input wire [0:17] min14,
	input wire [0:17] min15,
	
	output reg [0:17] out
);

	always @(sel or min00 or min01 or min02 or min03 or min04 or min05 or min06 or min07 
				or min08 or min09 or min10 or min11 or min12 or min13 or min14 or min15)
	begin
		case (sel)
			4'b0000: 
			begin
				out <= min00;
			end
			4'b0001:
			begin
				out <= min01;
			end
			4'b0010:
			begin
				out <= min02;
			end
			4'b0011:
			begin
				out <= min03;
			end
			4'b0100:
			begin
				out <= min04;
			end
			4'b0101:
			begin
				out <= min05;
			end
			4'b0110:
			begin
				out <= min06;
			end
			4'b0111:
			begin
				out <= min07;
			end
			4'b1000:
			begin
				out <= min08;
			end
			4'b1001:
			begin
				out <= min09;
			end
			4'b1010:
			begin
				out <= min10;
			end
			4'b1011:
			begin
				out <= min11;
			end
			4'b1100:
			begin
				out <= min12;
			end
			4'b1101:
			begin
				out <= min13;
			end
			4'b1110:
			begin
				out <= min14;
			end
			4'b1111:
			begin
				out <= min15;
			end
		endcase
	end
	
endmodule
