// This module will select one output from up to two inputs
module Mux2x24(
	input wire sel,
	input wire [0:23] min0,
	input wire [0:23] min1,
	
	output reg [0:23] out
);

	always @(sel or min0 or min1)
	begin
		case (sel)
			1'b0: 
			begin
				out[0:23] <= min0[0:23];
			end
			1'b1:
			begin
				out[0:23] <= min1[0:23];
			end
		endcase
	end
	
endmodule
