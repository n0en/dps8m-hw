module RomInitBinary
	(
		input 				clock,
		input [0:12]		address,
		output reg [0:17]	outputData
	);
	
	parameter INIT_FILE = "ROM.init";
	
	reg [0:17] rom [0:8191] /* synthesis ram_init_file = "tand.mif" */;
	
//	initial
//	begin
//		$readmemb(INIT_FILE, rom);
//	end
	
	always @ (posedge(clock))
		begin
			outputData <= rom[address];
		end
		
endmodule
