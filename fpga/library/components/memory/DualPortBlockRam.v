module DualPortBlockRam #(
    parameter DATA = 72,
    parameter ADDR = 10
) (
    // Port A
    input   wire                a_clk,
    input   wire                a_wr,
    input   wire    [0:ADDR-1]  a_addr,
    input   wire    [0:DATA-1]  a_din,
    output  reg     [0:DATA-1]  a_dout,
     
    // Port B
    input   wire                b_clk,
    input   wire                b_wr,
    input   wire    [0:ADDR-1]  b_addr,
    input   wire    [0:DATA-1]  b_din,
    output  reg     [0:DATA-1]  b_dout
);
 
	// Shared memory
	reg [0:DATA-1] mem [0:(2**ADDR)-1] /* synthesis ram_init_file = "dn355_1.mif" */;

	// Port A
	always @(posedge a_clk) begin
		a_dout      <= mem[a_addr];
		if(a_wr) begin
			a_dout      <= a_din;
			mem[a_addr] <= a_din;
		end
	end
 
	// Port B
	always @(posedge b_clk) begin
		b_dout      <= mem[b_addr];
		if(b_wr) begin
			b_dout      <= b_din;
			mem[b_addr] <= b_din;
		end
	end
 
endmodule
