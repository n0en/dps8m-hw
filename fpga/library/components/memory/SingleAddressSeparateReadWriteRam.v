module SingleAddressSeparateReadWriteRam
	(
		input					clock,
		input					ramEnable,
		input					writeEnable,
		input [14:0]		address,
		input	[17:0]		inputData,
		output reg [17:0]	outputData
	);
	
	reg [17:0] ramMain [0:32767];
	
	always @ (posedge(clock))
		if (ramEnable)
		begin
			if (writeEnable)
				ramMain[address] <= inputData;
			outputData <= ramMain[address];
		end
		
endmodule
