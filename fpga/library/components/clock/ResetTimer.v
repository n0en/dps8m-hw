// Provide a reset signal that is a constant length
module ResetTimer(
	input sysClk, 				// The system clock to time the reset pulse
	input resetTrigger, 		// The trigger signal to initiate a reset
	output sysReset			// The output reset signal
);
	
	reg [9:0] resetCount;	// Counter for how long to hold the reset signal high
	
	assign sysReset = (resetCount == 0) ? 1'b0 : 1'b1;
	
	always @ (posedge(resetTrigger), posedge(sysClk))
	begin
		if (resetTrigger)
		begin
			resetCount <= 10'h3ff;
		end
		else if (resetCount > 0)
		begin
			resetCount <= resetCount - 10'h001;
		end
	end
	
endmodule
