// This module will generate a 1 KHz output clock when given a 50 MHz input clock.
module Clock1KHz(
	input wire clock50MHz,	// Input of a 50 MHz clock signal
	output reg clock1KHz		// Output of a 1 KHz clock signal given the correct input clock
);

	reg [15:0] counter = 0;
	
	always @ (posedge(clock50MHz))
	begin
		counter <= (counter >= 50000) ? 16'b0 : counter + 16'b1;
		clock1KHz <= (counter < 25000) ? 1'b0 : 1'b1;
	end
	
endmodule
