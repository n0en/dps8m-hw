// This module will debounce a switch synchrounsly with a low speed clock.
// A 4 Hz clock works for most switches.
module SwitchDebouncer (
	input switchIn,			// The input switch to debounce
	input slowClk,				// The low speed clock to use for timing the debounce
	output reg switchOut		// The debounced output signal
);

	always @ (posedge(slowClk))
	begin
		if (switchIn && ~switchOut)
			switchOut = 1'b1;
		else if (~switchIn && switchOut)
			switchOut = 1'b0;
	end

endmodule
