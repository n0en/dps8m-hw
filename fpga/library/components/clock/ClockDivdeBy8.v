// This module will generate a signal that is 1/8 of the input signal
module ClockDivideBy8(
	input i_ClockIn,				// Input clock signal
	output wire o_ClockOut		// Output of signal at 1/8 the input signal with 50% duty cycle
);

	reg [2:0] counter = 0;
	
	assign o_ClockOut = counter[2];
	
	always @ (posedge(i_ClockIn))
	begin
		counter <= counter + 3'b001;
	end
	
endmodule
