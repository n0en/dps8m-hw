// This module will generate a 4 Hz output clock when given a 50 MHz input clock.
module Clock4Hz(
	input wire clock50MHz,	// Input of a 50 MHz clock signal
	output reg clock4Hz		// Output of a 4 Hz clock signal given the correct input clock
);

	reg [26:0] counter = 0;
	
	always @ (posedge(clock50MHz))
	begin
		counter <= (counter >= 24999999) ? 27'b0 : counter + 27'b1;
		clock4Hz <= (counter < 12500000) ? 1'b0 : 1'b1;
	end
	
endmodule
