# Engineering Specifications Index

## DOC (General Documentation and Standards)

| ESN              | Title                                                 |
|------------------|-------------------------------------------------------|
| ES-DOC-23-001-00 | DPS8M-HW Engineering Specification Document Standards |
|                  |                                                       |
|                  |                                                       |

## DPS (DPS8/M Mainframe)

| ESN              | Title                                                 |
|------------------|-------------------------------------------------------|
| ES-DPS-23-001-00 | DPS8/M Original Mainframe System Overview             |
|                  |                                                       |
|                  |                                                       |

## FNP (Datanet Front-End Processor)

| ESN              | Title                                                 |
|------------------|-------------------------------------------------------|
| ES-FNP-23-001-00 | Front-end Network Processor Overview                  |
| ES-FNP-23-002-00 | FNP System Control Unit                               |
| ES-FNP-23-003-00 | FNP Coupler Communication Protocol                    |
| ES-FNP-23-004-00 | FNP System Control Unit Tester                        |
|                  |                                                       |


